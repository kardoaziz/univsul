import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

@Injectable()
export class AuthenticationProvider {

  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello AuthenticationProvider Provider');
  }
  public logout()
  {
    localStorage.setItem("regUsername","");
    localStorage.setItem("regPassword","");
  }
  // public login(info:any)
  public login(user:any,password:any)
  {

    let urlsp = new URLSearchParams();
    urlsp.append('request',"login");
    urlsp.append('user',user);
    urlsp.append('pass',password);
    // urlsp.append('info',JSON.stringify(info));

    return this.http.post('http://'+this.global.host+'/registration/login.php',urlsp).map((res:Response)=> res.json());
      //return this.http.post('http://'+this.global.host+'/registration/login.php',urlsp).map((res:Response)=> res);
  }
   public getPermission(id_role:any)
  {

  	let urlsp = new URLSearchParams();
    urlsp.append('request',"getpermission");
  	urlsp.append('id_role',id_role+"");
  	// urlsp.append('info',JSON.stringify(info));

  	return this.http.post('http://'+this.global.host+'/registration/login.php',urlsp).map((res:Response)=> res.json());
  	  //return this.http.post('http://'+this.global.host+'/registration/login.php',urlsp).map((res:Response)=> res);
  }

  public getPermissionExam(id_role:any){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getpermissionExam");
    urlsp.append('id_role',id_role+"");
    return this.http.post('http://'+this.global.host+'/registration/login.php',urlsp).map((res:Response)=> res.json());
  }
}
