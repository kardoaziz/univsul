import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

@Injectable()
export class HomeProvider {

  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello HomeProvider Provider');
  }
  public add_persons(first:any,mid:any,third:any,gender:any,phone:any,email:any,address:any,id_dep:any,user:any,pass:any,emp_dep:any,id_role)
  {
    let urlsp = new URLSearchParams();
    urlsp.append('request','addPerson');
    urlsp.append('name',first);
    urlsp.append('fname',mid);
    urlsp.append('gname',third);
    urlsp.append('gender',gender);
    urlsp.append('phone',phone);
    urlsp.append('email',email);
    urlsp.append('address',address);
    urlsp.append('id_dep',id_dep);
    urlsp.append('user',user);
    urlsp.append('pass',pass);
    urlsp.append('emp_type',emp_dep);
    urlsp.append('id_role',id_role);
    // urlsp.append('info',JSON.stringify(info));
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());
    // return this.http.post('http://'+this.global.host+'/registration/login.php',urlsp).map((res:Response)=> res);
  }
  public addTeacher(first:any,mid:any,third:any,gender:any,phone:any,email:any,address:any,id_dep:any,acadimic_title:any){
    let urlsp = new URLSearchParams();
    urlsp.append('request','addteacher');
    urlsp.append('name',first);
    urlsp.append('fname',mid);
    urlsp.append('gname',third);
    urlsp.append('gender',gender);
    urlsp.append('phone',phone);
    urlsp.append('email',email);
    urlsp.append('address',address);
    urlsp.append('id_dep',id_dep);
    urlsp.append('acadimic_title',acadimic_title);
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());

  }
  public getPersons(dep:any){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getperson");
    urlsp.append('dep',dep);

    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());
        
  }
  public getMember(){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getmember");
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());
        
  }
  public getTeacher(dep_id){
    let urlsp=new URLSearchParams();
    urlsp.append('request',"getteacher");
    urlsp.append('depid',dep_id);
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());

  }
  public delete_member(id_person:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request',"deleteMember");
    urlsp.append('id_person',id_person);
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());
  }

  public update_employees(id_person,firstN:any,midN:any,thirdN:any,gender:any,phoneN:any,email:any,address:any,username:any,pass:any,id_role,role_name:any){
    let urlsp = new URLSearchParams();
    urlsp.append('request','updateEmployee');
    urlsp.append('id_person',id_person);
    urlsp.append('f_name',firstN);
    urlsp.append('m_name',midN);
    urlsp.append('l_name',thirdN);
    urlsp.append('gender',gender);
    urlsp.append('email',email);
    urlsp.append('phone',phoneN);
    urlsp.append('username',username);
    urlsp.append('pass',pass);
    urlsp.append('address',address);
    urlsp.append('id_role',id_role);
    urlsp.append('emp_type',role_name);
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());
  }
  updateTeacher(id_person,firstN:any,midN:any,thirdN:any,gender:any,phoneN:any,email:any,address:any,id_dep,acadimic_title:any){

    let urlsp = new URLSearchParams();
    urlsp.append('request','updateTeacher');
    urlsp.append('id_person',id_person);
    urlsp.append('f_name',firstN);
    urlsp.append('m_name',midN);
    urlsp.append('l_name',thirdN);
    urlsp.append('gender',gender);
    urlsp.append('email',email);
    urlsp.append('phone',phoneN);
    urlsp.append('address',address);
    urlsp.append('id_dep',id_dep);
    urlsp.append('acadimic_title',acadimic_title);
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());
  }
  delete_teacher(id_person){
    let urlsp=new URLSearchParams();
    urlsp.append('request',"deleteTeacher");
    urlsp.append('id_person',id_person);
    return this.http.post('http://'+this.global.host+'/registration/home.php',urlsp).map((res:Response)=> res.json());
  }
}
