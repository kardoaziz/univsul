import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import * as papa from 'papaparse';

import { ToastController} from 'ionic-angular';
import 'rxjs/add/operator/map';
// import { URLSearchParams } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import { Platform} from 'ionic-angular';
import { InfoPage } from '../../pages/info/info';
import { ModalController } from 'ionic-angular';

declare var cordova: any;

@Injectable()
export class GlobalProvider {
	dep_name:any;
	role_name:any;
	currentUser:any;
  currentUser_dep:any;
  currentUser_dep_id:any;
  showEmpDep:any;
	semster:any;
	public permissions:any;
	public student:any;
	actions:any;
	persons:any;
	stage:any;
  shownstudent:any;
  searchedStudent:any;
  host:any;
  info = {
    id_student:'',
    code:'',
  fname:'',
  mname:'',
  sname:'',
  dob:'',
  lname:'',
  gender:'',
  phone:'',
  selected_resident:'',
  selected_state:'',
  selected_reciveType:'',
  selected_sem:'',
  note:'',
  selected_dep:'',
  selected_stage:'',
  id_stage:'',
  id_sem:'',
  id_dep:'',
  resons:'',
  date_of_payment:'',
  payment:'',
  sem_payment:'',
  id_sem_payment:'',
  price:'',
  univ_name_transfer:'',
    dep_name_transfer:'',
  univ_name_descent:'',
  dep_name_descent:'',
  selectedDescentType:'',
  selectedDescentState:'',
  dep_name:''}
    selected_stage:'';
    settings:any;
    headerRow:any;
    csvData:any;
  constructor(public modalCtrl:ModalController,  public http: Http,private toast:ToastController , public platform:Platform) {
    console.log('Hello GlobalProvider Provider');
    this.getJSON().subscribe(data => {
          console.log("Settings");
          console.log(data);
          this.settings = data;
          this.host=data['host'];
        }, error => console.log(error));
  }

  public presentToast(text){
  	let tst=this.toast.create({
  		message:text,
  		duration:3000,
  		position:'bottom',
  		cssClass:"centertext",
  	});
  	tst.present();
  }
private getJSON(): Observable<any> {
    console.log(document.location.href);
    console.log("Loading json from");
    console.log("../assets/settings.json");
    return this.http.get("./assets/settings.json")
      .map((res:any)=> res.json())
      .catch((error:any) => {
        return Observable.throw(error);
      })
  }
  public readCsvData() {
    this.http.get('assets/hh.csv')
      .subscribe(data=>{
        data = data['_body'];//.replace(/^"(.*)"$/, '$1');;
        // console.log(data);
         let allTextLines = data['_body'].replace(/"/g, '').split("\n");
        let headers = allTextLines[0].replace(/\s/g, '').split(',');
        // headers = headers.replace("\""," ")
        console.log("allTextLines");
        console.log(allTextLines);
        // console.log(headers);
         let lines = [];
         for (let i = 0; i < allTextLines.length; i++) {
            // split content based on comma
            let data = allTextLines[i].replace(/\s/g, '').split(',');
            if (data.length === headers.length) {
              let tarr = [];
              for (let j = 0; j < headers.length; j++) {
                tarr.push(data[j]);
              }

             // log each row to see output 
             // console.log(tarr);
             lines.push(tarr);
              }
            }
            console.log(lines);
            let id_sem =headers.indexOf("id_sem");
            let id_sub =headers.indexOf("id_sub");;
            let id_student =headers.indexOf("id_student");;
            console.log("id_sem "+id_sem);
            console.log("id_sub "+id_sub);
            console.log("id_student "+id_student);
            let i=0;
            lines.forEach(ele=>{
              if(i!=0)
              console.log("Update grades set corse1=0 where id_sem="+ele[id_sem]+" and id_sub="+ele[id_sub]+" id_student="+ele[id_student]);
            i+=1;
            })
          }
      
      );
  }
   public extractData(res,name) {
    console.log(res);
    console.log(name);
    let csvData = this.convertToCSV(res);
    let parsedData = papa.parse(csvData).data;
 
    this.headerRow = parsedData[0];
 
    parsedData.splice(0, 1);
    this.csvData = parsedData;
    this.downloadCSV(name);
  }
 
  downloadCSV(name) {
    console.log(name);
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = name+".csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

convertToCSV(data) {
    var csv: any = '';
    var line: any = '';
    let fields :any;
    let headers = ["id_student","sub_name","f_name","m_name","s_name","l_name","id_sub","id_sem","corse1","corse2","final","crossing","crossingfinal","crossing2"];
    //Get keys and make them headers
    fields =  Object.keys(data[0]);
    // line +="no. ,";
    for (let key of Object.keys(data[0])) {
      if(headers.indexOf(key)>-1)
         line += key +" , ";
    
  }
  let balance = 0;

  // line += " balance , ";
    csv += line + '\r\n';

    //Data
    let j = 1;
    for (var i = data.length-1; i >=0 ; i--) {
      // console.log(data[i]);
      // console.log("balance: ", balance);
      line = '';
      // line+=j+" , ";
      j+=1;
      
      // Get each key as separate field separated by comma
      for (let key of fields) {
      if(headers.indexOf(key)>-1)
    line += data[i][key] +" , ";
    
  }
  // if(data[i]['type'] == 'Cash')
  // {
  //   balance += parseFloat(data[i]['dollar'])+(parseFloat(data[i]['dinar'])/parseFloat(data[i]['dollar_price']) );
  // }
  // else{
  //   balance -= parseFloat(data[i]['dollar'])+(parseFloat(data[i]['dinar'])/parseFloat(data[i]['dollar_price']) );

  // }
  // line += balance+" , ";
  // new row
      csv += line + '\r\n';

    }
    console.log(csv);
    return csv
  }
  public presentModal(location) {
    const modal =  this.modalCtrl.create(
       InfoPage,{cssClass:"mymodal",location:location}
    );
      modal.present();
  }
}
