import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

@Injectable()
export class SubjectProvider {
//
  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello SubjectProvider Provider');
  }//
  public add_subject(id_dep,sub_name:any,stage:any,unit:any,sub_type:any,h_theory:any,h_practice){
  	let urlsp = new URLSearchParams();
  	urlsp.append('request','addsubject');
  	urlsp.append('id_dep',id_dep);
  	urlsp.append('sub_name',sub_name);
  	urlsp.append('stage',stage);
  	urlsp.append('unit',unit);
  	urlsp.append('sub_type',sub_type);
  	urlsp.append('h_theory',h_theory);
  	urlsp.append('h_practice',h_practice);

  	return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
  getSubject(dep){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getsubject");
    urlsp.append('dep',dep);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
  masterSheet(dep:any, sem:any,stage:any){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"mastersheet");
    urlsp.append('dep',dep);
    urlsp.append('sem',sem);
    urlsp.append('stage',stage);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
  stuStageGrades(dep:any, sem:any,stage:any){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"stsubgrades");
    urlsp.append('id_dep',dep);
    urlsp.append('id_sem',sem);
    urlsp.append('id_stage',stage);
    urlsp.append('passing_limit',this.global.settings.passing_limit);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
   saveNotes(stu){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"savenotes");
    urlsp.append('stu',JSON.stringify(stu));
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
   lockSemester(sem){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"locksemester");
    urlsp.append('sem',sem);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
  unlockSemester(sem){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"unlocksemester");
    urlsp.append('sem',sem);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
  public get_Stage_Subject(id_student:any,id_sem:any,id_stage:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','get_stage_subject');
    urlsp.append('id_student',id_student);
    urlsp.append('id_sem',id_sem);
    urlsp.append('id_stage',id_stage);
    
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
  public getFailedStudents(id_dep:any,id_sem:any,id_stage:any,passing_limit,round){
    let urlsp=new URLSearchParams();
    urlsp.append('request','stufailedclass');
    urlsp.append('id_dep',id_dep);
    urlsp.append('id_sem',id_sem);
    urlsp.append('id_stage',id_stage);
    urlsp.append('round',round);
    urlsp.append('passing_limit',passing_limit);

    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
  public no_student(id_dep:any,id_sem:any,id_stage:any, passing_limit){
    let urlsp=new URLSearchParams();
    urlsp.append('request','no_student');
    urlsp.append('id_dep',id_dep);
    urlsp.append('id_sem',id_sem);
    urlsp.append('id_stage',id_stage);

    urlsp.append('passing_limit',passing_limit);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
  public clearCurve(id_dep:any,id_sem:any,id_stage:any,round){
    let urlsp=new URLSearchParams();
    urlsp.append('request','clearcurve');
    urlsp.append('id_dep',id_dep);
    urlsp.append('id_sem',id_sem);
    urlsp.append('id_stage',id_stage);
    urlsp.append('round',round);

    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
  public applyCurve(data:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','applycurve');
    urlsp.append('data',JSON.stringify(data));
    // urlsp.append('id_sem',id_sem);
    // urlsp.append('id_stage',id_stage);

    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
  public addcorse1(corse1:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addcorse1');
    urlsp.append('corse1',JSON.stringify( corse1));
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }

  public add_sub_grades(grades:any,round:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addgrades');
    urlsp.append('round',round);
    urlsp.append('grades',JSON.stringify( grades));
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }

  public addFinal(final:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addfinal');
    urlsp.append('final',JSON.stringify( final));
    // urlsp.append('state',JSON.stringify( state));
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
  public addcorse2(corse2:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addcorse2');
    urlsp.append('corse2',JSON.stringify( corse2));
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
  public getGrade(grade:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','getgrade');
    urlsp.append('grade',JSON.stringify( grade));
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());
  }
   public AnalyzeCurve(round:any,min:any,max:any,id_sem:any,passing_limit:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','totalcurve');
    urlsp.append('round',round);
    urlsp.append('min',min);
    urlsp.append('max',max);
    urlsp.append('id_sem',id_sem);
    urlsp.append('passing_limit',passing_limit);
    return this.http.post('http://'+this.global.host+'/registration/curve.php',urlsp).map((res:Response)=> res.json());
  }

  // getStageSubject(stagetxt){
  //   let urlsp=new URLSearchParams();
  //   urlsp.append('request','getSubjectStage');
  //   urlsp.append('stagetxt',stagetxt);
  //   return this.http.post('http://'+this.global.host+'/registration/curve.php',urlsp).map((res:Response)=> res.json());
  // }

  public update_subject(id_subject:any,id_dep,sub_name:any,selected_stage:any,unit:any,sub_type:any,theory:any,practice:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','updateSubject');
    urlsp.append('id_dep',id_dep);
    urlsp.append('id_subject',id_subject);
    urlsp.append('sub_name',sub_name);
    urlsp.append('selected_stage',selected_stage);
    urlsp.append('unit',unit);
    urlsp.append('sub_type',sub_type);
    urlsp.append('theory',theory);
    urlsp.append('practice',practice);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }

  public delete_subject(id_sub:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','deleteSubject');
    urlsp.append('id_sub',id_sub);
    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
}
