import { Http,Response} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

@Injectable()
export class AdmissionProvider {

  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello AdmissionProvider Provider');
  }

  public addStudent(stu){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addStudents');
    urlsp.append('info',JSON.stringify(stu));
     
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());
  }
  public addFromXLSX(stu,info){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addfromxlsx');
    urlsp.append('stu',JSON.stringify(stu));
    urlsp.append('info',JSON.stringify(info));
     
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res);
  }
  public addDescent(stu_id:any,univ_name:any,dep_name:any,selectedDescentType:any,selectedDescentState:any,id_stage:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','adddescent');
    urlsp.append('stu_id',stu_id);
    urlsp.append('univ_name',univ_name);
    urlsp.append('dep_name',dep_name);
    urlsp.append('selectedDescentType',selectedDescentType);
    urlsp.append('selectedDescentState',selectedDescentState);
    urlsp.append('stage',id_stage);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public addTransfer(stu_id:any,univ_name:any,dep_name:any,id_stage:any,reson:any,id_sem){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addtransfer');
    urlsp.append('stu_id',stu_id);
    urlsp.append('univ_name',univ_name);
    urlsp.append('dep_name',dep_name);
    urlsp.append('stage',id_stage);
    urlsp.append('reson',reson);
    urlsp.append('id_sem',id_sem);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public saveTransferinfo(info){
    let urlsp=new URLSearchParams();
    urlsp.append('request','savetransferinfo');
    urlsp.append('info',JSON.stringify(info));

    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());
  }
  public saveStudentinfo(info){
    let urlsp=new URLSearchParams();
    urlsp.append('request','savestudentinfo');
    urlsp.append('info',JSON.stringify(info));
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());
  }
  public showStudentPayment(id_student:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','showStudentPayment');
    urlsp.append('id_student',id_student);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public addRecursive(stu_id:any,id_stage:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addrecursive');
    urlsp.append('stu_id',stu_id);
    urlsp.append('stage',id_stage);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());
  }
  public addOrdinary(stu_id:any,id_stage:any){
    let urlsp=new URLSearchParams();
    urlsp.append('request','addordinary');
    urlsp.append('stu_id',stu_id);
    urlsp.append('stage',id_stage);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public addPayment(id_student:any,id_semester:any,id_attach:any,id_stage:any,price:any,payment:any,date_of_payment:any){
     let urlsp=new URLSearchParams();
    urlsp.append('request','addpayment');
    urlsp.append('id_student',id_student);
    urlsp.append('id_semester',id_semester);
    urlsp.append('id_attach',id_attach);
    urlsp.append('id_stage',id_stage);
    urlsp.append('price',price);
    urlsp.append('payment',payment);
    urlsp.append('date_of_payment',date_of_payment);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());
  }
   public getStudents(dep_info:any){
  	let urlsp=new URLSearchParams();
  	urlsp.append('request','getstudent');
  	urlsp.append('dep_id',dep_info['id']);
  	return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());
  }
  public getStage(){
    let urlsp=new URLSearchParams();
    urlsp.append('request','getstage');
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public getSemesters(){
    let urlsp=new URLSearchParams();
    urlsp.append('request','semesters');
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public getDepSub(dep_id){
    let urlsp=new URLSearchParams();
    urlsp.append('request','depsubject');
    urlsp.append('id_dep',dep_id);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public OfferSubjects(subjects,stage,sem){
    let urlsp=new URLSearchParams();
    urlsp.append('request','offersubject');
    urlsp.append('subjects',JSON.stringify(subjects));
    urlsp.append('stage',stage);
    urlsp.append('semester',sem);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res);

  }
  public get_Subject_student(id_dep:any,id_sub:any,id_sem){
    let urlsp=new URLSearchParams();
    urlsp.append('request','get_sub_student');
    urlsp.append('id_dep',id_dep);
    urlsp.append('id_sub',id_sub);
    urlsp.append('id_sem',id_sem);

    return this.http.post('http://'+this.global.host+'/registration/subject.php',urlsp).map((res:Response)=> res.json());

  }
  public getOfferedSubjects(sem,dep,stage){
    let urlsp=new URLSearchParams();
    urlsp.append('request','getoffersubject');
    urlsp.append('sem',sem);
    urlsp.append('dep',dep);
    urlsp.append('stage',stage);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public getSemStageStudent(idsem,id_stage,id_dep){
    let urlsp=new URLSearchParams();
    urlsp.append('request','semstagestudent');
    urlsp.append('id_sem',idsem);
    urlsp.append('id_stage',id_stage);
    urlsp.append('dep_id',id_dep);
    
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public getStageStudent(id_stage,id_dep){
    let urlsp=new URLSearchParams();
    urlsp.append('request','stagestudent');
    urlsp.append('id_stage',id_stage);
     urlsp.append('dep_id',id_dep);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public getSemStudent(idsem,id_dep){
    let urlsp=new URLSearchParams();
    urlsp.append('request','semstudent');
    urlsp.append('id_sem',idsem);
    urlsp.append('dep_id',id_dep);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public searchStudent(word){
    let urlsp=new URLSearchParams();
    urlsp.append('request','searchstudent');
    urlsp.append('word',word);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
   public getSelectedStudent(student){
    // console.log(student);
    let urlsp=new URLSearchParams();
    urlsp.append('request','getselectedStudent');
    urlsp.append('student',JSON.stringify(student));
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public getCertificateData(student){
    // console.log(student);
    let urlsp=new URLSearchParams();
    urlsp.append('request','getcertificatedata');
    urlsp.append('student',JSON.stringify(student));
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public addretake(stu_id,stage,id_sem){
    // console.log(student);
    let urlsp=new URLSearchParams();
    urlsp.append('request','retakeclass');
    urlsp.append('info',JSON.stringify({"stu_id":stu_id,"stage":stage,"id_sem":id_sem}));

    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res);

  }
  public addStuNewSem(stu_id,stage,id_sem,rt,oboor, id_old_sem,passing_limit){
    // console.log(student);
    let urlsp=new URLSearchParams();
    urlsp.append('request','addstudenttonewsem');
    urlsp.append('stu_id',stu_id);
    urlsp.append('stage',stage);
    urlsp.append('id_sem',id_sem);
    urlsp.append('id_old_sem',id_old_sem);
    urlsp.append('id_sem',id_sem);
    urlsp.append('oboor',oboor);
    urlsp.append('receive_type',rt);
    urlsp.append('passing_limit',passing_limit);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
   public addRequirement(file,requirement,id){
    // console.log(student);
    let urlsp=new URLSearchParams();
    urlsp.append('request','addrequirement');
    urlsp.append('file',file);
    urlsp.append('requirement',requirement);
    urlsp.append('id',id);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json() );

  }
  public getRequirement(id){
    // console.log(student);
    let urlsp=new URLSearchParams();
    urlsp.append('request','getrequirement');
    urlsp.append('id',id);
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json() );

  }
   public getSemStageSubjects(idsem,id_stage,id_dep){
    let urlsp=new URLSearchParams();
    urlsp.append('request','semstagesubjects');
    urlsp.append('id_sem',idsem);
    urlsp.append('id_stage',id_stage);
    urlsp.append('dep_id',id_dep);
    
    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public deleteoffer(d){
    let urlsp=new URLSearchParams();
    urlsp.append('request','deleteoffersubject');
    urlsp.append('data',JSON.stringify(d));

    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public offerSub(sub,sem,student_id){
    let urlsp=new URLSearchParams();
    urlsp.append('request','offersub');
    urlsp.append('id_sub',sub);
    urlsp.append('id_sem',sem);
    urlsp.append('id_student',student_id);

    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
  public deleteEnroll(d){
    let urlsp=new URLSearchParams();
    urlsp.append('request','deleteenroll');
    urlsp.append('data',JSON.stringify(d));

    return this.http.post('http://'+this.global.host+'/registration/admission.php',urlsp).map((res:Response)=> res.json());

  }
   public upload(fileToUpload: any,new_name:string) {
    let input = new FormData();
    input.append("uploaded", fileToUpload,new_name);

    return this.http.post('http://'+this.global.host+'/registration/uploadbrowser.php',input).map((res:Response)=> res.json());
}
  
}
