import { Http,Response} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

@Injectable()
export class PopularProvider {
//
  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello PopularProvider Provider');
  }

  public add_sem(semster:any,dean:any,reg_liable:any,head_exam:any,supervisor_exam:any){
  	let urlsp = new URLSearchParams();
    urlsp.append('request','addSemster');
  	urlsp.append('semster',semster);
  	urlsp.append('dean',dean);
  	urlsp.append('reg_liable',reg_liable);
  	urlsp.append('head_exam',head_exam);
  	urlsp.append('supervisor_exam',supervisor_exam);

  	return this.http.post('http://'+this.global.host+'/registration/popular.php',urlsp).map((res:Response)=> res.json());
  }
  getSemsterInfo(id_sem){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getSemsterInfo");
    urlsp.append('id_sem',id_sem);
    return this.http.post('http://'+this.global.host+'/registration/popular.php',urlsp).map((res:Response)=> res.json());
  }
  getSemster(){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getSemster");
    return this.http.post('http://'+this.global.host+'/registration/popular.php',urlsp).map((res:Response)=> res.json());
  }
   public delete_semester(id_sem:any){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"deleteSem");
    urlsp.append('id_sem',id_sem);
    return this.http.post('http://'+this.global.host+'/registration/popular.php',urlsp).map((res:Response)=> res.json());
  }

}
