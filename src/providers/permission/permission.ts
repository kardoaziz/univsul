import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

@Injectable()
export class PermissionProvider {

  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello PermissionProvider Provider');
  }

  public addRole(roleName:any){
  	let urlsp= new URLSearchParams();
    urlsp.append('request','add_role');
  	urlsp.append('role',roleName);
  	return this.http.post('http://'+this.global.host+'/registration/permission.php',urlsp).map((res:Response) => res);
  }
  getRole(){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getRole");
    return this.http.post('http://'+this.global.host+'/registration/permission.php',urlsp).map((res:Response)=> res.json());
  }
  getAction(){
    let urlsp= new URLSearchParams();
    urlsp.append('request',"getaction");
    return this.http.post('http://'+this.global.host+'/registration/permission.php',urlsp).map((res:Response)=> res.json());
  }
  update_this_perm(id_role:any,id_action:any,insert:any,select:any,update:any,delet:any){
    let urlsp= new URLSearchParams();
    urlsp.append('request','updatepermission');
    urlsp.append('id_role',id_role);
    urlsp.append('id_action',id_action);
    urlsp.append('insert',insert);
    urlsp.append('select',select);
    urlsp.append('update',update);
    urlsp.append('delete',delet);
    return this.http.post('http://'+this.global.host+'/registration/permission.php',urlsp).map((res:Response)=> res);
  }

}
