import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
//
@Injectable()
export class AddDepartmentProvider {

  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello AddDepartmentProvider Provider');
    console.log(this.global.host);
   
  }
  add_new_dep(dep_name_k:any,dep_name_e:any){
  	let urlsp = new URLSearchParams();
    urlsp.append('request','addDep');
  	urlsp.append('dep_name_k',dep_name_k);
  	urlsp.append('dep_name_e',dep_name_e);
  	return this.http.post('http://'+this.global.host+'/registration/add_department.php',urlsp).map((res:Response)=> res.json());
  }
  add_new_dep_info(head:any,assist:any,depid,id_sem){
    let urlsp = new URLSearchParams();
    urlsp.append('request','addDepinfo');
    urlsp.append('head',head);
    urlsp.append('assist',assist);
    urlsp.append('depid',depid);
    urlsp.append('id_sem',id_sem);
    return this.http.post('http://'+this.global.host+'/registration/add_department.php',urlsp).map((res:Response)=> res.json());
  }
  get_last_dep_info(depid){
    let urlsp = new URLSearchParams();
    urlsp.append('request','getLastDepinfo');
    urlsp.append('depid',depid);
    console.log('http://'+this.global.host+'/registration/add_department.php');
    return this.http.post('http://'+this.global.host+'/registration/add_department.php',urlsp).map((res:Response)=> res.json());
  }
  get_dep_info(depid){
    let urlsp = new URLSearchParams();
    urlsp.append('request','getDepinfo');
    urlsp.append('depid',depid);
    console.log('http://'+this.global.host+'/registration/add_department.php');
    return this.http.post('http://'+this.global.host+'/registration/add_department.php',urlsp).map((res:Response)=> res.json());
  }
  getDepartments(){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getdep");
    return this.http.post('http://'+this.global.host+'/registration/add_department.php',urlsp).map((res:Response)=> res.json());
  }
  delete_department(id_Dep,id_sem){
    let urlsp = new URLSearchParams();
    urlsp.append('request','delete_dep');
    urlsp.append('depid',id_Dep);
    urlsp.append('id_sem',id_sem);
    console.log('http://'+this.global.host+'/registration/add_department.php');
    return this.http.post('http://'+this.global.host+'/registration/add_department.php',urlsp).map((res:Response)=> res.json());
  }
}
