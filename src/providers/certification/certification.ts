import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

/*
  Generated class for the CertificationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CertificationProvider {

  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello CertificationProvider Provider');
  }

  public create_certification(info:any,first_name:any,second_name:any,third_name:any,last_name:any){
  	let urlsp = new URLSearchParams();
    urlsp.append('request',"createcertificate");
    urlsp.append('info',JSON.stringify(info));
    urlsp.append('first_name',first_name);
    urlsp.append('second_name',second_name);
    urlsp.append('third_name',third_name);
    urlsp.append('last_name',last_name);

    return this.http.post('http://'+this.global.host+'/registration/certification.php',urlsp).map((res:Response)=> res.json());

  }
  public getCertificate(word){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"getcertificate");
    urlsp.append('word',word);

    return this.http.post('http://'+this.global.host+'/registration/certification.php',urlsp).map((res:Response)=> res.json());

  }

}
