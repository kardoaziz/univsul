import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {URLSearchParams} from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';

/*
  Generated class for the ReportProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReportProvider {

  constructor(public http: Http,public global:GlobalProvider) {
    console.log('Hello ReportProvider Provider');
  }

  public create_report(info:any){
  	let urlsp = new URLSearchParams();
    urlsp.append('request',"newreport");
    urlsp.append('info',JSON.stringify(info));
    return this.http.post('http://'+this.global.host+'/registration/report.php',urlsp).map((res:Response)=> res.json());



  }
  public reports(){
    let urlsp = new URLSearchParams();
    urlsp.append('request',"reports");
    return this.http.post('http://'+this.global.host+'/registration/report.php',urlsp).map((res:Response)=> res.json());



  }

}
