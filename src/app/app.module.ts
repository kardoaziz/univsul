import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AdmissionPage } from '../pages/admission/admission';
import { CertificationPage } from '../pages/certification/certification';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { EditEnrollsPage } from '../pages/edit-enrolls/edit-enrolls';
import { PermissionPage } from '../pages/permission/permission';
import { ReportPage } from '../pages/report/report';
import { PopularPage } from '../pages/popular/popular';
import { DepartmentPage } from '../pages/department/department';
import { ViewadmissionPage } from '../pages/viewadmission/viewadmission';
import { LoginPage } from '../pages/login/login';
import { InfoPage } from '../pages/info/info';
import { HomeExamcommiteePage } from '../pages/home-examcommitee/home-examcommitee';
import { DegreeStudentPage } from '../pages/degree-student/degree-student';
import { AddDepartmentPage } from '../pages/add-department/add-department';
import { AddSubjectPage } from '../pages/add-subject/add-subject';
import { AddTeacherPage } from '../pages/add-teacher/add-teacher';
import { DepartmentExamcommiteePage } from '../pages/department-examcommitee/department-examcommitee';
import { FirstcoursePage } from '../pages/firstcourse/firstcourse';
import { FinalcoursePage } from '../pages/finalcourse/finalcourse';
import { OfferSubjectPage } from '../pages/offer-subject/offer-subject';
import { SubjectStudentGradePage } from '../pages/subject-student-grade/subject-student-grade';
import { SecondcoursePage } from '../pages/secondcourse/secondcourse';
import { PopularExamcommitteePage } from '../pages/popular-examcommittee/popular-examcommittee';
import {HttpModule} from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { PermissionProvider } from '../providers/permission/permission';
import { HomeProvider } from '../providers/home/home';
import { PopularProvider } from '../providers/popular/popular';
import { AddDepartmentProvider } from '../providers/add-department/add-department';
import { CertificationProvider } from '../providers/certification/certification';
import { ReportProvider } from '../providers/report/report';
import { GlobalProvider } from '../providers/global/global';
import { AdmissionProvider } from '../providers/admission/admission';
import { SubjectProvider } from '../providers/subject/subject';
import { SettingsPage } from '../pages/settings/settings';

import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@NgModule({
  declarations: [
    MyApp,
    AdmissionPage,
    CertificationPage,
    HomePage,
    TabsPage,
    PermissionPage,SettingsPage,
    ReportPage,
    PopularPage,InfoPage,
    DepartmentPage,EditEnrollsPage,
    ViewadmissionPage,
    LoginPage,OfferSubjectPage,
    HomeExamcommiteePage,
    DegreeStudentPage,
    AddDepartmentPage,
    AddSubjectPage,SubjectStudentGradePage,
    AddTeacherPage,
    DepartmentExamcommiteePage,
    FirstcoursePage,
    FinalcoursePage,
    SecondcoursePage,
    PopularExamcommitteePage
  ],
  imports: [
    BrowserModule,HttpModule,
    IonicModule.forRoot(MyApp,{
      tabsPlacement:'top',
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AdmissionPage,
    CertificationPage,SettingsPage,
    HomePage,EditEnrollsPage,
    TabsPage,SubjectStudentGradePage,
    PermissionPage,InfoPage,
    ReportPage,
    PopularPage,
    DepartmentPage,
    ViewadmissionPage,
    LoginPage,OfferSubjectPage,
    HomeExamcommiteePage,
    DegreeStudentPage,
    AddDepartmentPage,
    AddSubjectPage,
    AddTeacherPage,
    DepartmentExamcommiteePage,
    FirstcoursePage,
    FinalcoursePage,
    SecondcoursePage,
    PopularExamcommitteePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthenticationProvider,
    PermissionProvider,
    HomeProvider,
    PopularProvider,
    AddDepartmentProvider,
    CertificationProvider,
    ReportProvider,
    GlobalProvider,
    AdmissionProvider,
    SubjectProvider
  ]
})
export class AppModule {}
