import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinalcoursePage } from './finalcourse';

@NgModule({
  declarations: [
    FinalcoursePage,
  ],
  imports: [
    IonicPageModule.forChild(FinalcoursePage),
  ],
})
export class FinalcoursePageModule {}
