import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SubjectProvider } from '../../providers/subject/subject';
import { GlobalProvider } from '../../providers/global/global';
//
@IonicPage()
@Component({
  selector: 'page-finalcourse',
  templateUrl: 'finalcourse.html',
})
export class FinalcoursePage {
	student_info:any;
	id_student:any;
    id_stage:any;
	id_sub:any;
	student_grade:any;
  	corse1:any;
  	id_sem:any;
    res_final:any;
    level:any;
    list_state:any;
    states:any;
    locked:boolean=true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public sub:SubjectProvider,public global:GlobalProvider) {
  	console.log('this student');
  	console.log(navParams.get("this_student"));
  	this.student_info=navParams.get("this_student");
    this.locked=navParams.get("locked");
    console.log(this.locked);
    this.id_student=this.student_info['id_student'];
  	this.id_stage=navParams.get("id_stage");
    this.id_sem=navParams.get("id_sem");
  	this.sub.get_Stage_Subject(this.id_student,this.id_sem,this.id_stage).subscribe(data=>{
      console.log("student grade");
      console.log(data);
    this.student_grade = data;

   
  })
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinalcoursePage');
  }
 public showinfo()
  {
    this.global.presentModal('finalcourse');
  }
  grade_final(){
    
    let levels = ["کەوتوو","پەسەند","ناوەند","باش","زۆر باش","باڵا"];
    let states = ["کەوتوو","دەرچوو"];
    let res_final=-1;
    // // console.log(i);
    // console.log('student grade');
    //  console.log(this.student_grade);
     for (var i = 0; i <= this.student_grade.length-1; i++) {
      console.log(this.student_grade[i]['offer_type']);
        if(this.student_grade[i]['offer_type']=="crossing")
        {
           res_final=(parseInt(this.student_grade[i]['crossingfinal'])+parseInt(this.student_grade[i]['crossing']));
          console.log(this.student_grade[i]['crossingfinal']+","+parseInt(this.student_grade[i]['crossing'])+"="+res_final);

        }
        else{
           res_final=(parseInt(this.student_grade[i]['final'])+parseInt(this.student_grade[i]['corse1']));
          console.log(this.student_grade[i]['final']+","+parseInt(this.student_grade[i]['corse1'])+"="+res_final);

        }
          this.student_grade[i]['states'] = states[parseInt((res_final/50)+"")];
            // console.log(parseInt(parseInt(res_final+"")/50));
          if (parseInt(parseInt(res_final+"")/50 +"")>0) 
          {
              let m=res_final%50;
              m=parseInt((m/10)+"");
              this.student_grade[i]['level'] = levels[m+1];
          }else{
              this.student_grade[i]['level'] = levels[parseInt((res_final/50)+"")];

          }

    }
           console.log(this.student_grade);
    
    this.sub.addFinal(this.student_grade).subscribe(data=>{
      console.log(data);
      if(data['result']=='success'){
        this.global.presentToast('زیادکرا');

        
        
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە  ');
      }
    })
  }

  calculation(){

  }

}
