import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepartmentExamcommiteePage } from './department-examcommitee';

@NgModule({
  declarations: [
    DepartmentExamcommiteePage,
  ],
  imports: [
    IonicPageModule.forChild(DepartmentExamcommiteePage),
  ],
})
export class DepartmentExamcommiteePageModule {}
