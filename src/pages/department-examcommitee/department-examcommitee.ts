import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirstcoursePage } from '../../pages/firstcourse/firstcourse';
import { FinalcoursePage } from '../../pages/finalcourse/finalcourse';
import { SecondcoursePage } from '../../pages/secondcourse/secondcourse';
import { EditEnrollsPage } from '../../pages/edit-enrolls/edit-enrolls';
import { AdmissionProvider } from '../../providers/admission/admission';
import { SubjectProvider } from '../../providers/subject/subject';
import { GlobalProvider } from '../../providers/global/global';
import { SubjectStudentGradePage } from '../../pages/subject-student-grade/subject-student-grade';
import { AlertController } from 'ionic-angular';
import { Chart } from 'chart.js';

import { PopularProvider } from '../../providers/popular/popular';

@IonicPage()
@Component({
  selector: 'page-department-examcommitee',
  templateUrl: 'department-examcommitee.html',
})
export class DepartmentExamcommiteePage {
  @ViewChild('barCanvas') barCanvas;
  selected_sem:any;
  selected_sem2:any;
  selected_stage:any;
  selected_sub:any;
  selected_stage2:any;
  dep_id:any;
  tobecurved:any;
  plot:boolean=false;
  SelectedStudents=[];
  foundSubjects:any;
  warning:string="";
  semlocked:boolean=false;
  failedTransfer=[];
  barChart: any;
  no_student:any;
  no_failed:any;
  survey:any;
  constructor(public popCtrl:PopularProvider,public sub:SubjectProvider, public alertCtrl:AlertController, public navCtrl: NavController, public navParams: NavParams,public adm:AdmissionProvider,public global:GlobalProvider) {
  console.log("navParams.get('dep')");
  console.log(navParams.get("dep"));
  this.dep_id=navParams.get("dep");
  // this.global.readCsvData();
  this.selected_sem=this.global.semster[this.global.semster.length-1]['year'];
  this.getStudents();
  

  
}
 public showinfo()
  {
    this.global.presentModal('department-examcommitee');
  }
public hideplot()
{
  this.plot=!this.plot;
}
public getSubjects()
{
  let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    console.log("id_sem");
    console.log(id_sem);
    console.log(id_stage);
    console.log(this.dep_id);
  this.adm.getOfferedSubjects(id_sem,this.dep_id.id,id_stage).subscribe(data=>{
    console.log(data);
    this.foundSubjects = data;
  })
}
public showSubStudent(grade:any)
{

    let id_sub=-1;
    this.foundSubjects.forEach(ele=>{
      if(ele['sub_name'] == this.selected_sub)
      {
        id_sub=ele['id_sub'];
      }
    })
     let id_sem=-1;
     // let semlocked=false;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
        if(ele['locked']=="1")
          this.semlocked=true;
      }
    })
    console.log(id_sub);
    console.log(this.dep_id.id);
  this.adm.get_Subject_student(this.dep_id.id,id_sub,id_sem).subscribe(data=>{
    console.log("-------");
    console.log(data);
    
    this.navCtrl.push(SubjectStudentGradePage,{data:data,grade:grade,sem:this.selected_sem,semlock:this.semlocked,sub:this.selected_sub,dep:this.dep_id.name,stage:this.selected_stage});
  })
}
public showResults(x)
{
 let id_sem=-1;
     // let semlocked=false;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
        if(ele['locked']=="1")
          this.semlocked=true;
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    this.popCtrl.getSemsterInfo(id_sem).subscribe(data=>{
      console.log(data);
    var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/results.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "info";
      mapInput.value = JSON.stringify({round:x,sem:this.selected_sem,dep:this.dep_id.name,stage:this.selected_stage}); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "data";
      mapInput2.value = JSON.stringify({id_sem:id_sem,id_dep:this.dep_id.id,id_stage:id_stage}); 
      var mapInput3 = document.createElement("input");
      mapInput3.name = "dep_info";
      mapInput3.value = JSON.stringify(data[0]); 
      console.log(data[0]);
      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);
      mapForm.appendChild(mapInput3);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
    });
     
}
public masterSheet()
{
  let id_sem=-1;
     // let semlocked=false;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
        if(ele['locked']=="1")
          this.semlocked=true;
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/master.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify({
                                      id_dep:this.dep_id.id , 
                                      dep:this.dep_id.name , 
                                      stage:this.selected_stage, 
                                      id_stage:id_stage, 
                                      sem:this.selected_sem,
                                      id_sem:id_sem
                                    }); 
      // var mapInput2 = document.createElement("input");
      // mapInput2.name = "students";
      // mapInput2.value = JSON.stringify(data); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      // mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();

    this.sub.masterSheet(this.dep_id.id,id_sem,id_stage).subscribe(data=>{
      console.log(data);
    })
}
public applyCurve()
{
    let alert = this.alertCtrl.create({
    title: 'Curve',
    inputs: [
      {
        name: 'round',
        placeholder: 'Round'
      },
      {
        name: 'curve',
        placeholder: 'Curve'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Set Curve',
        handler: inputdata => {
           let id_sem=-1;

      this.global.semster.forEach(ele=>{
          if(ele['year'] == this.selected_sem)
          {
            id_sem=ele['id'];
            // j=i+0;
          }
          // i+=1;
        })
      let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
  this.sub.getFailedStudents(this.dep_id.id,id_sem,id_stage ,this.global.settings.passing_limit,inputdata['round']).subscribe(data=>{
        console.log("failed");
        console.log(data);
        if(!data)
          data=[];
       data.forEach(ele=>{
          if(inputdata['round'] ==1)
          { if(parseInt(ele['crossing'])>0)
              ele['result']=parseInt(ele['crossing'])+parseInt(ele["crossingfinal"]);
            else 
              ele['result']=parseInt(ele['corse1'])+parseInt(ele["final"]);
          // let gr = [parseInt(ele['corse1'])+parseInt(ele["final"]),parseInt(ele['corse1'])+parseInt(ele["corse2"]),parseInt(ele['crossing'])+parseInt(ele["crossingfinal"]),parseInt(ele['crossing'])+parseInt(ele["crossing2"])];

          }
            else{
              if(parseInt(ele['crossing'])>0)
              ele['result']=parseInt(ele['crossing'])+parseInt(ele["crossing2"]);
            else 
              ele['result']=parseInt(ele['corse1'])+parseInt(ele["corse2"]);
          
            }
          });
        console.log("with result");
        console.log(data);

        let temp1 = {};
          // jsonek drust akaen bo har studentek listy darsa kawtwakani tedaya
            data.forEach(ele=>{
              if(!temp1[ele['id_student']])
                  temp1[ele['id_student']] = [];
              temp1[ele['id_student']].push(ele);
            
            });
            console.log(temp1);
            // bo har studentek darsakan sort akaen ba pey nzikian la darchunawa
            Object.keys(temp1).forEach(ele=>{
              temp1[ele] = temp1[ele].sort(function(a,b){
              return a.result - b.result;
              });
              temp1[ele].reverse();
            });

            console.log("sorted");
            console.log(temp1);
        
            ///////////////////////////////////////

            let c = parseInt(inputdata['curve']);
            console.log("curve : "+c);
              
              let temp = this.jsonCopy(temp1);
              console.log(temp);
            let helpedStudent = 0;
            let reduceload = 0;
            this.tobecurved = [];
            // darajay qarar dabash akaen bas darsakana agar pey darbchet
            // bo awae bzanin chan dars dar achet baw qarara
            Object.keys(temp).forEach(ele=>{
              console.log("student id");
              console.log(ele);
              console.log(temp[ele]);
              // temp[ele].forEach
              let curve = c+0;
              console.log("curve starts "+curve);
              let ind = [];
              let subject_curves = [];
              let i=0;
              for(let e of temp[ele]){
                console.log(e);
                console.log(50-e["result"]);
                // agar darsaka wabu ka ba qararish dar nachet
                if((50-e["result"])>curve || curve<=0)
                  {
                    console.log("break");
                    break;
                  }
                else{
                  // agar darsaka ba qarar darchu 
                  subject_curves.push(50-e["result"]);
                  curve-=(50-e["result"]);
                  ind.push(i);
                }
                // aw bra daraja qarary ka mawa pash awae darsek sudy le abinet 
                console.log("curve = "+curve);
                i+=1;

              }
              // aw darsanay ka ba qarar darchua aisrinawa la listaka
              if(ind.length>0)
              {
                let helpeds=false;
                if (ind.length == temp[ele].length||(inputdata['round']=="2" && (temp[ele].length-ind.length)<=this.global.settings.oboor))
                {
                  this.SelectedStudents.push(temp[ele][0]['id_student']);
                  helpeds=true;
                }
                // else if ((temp[ele].length-ind.length)<=this.global.settings.oboor)
                // {
                //   this.SelectedStudents.push(temp[ele][0]['id_student']);
                //   helpeds=true;
                // }
                // let k=0;
                  while(ind.length) {
                    let m = ind.pop();
                    let n = subject_curves.pop();
                    // k+=1;
                    temp[ele][m]['help'] = n;
                    if(helpeds)
                    this.tobecurved.push(temp[ele][m]);
                        temp[ele].splice(m, 1);
                    }
              }
              // agar xwendkarek ba qararaka hamu darsakan darchu, aizhmerin
              if(temp[ele].length==0)
                helpedStudent+=1;
              else if(temp[ele].length<=this.global.settings.oboor){
                reduceload+=1;
              }
            });
            console.log(temp);
            console.log("helpedStudent");
            console.log(helpedStudent);
            console.log("reduceload");
            console.log(reduceload);
            console.log('tobecurved');
            console.log(this.tobecurved);
            this.sub.applyCurve(this.tobecurved).subscribe(data=>{
              console.log(data);
              this.global.presentToast('بە سەرکەوتووی ئەنژام درا ');
            })
          });
          }
      }
    ]
  });
  alert.present();
            // survey.push({curve:c,helped:helpedStudent});
            ///////////////////////////////////////
          // });
}
public clearCurve()
{
  let prompt = this.alertCtrl.create({
    title: 'Curve Analyze for ',
    message: ''+this.selected_sem,
    inputs : [
    
    {
        type:'number',
        name:'round',
        placeholder:'خول ١ یان ٢'
    }],
    buttons : [
    {
        text: "پاشگه‌ز بونه‌وه‌",
        handler: data => {
        console.log("cancel clicked");
        }
    },
    {
        text: "سرینه‌وه‌",
        handler: inp => {
          let id_sem=-1;
  // let no_student=-1;
this.survey=[];
      this.global.semster.forEach(ele=>{
          if(ele['year'] == this.selected_sem)
          {
            id_sem=ele['id'];
            // j=i+0;
          }
          // i+=1;
        })
let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    this.sub.clearCurve(this.dep_id.id,id_sem,id_stage,inp['round']).subscribe(data=>{
      console.log(data);
      this.global.presentToast('بە سەرکەوتوویی ئەنجام درا');

    })
        }}]
      });
  prompt.present();
}
public analyzeCurve()
{
  
            let prompt = this.alertCtrl.create({
    title: 'Curve Analyze for ',
    message: ''+this.selected_sem,
    inputs : [
    
    {
        type:'number',
        name:'round',
        placeholder:'خول ١ یان ٢'
    },
    {
          name: 'min',
          type: 'number',
          placeholder: 'كه‌مترین بریار'
        },
    {
          name: 'max',
          type: 'number',
          placeholder: 'زۆرترین بریار'
        }],
    buttons : [
    {
        text: "پاشگه‌ز بونه‌وه‌",
        handler: data => {
        console.log("cancel clicked");
        }
    },
    {
        text: "پشاندان",
        handler: inp => {
          let id_sem=-1;
  // let no_student=-1;
this.survey=[];
      this.global.semster.forEach(ele=>{
          if(ele['year'] == this.selected_sem)
          {
            id_sem=ele['id'];
            // j=i+0;
          }
          // i+=1;
        })
let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
      this.sub.getFailedStudents(this.dep_id.id,id_sem,id_stage,this.global.settings.passing_limit,inp['round']).subscribe(data=>{
        console.log("---=----");
        console.log(data);
         if(!data)
          data=[];
         this.sub.no_student(this.dep_id.id,id_sem,id_stage,this.global.settings.passing_limit).subscribe(data=>{
         this.no_student=data['no_student'];
         this.no_failed=data['no_failed'];
         this.survey.push({curve:0,helped:0});
          console.log("---=----");
          console.log(data);
          console.log(this.no_student);
        });
        data.forEach(ele=>{
          if(inp['round'] ==1)
          { if(parseInt(ele['crossing'])>0)
              ele['result']=parseInt(ele['crossing'])+parseInt(ele["crossingfinal"]);
            else 
              ele['result']=parseInt(ele['corse1'])+parseInt(ele["final"]);
          // let gr = [parseInt(ele['corse1'])+parseInt(ele["final"]),parseInt(ele['corse1'])+parseInt(ele["corse2"]),parseInt(ele['crossing'])+parseInt(ele["crossingfinal"]),parseInt(ele['crossing'])+parseInt(ele["crossing2"])];

          }
            else{
              if(parseInt(ele['crossing'])>0)
              ele['result']=parseInt(ele['crossing'])+parseInt(ele["crossing2"]);
            else 
              ele['result']=parseInt(ele['corse1'])+parseInt(ele["corse2"]);
          
            }

          // console.log(gr);
          // console.log(Math.max.apply(Math,gr))
          // ele["result"]=  Math.max.apply(Math,gr);
        })
        console.log("with result");
        console.log(data);

        let temp1 = {};
          // jsonek drust akaen bo har studentek listy darsa kawtwakani tedaya
            data.forEach(ele=>{
              if(!temp1[ele['id_student']])
                  temp1[ele['id_student']] = [];
              temp1[ele['id_student']].push(ele);
            
            });
            console.log(temp1);
            // bo har studentek darsakan sort akaen ba pey nzikian la darchunawa
            Object.keys(temp1).forEach(ele=>{
              temp1[ele] = temp1[ele].sort(function(a,b){
              return a.result - b.result;
              });
              temp1[ele].reverse();
            });

            console.log("sorted");
            console.log(temp1);
            
          console.log(inp);
            for (let c=parseInt(inp['min']); c<=parseInt(inp['max']);c++)
            {
              console.log("curve : "+c);
              
              let temp = this.jsonCopy(temp1);
              console.log(temp);
            let helpedStudent = 0;
            this.tobecurved = [];
            // darajay qarar dabash akaen bas darsakana agar pey darbchet
            // bo awae bzanin chan dars dar achet baw qarara
            Object.keys(temp).forEach(ele=>{
              // console.log("student id");
              // console.log(ele);
              // temp[ele].forEach
              let curve = c+0;
              let ind = [];
              let subject_curves = [];
              let i=0;
              for(let e of temp[ele]){
                // agar darsaka wabu ka ba qararish dar nachet
                if((50-e["result"])>curve || curve<=0)
                  {
                    // console.log("break");
                    break;
                  }
                else{
                  // agar darsaka ba qarar darchu 
                  subject_curves.push(50-e["result"]);
                  curve-=50-e["result"];
                  ind.push(i);
                }
                // aw bra daraja qarary ka mawa pash awae darsek sudy le abinet 
                // console.log("curve : "+curve);
                i+=1;

              }
              // aw darsanay ka ba qarar darchua aisrinawa la listaka
              // console.log("remove indexs");
              // console.log(ind);
              if(ind.length>0)
              {
                let helpeds=false;
                if (ind.length== temp[ele].length ||(inp['round']=="2" && (temp[ele].length-ind.length)<=this.global.settings.oboor))
                {
                  this.SelectedStudents.push(temp[ele][0]['id_student']);
                  helpeds=true;
                }
                // let k=0;
                  while(ind.length) {
                    let m = ind.pop();
                    let n = subject_curves.pop();
                    // k+=1;
                    temp[ele][m]['help'] = n;
                    if(helpeds)
                    this.tobecurved.push(temp[ele][m]);
                        temp[ele].splice(m, 1);
                    }
                
                

              }
              // console.log("after remove");
              // console.log(temp[ele]);
              // agar xwendkarek ba qararaka hamu darsakan darchu, aizhmerin
              if(temp[ele].length<=this.global.settings.oboor)
                helpedStudent+=1;
            });
            console.log(temp);
            console.log(helpedStudent);
            console.log('tobecurved');
            console.log(this.tobecurved);
            this.survey.push({curve:c,helped:helpedStudent});
          }
          console.log(this.survey);
          console.log(this.SelectedStudents);
          ////////////////////////////////////////// pshandany chart
          this.showplot(this.survey);
          //////////////////////////////////////////
       });  }

      }
      ]
      });
            prompt.present();
            // console.log(temp.filter(i => i === 2).length;);

      // })
 
  
}
public showplot(survey)
{
  this.hideplot();
    setTimeout(() => {
   this.barChart = new Chart(this.barCanvas.nativeElement, {

            type: 'bar',
            data: {
                labels: survey.map(function(value) {
  return value.curve;
}),
                datasets: [{
                    label: 'بریار',
                    data: survey.map(function(value) {
  return value.helped;
}),
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(0,255,0, 0.2)',
                        'rgba(0, 0, 255, 0.2)',
                        'rgba(128,128,0, 0.2)',
                        'rgba(255,0,0, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(0, 255, 0, 1)',
                        'rgba(0, 0, 255, 1)',
                        'rgba(128,128,0, 1)',
                        'rgba(255,0,0, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }

        });},1000);
}
public jsonCopy(src) {
  return JSON.parse(JSON.stringify(src));
}
public unlockSemester()
{
  let alert =  this.alertCtrl.create({
      title: 'Confirm!',
      message: 'دلنیایت ئه‌ته‌وێت سالی خوێندنی <strong>'+this.selected_sem+'</strong> بكه‌یته‌وه‌!!!?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
                            console.log("Unlock Semster");
                            let id_sem=-1;
                            let j =-1;
                            let i = 0;
                            this.global.semster.forEach(ele=>{
                                if(ele['year'] == this.selected_sem)
                                {
                                  id_sem=ele['id'];
                                  j=i+0;
                                }
                                i+=1;
                              })
                            this.sub.unlockSemester(id_sem).subscribe(data=>{
                                                console.log(data);
                                                if(data['result']=="success")
                                                {
                                                  this.global.semster[j]['locked']="0";
                                                  this.getStudents();
                                                }
                                                console.log(this.global.semster);
                                              })
                           }
              }
            ]
    });

     alert.present();

}
public lockSemester()
{
  console.log("Lock Semster");
   let alert =  this.alertCtrl.create({
      title: 'Confirm!',
      message: 'دلنیایت ئه‌ته‌وێت سالی خوێندنی  <strong>'+this.selected_sem+'</strong> قوفڵ بكه‌یت!!! ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
                            let id_sem=-1;
                            let j =-1;
                            let i = 0;
                            this.global.semster.forEach(ele=>{
                                if(ele['year'] == this.selected_sem)
                                {
                                  id_sem=ele['id'];
                                  j=i+0;
                                }
                                i+=1;
                              })
                            this.sub.lockSemester(id_sem).subscribe(data=>{
                              console.log(data);
                              if(data['result']=="success")
                              {
                                this.global.semster[j]['locked']="1";
                                this.getStudents();
                              }
                              console.log(this.global.semster);

                            })

                            console.log('Confirm Okay');
          }
        }
      ]
    });

     alert.present();

}
public transferSelectedStudents()
{
  this.failedTransfer=[];
  let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem2)
      {
        id_sem=ele['id'];
      }
    })
     let id_old_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_old_sem=ele['id'];
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage2)
      {
        id_stage=ele['id_stage'];
      }
    });
    let id_old_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_old_stage=ele['id_stage'];
      }
    });
    
  this.SelectedStudents.forEach(i=>{
     console.log(this.global.shownstudent[i]['id_student']);
    this.adm.addStuNewSem(this.global.shownstudent[i]['id_student'],id_stage,id_sem,this.global.shownstudent[i]['recieve_type'],this.global.settings.oboor,id_old_sem,this.global.settings.passing_limit).subscribe(data=>{
      console.log(data);
      if(data['result']!="success")
      {
        this.failedTransfer.push(this.global.shownstudent[i]['id_student']);
        console.log(this.failedTransfer);
        console.log("add retake");

        this.adm.addretake(this.global.shownstudent[i]['id_student'],id_old_stage,id_sem).subscribe(data=>{
          console.log(data);
        });
      }
    })
  })
}
public addStuNewSem(i)
{
  let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem2)
      {
        id_sem=ele['id'];
      }
    })
    let id_old_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_old_sem=ele['id'];
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage2)
      {
        id_stage=ele['id_stage'];
      }
    })
    let id_old_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_old_stage=ele['id_stage'];
      }
    })
    console.log("before passing students ");
    console.log(id_stage);
    console.log(id_sem);
    console.log(id_old_sem);
    console.log(this.global.shownstudent[i]['id_student']);
    if(id_stage>-1 && id_sem>-1)
      {
    this.adm.addStuNewSem(this.global.shownstudent[i]['id_student'],id_stage,id_sem,this.global.shownstudent[i]['recieve_type'],this.global.settings.oboor,id_old_sem,this.global.settings.passing_limit).subscribe(data=>{
      console.log(data);
      if(data['result']!="success")
      {this.global.presentToast("Failed to Transfer Student due to lot of failed class");
     this.failedTransfer.push(this.global.shownstudent[i]['id_student']);
        console.log(this.failedTransfer);
      console.log("add retake");

        this.adm.addretake(this.global.shownstudent[i]['id_student'],id_old_stage,id_sem).subscribe(data=>{
          console.log(data);
        });}
    else
      this.global.presentToast("Student Passed Successfully");

    })
  }
}
public getStudents()
{
  this.getSubjects();
  console.log(this.dep_id['id']);
   let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
        if(ele['locked']=="1")
        {
          this.warning = "This Semster is Locked, You can NOT enter marks or make any change";
          this.semlocked = true;
        }
        else{
          this.semlocked = false;
          this.warning = "";
        }
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    console.log(id_sem);
    console.log(id_stage);
  if(this.selected_stage && this.selected_sem)
  {
    console.log("get sem stage student");
    this.adm.getSemStageStudent(id_sem,id_stage,this.dep_id['id']).subscribe(data=>{
console.log(data);
      this.global.student=data;
this.global.shownstudent=data;
    })
  }
  else if(!this.selected_stage && this.selected_sem)
  {
    console.log("get sem   student");
    this.adm.getSemStudent(id_sem,this.dep_id['id']).subscribe(data=>{
console.log(data);
      this.global.student=data;
this.global.shownstudent=data;
    })
  }
  else if(this.selected_stage && !this.selected_sem)
  {
    console.log("get stage student");
    this.adm.getStageStudent(id_stage,this.dep_id['id']).subscribe(data=>{
console.log(data);
      this.global.student=data;
this.global.shownstudent=data;
    })
  }
}
public getitems(param:any)
  {
    let val = param.target.value;
    if (val && val.trim() != '') {

      this.global.shownstudent = this.global.student.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.state.toLowerCase().indexOf(val.toLowerCase())>-1|| item.stage_txtEn.toLowerCase().indexOf(val.toLowerCase())>-1);
      });
  }else {
      this.global.shownstudent = this.global.student;

    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DepartmentExamcommiteePage');
  }
  firstcourse(j:number){
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
  this.navCtrl.push(FirstcoursePage,{id_sem:id_sem, id_stage:id_stage,  this_student : this.global.student[j],locked:this.semlocked});
  console.log("id_student");
  console.log(this.global.student[j]['id_student']);
  }

  finalcourse(j:number){
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
    this.navCtrl.push(FinalcoursePage,{id_sem:id_sem,id_stage:id_stage,this_student : this.global.student[j],locked:this.semlocked });
    console.log("id_student");
    console.log(this.global.student[j]['id_student']);
  }
  secondcourse(j:number){
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
    this.navCtrl.push(SecondcoursePage,{id_sem:id_sem,id_stage:id_stage,this_student : this.global.student[j],locked:this.semlocked });
    console.log("id_student");
    console.log(this.global.student[j]['id_student']);
  }
  public selectall()
  {
    for(let i=0;i<this.global.shownstudent.length;i++)
    {
      this.selectStudent(i);
    }
  }
  public selectStudent(i)
{
  console.log(i);
  
  if(this.SelectedStudents.indexOf(this.global.shownstudent[i]['id_student']) > -1)
  {
    this.SelectedStudents.splice(this.SelectedStudents.indexOf(this.global.shownstudent[i]['id_student']), 1);
    this.global.shownstudent[i]['selected']=0;
  }
  else {
    this.SelectedStudents.push(this.global.shownstudent[i]['id_student']);
    this.global.shownstudent[i]['selected']=1;
  }
  console.log(this.global.shownstudent[i]);
}
public editEnroll(i)
{
  let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
  this.navCtrl.push(EditEnrollsPage,{student: this.global.shownstudent[i],id_sem:id_sem,dep_id:this.dep_id});
}
public showRanking(x)
{
  let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
    
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
  this.sub.stuStageGrades(this.dep_id.id,id_sem,id_stage).subscribe(data=>{
      console.log(data);
      let studentgrades=data['grades'];
      let totalunit=data['totalunit'];
      let grades={};
      studentgrades.forEach(ele=>{
        if(parseInt(ele['corse2'])==0 && parseInt(ele['crossing2'])==0)
        {
          if(parseInt(ele['crossing'])!=0){
            console.log("crossing")
            ele['graderate']=(parseInt(ele['crossing'])+parseInt(ele['crossingfinal'])+parseInt(ele['curve']))*(parseInt(ele['unit'])/totalunit);
          }
          else{
            ele['graderate']=(parseInt(ele['corse1'])+parseInt(ele['final'])+parseInt(ele['curve']))*(parseInt(ele['unit'])/totalunit);
          }
           if(!grades[ele['id_student']]){
            grades[ele['id_student']]={f_name:ele['f_name'],m_name:ele['m_name'],s_name:ele['s_name'],l_name:ele['l_name'],grade:0};
           }
           grades[ele['id_student']]['grade']+=ele['graderate'];
             }
           });
       
        
            console.log(studentgrades);
            console.log(grades);
             var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/ranking.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify({dep:this.dep_id.name , 
                                      stage:this.selected_stage, 
                                      sem:this.selected_sem, grade:x}); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "students";
      mapInput2.value = JSON.stringify(grades); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
          });
}
public showStudents()
{
  let dep="";
  let unit="";
  let ut="";
  let up="";
    this.global.dep_name.forEach(ele=>{
      if(ele['id'] == this.dep_id.id)
      {
        dep=ele['namee'];
      }
    })
     let id_sub=-1;
    this.foundSubjects.forEach(ele=>{
      if(ele['sub_name'] == this.selected_sub)
      {
        id_sub=ele['id_sub'];
        unit=ele['unit'];
        ut=ele['h_theory'];
        up=ele['h_practice'];
      }
    })
     let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    console.log(dep);
    if(id_sub==-1)
    {
 this.adm.getSemStageStudent(id_sem,id_stage,this.dep_id.id).subscribe(data=>{
    
    console.log("-------");
    console.log(data);
      var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/studentsName.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify({dep:dep , 
                                      stage:this.selected_stage, 
                                      sub:'', 
                                      teacher:"",
                                      unit:unit,
                                      ht :ut,
                                      hp:up,
                                      pre_final:this.global.settings.pre_final,
                                      sem:this.selected_sem}); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "students";
      mapInput2.value = JSON.stringify(data); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
  });
    }else{
     this.adm.get_Subject_student(this.dep_id.id,id_sub,id_sem).subscribe(data=>{
    
    console.log("-------");
    console.log(data);
      var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/studentsName.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify({dep:dep , 
                                      stage:this.selected_stage, 
                                      sub:this.selected_sub, 
                                      teacher:"",
                                      unit:unit,
                                      ht :ut,
                                      hp:up,
                                      sem:this.selected_sem}); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "students";
      mapInput2.value = JSON.stringify(data); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
  });
    // console.log(this.global.shownstudent);

 }
 
   
}
public showCurved()
 {
   var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/reportPrint.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify({sem:this.selected_sem,dep:this.dep_id.name,sub:"بریاره‌كان",pre_final:this.global.settings.pre_final,final:this.global.settings.final}); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "students";
      mapInput2.value = JSON.stringify(this.tobecurved); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
 }
 public showCurvedStatus()
 {
   var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/curveStatus.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "info";
      mapInput.value = JSON.stringify({sem:this.selected_sem,dep:this.dep_id.name,no_student:this.no_student,no_failed:this.no_failed,stage:this.selected_stage}); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "data";
      mapInput2.value = JSON.stringify(this.survey); 
      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);
      // Add the form to dom
      document.body.appendChild(mapForm);
      // Just submit
      mapForm.submit();
 }

}
