import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopularExamcommitteePage } from './popular-examcommittee';

@NgModule({
  declarations: [
    PopularExamcommitteePage,
  ],
  imports: [
    IonicPageModule.forChild(PopularExamcommitteePage),
  ],
})
export class PopularExamcommitteePageModule {}
