import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddDepartmentPage } from '../../pages/add-department/add-department';
import { AddSubjectPage } from '../../pages/add-subject/add-subject';
import { AddTeacherPage } from '../../pages/add-teacher/add-teacher';
import { PopularProvider } from '../../providers/popular/popular';
import { GlobalProvider } from '../../providers/global/global';
import { AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-popular-examcommittee',
  templateUrl: 'popular-examcommittee.html',
})
export class PopularExamcommitteePage {
  semster:any;dean:any;reg_liable:any;headExamComm:any;examSupervisor:any;
  current_semester:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public popular:PopularProvider,public global:GlobalProvider, public alertCtrl:AlertController) {
    console.log("current_semster constructor");
    this.getSemster();
  }
 public showinfo()
  {
    this.global.presentModal('popular-examcommittee');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PopularExamcommitteePage');
  }
  add_department(){
  this.navCtrl.push(AddDepartmentPage);
  }
  add_subject(){
  this.navCtrl.push(AddSubjectPage);
  }
  add_teacher(){
  this.navCtrl.push(AddTeacherPage);
  }
  add_semster(){
    this.popular.add_sem(this.semster,this.dean,this.reg_liable,this.headExamComm,this.examSupervisor).subscribe(data=>{
      console.log(data);
      if (data['result']=="success") {
        this.current_semester=data['id_semester'];
        this.getSemster();
        this.global.presentToast('بە سەرکەوتوویی زیادکرا ');
        this.semster=null;
        this.dean=null;
        this.reg_liable=null;
        this.headExamComm=null;
        this.examSupervisor=null;
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
      }
    })
  }
  
delete_sem(j:number){

  const alert = this.alertCtrl.create({
      title: 'ئاگادارکردنەوە !',
      message: 'دڵنیایت ئەتەوێت ئەم وەرزە بسڕیتەوە؟',
      buttons: [
        {
          text: 'نەخێر',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'بەڵێ',
          handler: () => {
            let id_sem="";
            this.global.semster.forEach(ele=>{
              if(ele['id']==this.global.semster[j]['id']){
                id_sem = ele['id'];
              }
             })
              this.popular.delete_semester(id_sem).subscribe(data=>{
                console.log(data);
                if (data['result']=="success") {
                  this.global.presentToast('بەسەرکەوتویی جێبەجێ کرا');
                }else{
                  this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
                }
              })
          }
        }
      ]
    });
     alert.present();
  }
  public getSemster(){
    this.popular.getSemster().subscribe(data=>{
      console.log(data);
     this.global.semster=data;
    })
  }

}
