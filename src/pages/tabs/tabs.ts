import { Component } from '@angular/core';
import {  Tabs, Events,NavController,NavParams } from 'ionic-angular';

import { AdmissionPage } from '../admission/admission';
import { CertificationPage } from '../certification/certification';
import { HomePage } from '../home/home';
import { ReportPage } from '../report/report';
import { HomeExamcommiteePage } from '../home-examcommitee/home-examcommitee';
import { DegreeStudentPage } from '../degree-student/degree-student';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
	tab1Root = HomePage;
	tab2Root = CertificationPage;
	tab3Root = AdmissionPage;
	tab4Root = ReportPage;
    tab5Root = HomeExamcommiteePage;
    tab6Root = DegreeStudentPage;
  constructor(public global:GlobalProvider) {

    console.log("==============");
  	console.log(global.permissions);

  }
   }
