import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { DepartmentPage } from '../../pages/department/department';
import { PopularProvider } from '../../providers/popular/popular';
import { GlobalProvider } from '../../providers/global/global';

@IonicPage()
@Component({
  selector: 'page-popular',
  templateUrl: 'popular.html',
})
export class PopularPage {
  semster:any;dean:any;reg_liable:any;headExamComm:any;examSupervisor:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public popular:PopularProvider,public global:GlobalProvider) {
    this.getSemster();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopularPage');
  }
  home(){
  this.navCtrl.push(HomePage);
  }
  selectChangeHandler (event: any) {
  this.navCtrl.push(DepartmentPage);
  }
  add_semster(){
    this.popular.add_sem(this.semster,this.dean,this.reg_liable,this.headExamComm,this.examSupervisor).subscribe(data=>{
      console.log(data);
      if (data['result']=="success") {
        this.global.presentToast('بەسەرکەوتوویی زیادکرا ');
        this.getSemster();
        this.semster=null;
        this.dean=null;
        this.reg_liable=null;
        this.headExamComm=null;
        this.examSupervisor=null;
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دووبارەی بکەرەوە');
      }
    })
  }
  public getSemster(){
    this.popular.getSemster().subscribe(data=>{
      console.log(data);
     this.global.semster=data;
    })
  }
}
