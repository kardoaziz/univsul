import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { DepartmentExamcommiteePage } from '../../pages/department-examcommitee/department-examcommitee';
import { GlobalProvider } from '../../providers/global/global';
import {AuthenticationProvider} from '../../providers/authentication/authentication';
import { LoginPage } from '../../pages/login/login';
import { SettingsPage } from '../../pages/settings/settings';

@IonicPage()
@Component({
  selector: 'page-degree-student',
  templateUrl: 'degree-student.html',
})
export class DegreeStudentPage {

  constructor(public app: App, public authCtrl:AuthenticationProvider,public navCtrl: NavController, public navParams: NavParams,public global:GlobalProvider) {
    console.log("inside degree student");
    console.log("this.global.dep_name");
    console.log(this.global.dep_name);
    let k=-1;

    this.global.dep_name.forEach(ele=>{
      k++;
      if(ele['namee'] == this.global.currentUser_dep)
      {

        this.global.showEmpDep=this.global.currentUser_dep;
        this.openDep(k);
       console.log(ele['namee']);
      }else{
        this.global.showEmpDep='';
      }
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DegreeStudentPage');
  } 
  public openDep(i:number)
  {
    this.navCtrl.push(DepartmentExamcommiteePage,{dep : this.global.dep_name[i] });
  }
  public logout()
  {
    this.authCtrl.logout();
    this.app.getRootNav().setRoot(LoginPage);
  }
 public showinfo()
  {
    this.global.presentModal('degree-student');
  }
  public settings()
  {
    this.global.presentModal('degree-student');
  }
}
