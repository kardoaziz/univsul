import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DegreeStudentPage } from './degree-student';

@NgModule({
  declarations: [
    DegreeStudentPage,
  ],
  imports: [
    IonicPageModule.forChild(DegreeStudentPage),
  ],
})
export class DegreeStudentPageModule {}
