import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddSubjectPage } from '../../pages/add-subject/add-subject';
import { AddTeacherPage } from '../../pages/add-teacher/add-teacher';
import { PopularExamcommitteePage } from '../../pages/popular-examcommittee/popular-examcommittee';
import { AddDepartmentProvider } from '../../providers/add-department/add-department';
import { GlobalProvider } from '../../providers/global/global';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-add-department',
  templateUrl: 'add-department.html',
})
export class AddDepartmentPage {
  dep_name_k:any;dep_name_e:any;
  dep_head:any;
  dep_info:any;
  dep_assistance:any;
  selected_department:any;
  selected_sem:any;
  result_depinfo:any;
  constructor(public viewCtrl:ViewController, public navCtrl: NavController, public navParams: NavParams,public dep:AddDepartmentProvider, public global:GlobalProvider, public alertCtrl:AlertController) {
    this.getDepartments();
   // this.getDepartmentsInfo();
    // console.log("semster inside add department page");
    // console.log(this.global.semster['id']);
  }
 public showinfo()
  {
    this.global.presentModal('add-department');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddDepartmentPage');
  }
  popular_examcommittee(){
  this.navCtrl.push(PopularExamcommitteePage);
  }
  add_subject(){
  this.navCtrl.push(AddSubjectPage);
  }
  add_teacher(){
  this.navCtrl.push(AddTeacherPage);
  }//
  add_new_department(){

    this.dep.add_new_dep(this.dep_name_k,this.dep_name_e).subscribe(data=>{
      console.log(data);
      if (data['result']=="success") {
        this.getDepartments();
        this.global.presentToast('زیادکرا');
        this.dep_name_k=null;
        this.dep_name_e=null;
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە  ');
      }
    })
  }
  public add_Dep_Info()
  {
    let depid=-1;
    this.global.dep_name.forEach(ele=>{
      if(ele['name'] == this.selected_department)
      {
        depid=ele['id'];
      }
    })
     let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
    console.log(depid);
    console.log(this.selected_department);
     console.log(id_sem);
    console.log(this.selected_sem);
    this.dep.add_new_dep_info(this.dep_head,this.dep_assistance,depid,id_sem).subscribe(data=>{
      console.log(data);
      if (data['result']=="success") {
        this.getDepartmentsInfo();
        this.global.presentToast('زیادکرا');
        this.dep_head=null;
        this.dep_assistance=null;
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە  ');
      }
    })
    // lera nawe sarok bash u bryadar aw awana insert bka bo tablek
  }
  public getDepartmentsInfo()
  {
    // Lera nawe sarok bash u bryardar aw awana get bka bo pshandan 
  let depid=-1;
    this.global.dep_name.forEach(ele=>{
      if(ele['name'] == this.selected_department)
      {
        depid=ele['id'];
      }
    })
  this.dep.get_dep_info(depid).subscribe(data=>{
      console.log(data);

       this.result_depinfo=data;
       console.log(this.result_depinfo);
      if (data) {
        this.dep_info = data;
      }else{
        console.log("empty");
          const alert =  this.alertCtrl.create({
          title: 'ئاگادارکردنەوە !',
          message: 'هیچ زانیاریەک دەربارەی ئەم بەشە داخڵ نەکراوە.',
          buttons: ['OK']
        });
         alert.present();  
      }
      
    });
  }
  public getDepartments(){
    this.dep.getDepartments().subscribe(data=>{
      console.log(data);
     this.global.dep_name=data;
    })
  }
  public onClickCancel() {
    this.viewCtrl.dismiss();
  }

  delete_dep(j:number){
  const alert = this.alertCtrl.create({
      title: 'ئاگادارکردنەوە !',
      message: 'دڵنیایت ئەتەوێت بەشی '+this.dep_info[j]['dep_namek']+"بسڕیتەوە ؟",
      buttons: [
        {
          text: 'نەخێر',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'بەڵێ',
          handler: () => {
            let id_dep="";
            this.dep_info.forEach(ele=>{
              if(ele['id_dep']==this.dep_info[j]['id_dep']){
                id_dep = ele['id_dep'];

                console.log("test");
                console.log(id_dep);
                console.log("test");
              }
             })
              let id_sem=-1;
              this.dep_info.forEach(ele=>{
              if(ele['id_sem']==this.dep_info[j]['id_sem']){
                id_sem = ele['id_sem'];

                console.log("test");
                console.log(id_sem);
                console.log("test");
              }
             })
              this.dep.delete_department(id_dep,id_sem).subscribe(data=>{
                console.log(data);
                if (data['result']=="success") {
                  this.global.presentToast('بەسەرکەوتویی جێبەجێ کرا');
                }else{
                  this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
                }
              })
          }
        }
      ]
    });
     alert.present();
  }
}
