import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController,ToastController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
// import { PriviledgeProvider } from '../../providers/priviledge/priviledge';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})

export class SettingsPage {
  @ViewChild("fileInput") fileInput;


  edited:boolean=false;
  constructor(public sanitizer: DomSanitizer,public toastCtrl:ToastController, public global:GlobalProvider, public navCtrl: NavController, public navParams: NavParams) {
  // this.url_="http://localhost/clinic/recipt_2.php?settings="+JSON.stringify(this.global_.reports);
  // this.url=this.sanitizer.bypassSecurityTrustResourceUrl(this.url_);
  // console.log(this.url);
  }
  public showinfo()
  {
    this.global.presentModal('settings');
  }
  public settingChanged()
  {
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

}
