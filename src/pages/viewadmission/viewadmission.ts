import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdmissionPage } from '../../pages/admission/admission';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the ViewadmissionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewadmission',
  templateUrl: 'viewadmission.html',

})
export class ViewadmissionPage {
  public show:  boolean;
  constructor(public global:GlobalProvider,  public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewadmissionPage');
  }
 public showinfo()
  {
    this.global.presentModal('viewadmission');
  }
  admission(){
  this.navCtrl.push(AdmissionPage);
  }
   public decentparallel(){
   this.show=!this.show;
  }

}
