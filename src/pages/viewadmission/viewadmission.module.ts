import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewadmissionPage } from './viewadmission';

@NgModule({
  declarations: [
    ViewadmissionPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewadmissionPage),
  ],
})
export class ViewadmissionPageModule {}
