import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { ReportProvider } from '../../providers/report/report';
import { LoginPage } from '../../pages/login/login';
import {AuthenticationProvider} from '../../providers/authentication/authentication';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  public hidee: boolean;
  public newreport: boolean=false;
  public usereport: boolean=false;
  public info={code:"",title:"",body:"",direct:"",follow:""};
  code:any;select_report:any;direct:any;date:any;body:any;follow:any;
  public reports=[];
  constructor( public global:GlobalProvider, public app:App,public authCtrl:AuthenticationProvider, public navCtrl: NavController, public navParams: NavParams,public report:ReportProvider) {
    this.report.reports().subscribe(data=>{
      console.log(data);
      this.reports=data;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }

  addnewreport()
  {
    this.newreport=true;
  }
  insertnewreport()
  {
    this.report.create_report(this.info).subscribe(data=>{
      console.log(data);
      if(data['result']=="success")
      {
        this.newreport=false;
        this.info={code:"",title:"",body:"",direct:"",follow:""};
      }
    });
  }
  home(){
  this.navCtrl.push(HomePage);
  }
   public hide()
  {
  this.hidee=!this.hidee;
  }
  create_rep(){
    // this.report.create_report(this.code,this.select_report,this.direct,this.date,this.body,this.follow).subscribe(data=>{
    // })
  }
  public logout()
  {
    this.authCtrl.logout();
    // this.navCtrl.setRoot(LoginPage);
            this.app.getRootNav().setRoot(LoginPage);

  }
  selectReport(i)
  {
    this.info=this.reports[i];
    this.usereport=true;
  }
  viewReport()
  {
    var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/viewReport.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify(this.info); 
      var mapInput2 = document.createElement("input");
      // mapInput2.name = "students";
      // mapInput2.value = JSON.stringify(this.tobecurved); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      // mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
  }
}
