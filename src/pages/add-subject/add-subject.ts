import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PopularExamcommitteePage } from '../../pages/popular-examcommittee/popular-examcommittee';
import { AddDepartmentPage } from '../../pages/add-department/add-department';
import { AddTeacherPage } from '../../pages/add-teacher/add-teacher';
import { GlobalProvider } from '../../providers/global/global';
import { SubjectProvider } from '../../providers/subject/subject';
import { OfferSubjectPage } from '../../pages/offer-subject/offer-subject';
import { AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-add-subject',
  templateUrl: 'add-subject.html',
})
export class AddSubjectPage {
  sub_name:any;selected_dep:any;selected_stage:any;sub_type:any;theory:any;practice:any;unit:any;
  subject:any;
  id:any;
  id_dep:any;
  show_update_btn:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public sub:SubjectProvider,public global:GlobalProvider, public alertCtrl:AlertController) {
    this.show_update_btn="false";
    this.getSubjects();
  }
   public showinfo()
  {
    this.global.presentModal('add-subject');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddSubjectPage');
  }

  popular_examcommittee(){
  this.navCtrl.push(PopularExamcommitteePage);
  }

  add_department(){
  this.navCtrl.push(AddDepartmentPage);
  }
  offerSubject(){
  this.navCtrl.push(OfferSubjectPage);
  }

  add_teacher(){
  this.navCtrl.push(AddTeacherPage);
  }
  addSubject(){
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.selected_dep)
      {
        id_dep = ele['id'];
      }
    })
    this.sub.add_subject(id_dep,this.sub_name,this.selected_stage,this.unit,this.sub_type,this.theory,this.practice).subscribe(data=>{
      if(data['result']=='success'){
        console.log(data);
      this.getSubjects();
      this.global.presentToast('زیادکرا');
      id_dep=null;
      this.sub_name=null;
      this.selected_stage=null;
      this.unit=null;
      this.sub_type=null;
      this.theory=null;
      this.practice=null;
    }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە  ');
    }
      
      
    })
  }
  public getSubjects(){
    this.sub.getSubject(this.selected_dep).subscribe(data=>{
      console.log(data);
     this.subject=data;
    })
  }

  //  getSubjectStage(){
  //   console.log("inside ion chanege")
  //   let stagetxt="";
  //   this.subject.forEach(ele=>{
  //     //console.log(ele);
  //     if(ele['stage']==this.selected_stage)
  //     {
  //       stagetxt=ele['stage']
  //     }
  //   })
  //   this.sub.getStageSubject(stagetxt).subscribe(data=>{
  //     if(data['result']=='success'){
  //       console.log(data);
  //     }
  //   })

  // }

  get_subject_info_toUpdate(j:number){
     this.id="";
    this.subject.forEach(ele=>{
      //console.log(ele);
      if(ele['id']==this.subject[j]['id'])
      {
        this.id=ele['id'];
        this.sub_name=this.subject[j]['sub_name'];
        this.selected_dep=this.subject[j]['dep_namek'];
        this.id_dep=this.subject[j]['dep_namek'];
        this.selected_stage=this.subject[j]['stage'];
        this.sub_type=this.subject[j]['sub_type'];
        this.theory=this.subject[j]['h_theory'];
        this.practice=this.subject[j]['h_practice'];
        this.unit=this.subject[j]['unit'];
        this.show_update_btn="true";
        console.log(this.subject[j]);
      }
    })

  }

  update_subject(){
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.selected_dep)
      {
        id_dep = ele['id'];
      }
    })

    this.sub.update_subject(this.id,id_dep,this.sub_name,this.selected_stage,this.unit,this.sub_type,this.theory,this.practice).subscribe(data=>{
      if(data['result']=='success'){
        console.log(data);
        this.getSubjects();
        this.global.presentToast('بە سەرکەوتوویی ئەنجام درا');
        id_dep=null;
        this.sub_name=null;
        this.selected_stage=null;
        this.unit=null;
        this.sub_type=null;
        this.theory=null;
        this.practice=null;
      }else{
          this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە  ');
      }  
    })
  }

  delete_Subject(j:number){
    const alert = this.alertCtrl.create({
      title: 'ئاگادارکردنەوە !',
      message: 'دڵنیایت ئەتەوێت   '+this.subject[j]['sub_name']+'ە بسڕیتەوە ؟',
      buttons: [
        {
          text: 'نەخێر',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'بەڵێ',
          handler: () => {
            this.id="";
            this.subject.forEach(ele=>{
              if(ele['id']==this.subject[j]['id']){
                this.id=ele['id'];
              }
            });
            
            this.sub.delete_subject(this.id).subscribe(data=>{
              console.log(data);
              if (data['result']=="success") {
                this.global.presentToast('بەسەرکەوتویی جێبەجێ کرا');
              }else{
                this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
              }
            })
          }
        }
      ]
    });
     alert.present();
  }
}

