import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { AdmissionProvider } from '../../providers/admission/admission';
import { GlobalProvider } from '../../providers/global/global';
import { SubjectProvider } from '../../providers/subject/subject';


@IonicPage()
@Component({
  selector: 'page-department',
  templateUrl: 'department.html',
})
export class DepartmentPage {
selectedStage:string="First";

student_info:any;
  id_student:any;
    id_stage:any;
  id_sub:any;
  student_grade:any;
  dep:any;
  selected_sem:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public adm:AdmissionProvider,public sub:SubjectProvider,public global:GlobalProvider) {
  console.log(navParams.get("dep"));
  this.dep=navParams.get("dep");
  this.showStudentsByStage();
  // this.adm.getStudents(navParams.get("dep")).subscribe(data=>{
  //   console.log("student department");
  //   console.log(data);
  //   this.global.student = data;
  //   this.global.shownstudent = data;
  // });
  // console.log('this student');
  //   console.log(navParams.get("this_student"));
  //   this.student_info=navParams.get("this_student");
  //   this.id_student=this.student_info['id_student'];
  //   this.id_stage=this.student_info['id_stage'];
  //   this.sub.get_Stage_Subject(this.id_student).subscribe(data=>{
  //     console.log(data);
  //   this.student_grade = data;
  //   })
  }
 // public showinfo()
 //  {
 //    this.global.presentModal('department');
 //  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DepartmentPage');
  }
  home(){
  this.navCtrl.push(HomePage);
  }
  selectChangeHandler (event: any) {
  this.navCtrl.push(DepartmentPage);
  //this.buttonColor = '#345465'; 
  }
  public getItems(param:any)
  {
    // console.log(this.global.student);
    let val = param.target.value;
    if (val && val.trim() != '') {

      this.global.shownstudent = this.global.student.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.state.toLowerCase().indexOf(val.toLowerCase())>-1|| item.stage_txtEn.toLowerCase().indexOf(val.toLowerCase())>-1);
      });
  }else {
      this.global.shownstudent = this.global.student;

    }
  }
   public showinfo()
  {
    this.global.presentModal('certification');
  }
  openDep(i:number){
    this.navCtrl.setRoot(DepartmentPage,{dep : this.global.dep_name[i] });
  }
  public showStudentsByStage()
  {
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxtE'] == this.selectedStage)
      {
        id_stage=ele['id_stage'];
      }
    })
    console.log(id_stage)
    console.log(this.dep['id'])
    if(!this.selected_sem)
    {

      this.adm.getStageStudent(id_stage, this.dep['id']).subscribe(data=>{
        console.log("data");
        console.log(data);
        this.global.student=data;
        this.global.shownstudent=data;
      })
    }
    else{
      // console.log(this.global.semster);
          let id_sem=-1;
        this.global.semster.forEach(ele=>{
          // console.log(ele);
          if(ele['year'] == this.selected_sem)
          {
            id_sem=ele['id'];
          }});
          this.adm.getSemStageStudent(id_sem,id_stage,this.dep['id']).subscribe(data=>{
    console.log(data);
          this.global.student=data;
          this.global.shownstudent=data;
        })
    }
    // console.log(this.selectedStage);
  }
}
