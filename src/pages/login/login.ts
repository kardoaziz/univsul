import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';
import {AuthenticationProvider} from '../../providers/authentication/authentication';
import { AddDepartmentProvider } from '../../providers/add-department/add-department';
import { GlobalProvider } from '../../providers/global/global';
import { PermissionProvider } from '../../providers/permission/permission';
import { PopularProvider } from '../../providers/popular/popular';
import { AdmissionProvider } from '../../providers/admission/admission';
import { Platform } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  isActiveToggleTextPassword: Boolean = true;
  info = {'user':'','password':''};
  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams,public auth:AuthenticationProvider, public global:GlobalProvider,public dep:AddDepartmentProvider,public perm:PermissionProvider,public popular:PopularProvider,public adm:AdmissionProvider) {
    
    if(localStorage.getItem("regUsername")!="")
    {
      console.log("Login --");
    this.info.user=localStorage.getItem("regUsername");
    this.info.password=localStorage.getItem("regPassword");
    this.tabs();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    // if(localStorage.getItem("usernameR") && localStorage.getItem("passwordR"))
    // {
    //     this.info.user = localStorage.getItem("usernameR");
    //     this.info.password = localStorage.getItem("passwordR");
    //     console.log("this.info.user");
    //     console.log(this.info.user);
    //     console.log(this.info.password);
    //     this.tabs();
    // }
  }
  
  public toggleTextPassword(): void{
      this.isActiveToggleTextPassword = (this.isActiveToggleTextPassword==true)?false:true;
  }
  public getType() {
      return this.isActiveToggleTextPassword ? 'password' : 'text';
  }

  tabs(){
    console.log(this.info.user);
    // console.log(this.info.password);
 // this.navCtrl.setRoot(TabsPage);
    // this.auth.login(this.info).subscribe(data=>{
this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     
    this.auth.login(this.info.user,this.info.password).subscribe(data=>{
      console.log(data);

      console.log("current user department");
      this.global.currentUser_dep=data['userinfo'][0]['dep'];
      this.global.currentUser_dep_id=data['userinfo'][0]['id_dep'];
      console.log(this.global.currentUser_dep);
      console.log(this.global.currentUser_dep_id);
      
      if(data['result'] == "success")
        {
          this.getDepartments();
          this.global.currentUser = data['userinfo'][0];
          localStorage.setItem("usernameR",this.info.user);
          localStorage.setItem("passwordR",this.info.password);

          if(this.global.currentUser_dep_id==1){
            //get registration action
            console.log("current user dep = 1")
              this.auth.getPermission(data['userinfo'][0]['id_role']).subscribe(data=>{
                console.log("registration permissions");
                console.log(data);
                let temp = {};
                data.forEach(ele=>{
                  temp[ele['action_type']] = {select:ele['viewp']=="1",update:ele['updatep']=="1",delete:ele['deletep']=="1",insert:ele['insertp']=="1"};
                });
                this.global.permissions = temp;
                console.log(this.global.permissions);
                 localStorage.setItem("regUsername",this.info.user);
                  localStorage.setItem("regPassword",this.info.password);
                  this.getRole();
                  this.getSemster();
                  this.getStage();
              this.navCtrl.setRoot(TabsPage);
              })
            }
          else
          {

            // get Exam committee permission
            this.auth.getPermissionExam(data['userinfo'][0]['id_role']).subscribe(data=>{
              console.log("Exam permissions");
              console.log(data);
              let temp = {};
              data.forEach(ele=>{
                temp[ele['action_type']] = {select:ele['viewp']=="1",update:ele['updatep']=="1",delete:ele['deletep']=="1",insert:ele['insertp']=="1"};
              });
              this.global.permissions = temp;
              console.log(this.global.permissions);
              localStorage.setItem("regUsername",this.info.user);
              localStorage.setItem("regPassword",this.info.password);
              this.getRole();
              this.getSemster();
              this.getStage();
              this.navCtrl.setRoot(TabsPage);
            })
          }
        }
        else{
            this.global.presentToast("ئەم ئەندامە بەردەست نیە ");
          }

    });
     // statusBar.styleDefault();
     //  splashScreen.hide();
    });
  }
  public getDepartments(){
    this.dep.getDepartments().subscribe(data=>{
      console.log("DEpartments:");
      console.log(data);
     this.global.dep_name=data;
    })
  }
  public getRole(){
    this.perm.getRole().subscribe(data=>{
      console.log("Roles :");
      console.log(data);
     this.global.role_name=data;
    })
  }
  public getSemster(){
    this.popular.getSemster().subscribe(data=>{
      console.log("semesters :");
      console.log(data);
     this.global.semster=data;
    })
  }
  public getStage(){
    this.adm.getStage().subscribe(data=>{
      console.log("stage :");
      console.log(data);
      this.global.stage=data;
    })
  }

}
