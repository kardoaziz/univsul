import { Component } from '@angular/core';
import { IonicPage, NavParams,ToastController,NavController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
// import { PriviledgeProvider } from '../../providers/priviledge/priviledge';
// import { DomSanitizer } from '@angular/platform-browser';
import {ViewController} from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'info',
  templateUrl: 'info.html',
})

export class InfoPage {
  selectedItems:any;
location:string="add-subject";
information={
"add-department"
                            :"لێره‌دا ده‌توانین به‌شی نوێ زیاد بكه‌ین \n وه‌ ده‌توانی بۆ هه‌ر وه‌رزێك سه‌رۆك به‌ش و بریارده‌ری به‌ش دیاری بكه‌ین",
"add-subject"
                            :"لێره‌دا ده‌توانین وانه‌ی نوێ زیاد بكه‌ین بۆ به‌ش و قۆناغێكی دیاری كراو \n وه‌ ده‌توانی وانه‌كانی هه‌ر به‌شێك ببینین دوای دیاری كردنی به‌شه‌كه‌",
"add-teacher"
                            :"لێره‌دا ده‌توانین مامۆستا زیاد بكه‌ین بۆ هه‌ر به‌شێك  \n ده‌توانی مامۆستاكانی هه‌ر به‌شێك ببینین دوای دیاری كردنی به‌شه‌كه‌",
"admission"
                            :"لێره‌دا ده‌توانین خوێندكاری نوێ تۆمار بكه‌ین یاخود گۆرانكاری له‌ زانیاری خوێندكاره‌كه‌دا بكه‌یب. \n ده‌توانین فایلی داواكاریه‌كان هاوپێچ بكه‌ین له‌گه‌ل خوێندكاره‌كه‌دا. ",
"certification"
                            :"لێره‌دا ده‌توانین بڕوانامه‌  بۆ خوێندكار دروست بكه‌ین ئه‌گه‌ر تۆمار كرابێت و نمره‌ی بۆ داخل كرابێت",
"degree-student"
                            :"degree-student",
"department"
                            :"department",
"department-examcommitee"
                            :"department-examcommitee",
"edit-enroll"
                            :"edit-enroll",
"finalcourse"
                            :"finalcourse",
"firstcourse"
                            :"firstcourse",
"home"
                            :"home",
"home-examcommitee"
                            :"home-examcommitee",
"login"
                            :"login",
"offer-subject"
                            :"offer-subject",
"permission"
                            :"permission",
"popular"
                            :"popular",
"popular-examcommittee"
                            :"popular-examcommittee",
"report"
                            :"report",
"secondcourse"
                            :"secondcourse",
"settings"
                            :"settings",
"subject-student-grade"
                            :"لێره‌دا نمره‌ی وانه‌یه‌كی دیاری كراو بۆ سه‌رجه‌م خوێندكاره‌كان تۆمار ده‌كه‌ین یاخود ده‌یبینین. \n ئه‌گه‌ر نه‌توانرا گۆراناری له‌ نمره‌كاندا بكرێت واته‌ رێگه‌ت پێ نه‌دراوه‌ بۆ ئه‌و كاره‌. \n بۆ وردبینی ده‌توانین تێبینی بنوسین له‌سه‌ر نمره‌یه‌ك ئه‌گه‌ر هه‌ڵه‌یه‌كی تێدا بوو.",
"viewAdmission"
                            :"viewAdmission",
}
  edited:boolean=false;
  constructor(public viewCtrl:ViewController, public toastCtrl:ToastController,  public navCtrl: NavController, public navParams: NavParams) {
    this.location=this.navParams.get("location");
    console.log(this.location);
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad Info page');
  }
  public dismiss()
  {
  	this.viewCtrl.dismiss(this.selectedItems);
  }

}
