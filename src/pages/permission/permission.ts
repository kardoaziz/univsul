import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { PermissionProvider } from '../../providers/permission/permission';
import { GlobalProvider } from '../../providers/global/global';
import {AuthenticationProvider} from '../../providers/authentication/authentication';

@IonicPage()
@Component({
  selector: 'page-permission',
  templateUrl: 'permission.html',
})
export class PermissionPage {
  role:any;
  action_name:any;
  this_person:any;
  this_permission:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public perm :PermissionProvider, public global:GlobalProvider,public auth:AuthenticationProvider) {
    this.getAction();
    this.getRole();
    
    console.log("1 person");
    if(navParams.get("person")){
      this.this_person=navParams.get("person");
      console.log(this.this_person);
      console.log("this.global.currentUser_dep_id");
      console.log(this.global.currentUser_dep_id);
      
      if (this.global.currentUser_dep_id==1) {
        this.auth.getPermission(this.this_person['id_role']).subscribe(data=>{
          console.log("permissions");
          let i=0;
          data.forEach(ele=>{
            data[i]['insertp'] = data[i]['insertp']=="1";
            data[i]['viewp'] = data[i]['viewp']=="1";
            data[i]['deletep'] = data[i]['deletep']=="1";
            data[i]['updatep'] = data[i]['updatep']=="1";
            i+=1; 
          });
            console.log("this permission");
            console.log(data);
            this.this_permission = data;
        })
      }
      else {
        this.auth.getPermissionExam(this.this_person['id_role']).subscribe(data=>{
            console.log("permissions");
            let i=0;
            data.forEach(ele=>{
              data[i]['insertp'] = data[i]['insertp']=="1";
              data[i]['viewp'] = data[i]['viewp']=="1";
              data[i]['deletep'] = data[i]['deletep']=="1";
              data[i]['updatep'] = data[i]['updatep']=="1";
             
              i+=1;  
            });
            console.log("this permission");
            console.log(data);
            this.this_permission = data; 
          })
      }
  }
    
  }
  public chbchanged(i)
  {
    console.log(this.this_permission[i]);
    this.perm.update_this_perm(this.this_permission[i].id_role,this.this_permission[i].id_action,this.this_permission[i].insertp,this.this_permission[i].viewp,this.this_permission[i].updatep,this.this_permission[i].deletep).subscribe(data=>{
      if(data['result']=='success'){
        this.global.presentToast('Updated');
      }
    })
  }
 public showinfo()
  {
    this.global.presentModal('permission');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PermissionPage');
    this.getAction();
  }

  home(){
  this.navCtrl.setRoot(HomePage);
  }

  add_role(){
    this.perm.addRole(this.role).subscribe(data=>{
      if(data['result']=='success'){
        this.getRole();
        this.global.presentToast('بەسەرکەوتووی زیادکرا');
        this.role=null;
      }else{
        // this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە ');
      }
  })
  }
  public getRole(){
    this.perm.getRole().subscribe(data=>{
      console.log(data);
     this.global.role_name=data;
    })
  }
  public getAction(){
    this.perm.getAction().subscribe(data=>{
      this.global.actions=data;
      console.log("actions ")
      console.log(this.global.actions);

    })
  }
  public getPermission(i)
  {
    this.auth.getPermission(this.global.role_name[i]['id']).subscribe(data=>{
            console.log("permissions");
            let i=0;
            data.forEach(ele=>{
              data[i]['insertp'] = data[i]['insertp']=="1";
              data[i]['viewp'] = data[i]['viewp']=="1";
              data[i]['deletep'] = data[i]['deletep']=="1";
              data[i]['updatep'] = data[i]['updatep']=="1";
              i+=1; 
            });
            console.log("this permission");
            console.log(data);
            this.this_permission = data;
          })
  }
}
