import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirstcoursePage } from './firstcourse';

@NgModule({
  declarations: [
    FirstcoursePage,
  ],
  imports: [
    IonicPageModule.forChild(FirstcoursePage),
  ],
})
export class FirstcoursePageModule {}
