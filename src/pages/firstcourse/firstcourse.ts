import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SubjectProvider } from '../../providers/subject/subject';
import { GlobalProvider } from '../../providers/global/global';

////
@IonicPage()
@Component({
  selector: 'page-firstcourse',
  templateUrl: 'firstcourse.html',
})
export class FirstcoursePage {
	student_info:any;
  id_student:any;
  id_stage:any;
	id_sub:any;
	student_grade:any;
  corse1:any;
  id_sem:any;
  locked:boolean=true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public sub:SubjectProvider,public global:GlobalProvider) {
  	console.log('this student');
  	console.log(navParams.get("this_student"));
  	this.student_info=navParams.get("this_student");
    this.locked=navParams.get("locked");
    console.log(this.locked);
    this.id_student=this.student_info['id_student'];
  	this.id_stage=navParams.get("id_stage");
    this.id_sem=navParams.get("id_sem");
  	this.sub.get_Stage_Subject(this.id_student,this.id_sem,this.id_stage).subscribe(data=>{
      
      console.log("-=-=--");
      console.log(data);
    this.student_grade = data;
  })
  }
 public showinfo()
  {
    this.global.presentModal('firstcourse');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstcoursePage');
  }
  grade_corse1(i:number){
    
    console.log(i);
    console.log('student grade');
     console.log(this.student_grade);
    this.sub.addcorse1(this.student_grade).subscribe(data=>{
      console.log(data);
      if(data['result']=='success'){
        this.global.presentToast('زیادکرا');
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە  ');
      }
    })
  }

}
