import { Component } from '@angular/core';
import * as papa from 'papaparse';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { SubjectProvider } from '../../providers/subject/subject';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the SubjectStudentGradePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subject-student-grade',
  templateUrl: 'subject-student-grade.html',
})
export class SubjectStudentGradePage {
students:any;
round:any;
sem:any;
sub:any;
grade:any;
stage:any;
dep:any;
semlocked:boolean=false;
semlock:boolean=false;
  constructor(public alertCtrl:AlertController, public sub_:SubjectProvider, public global:GlobalProvider, public navCtrl: NavController, public navParams: NavParams) {
 	console.log(this.navParams.get("data"));
 	console.log(this.navParams.get("grade"));
 	this.grade = this.navParams.get("grade");
 	this.sem = this.navParams.get("sem");
 	this.sub = this.navParams.get("sub");
  this.dep = this.navParams.get("dep");
  this.stage = this.navParams.get("stage");
  this.semlock = this.navParams.get("semlock");
 	this.round= this.global.settings.grades[this.grade];
 	this.students = this.navParams.get("data");
  let i=0;
  if(this.students)
  {

 	this.students.forEach(ele=>{
    console.log(i);
 		ele['corse1']  = parseInt(ele['corse1']);
 		ele['final']= parseInt(ele['final']);
 		ele['corse2']= parseInt(ele['corse2']);
 		ele['crossing']= parseInt(ele['crossing']);
 		ele['crossing2']= parseInt(ele['crossing2']);
 		ele['crossingfinal']= parseInt(ele['crossingfinal']);
    ele['curve']= parseInt(ele['curve']);
    // if(this.grade=="corse2")
    //   ele['gcorse1']= ele['corse1']+ele['final']+ele['curve'];
    this.gradeChanged(i);
    i+=1;
 	});
  if(this.grade=="corse2"){
   this.students = this.students.filter((item) => {
        return ((item['corse1']+item['final']+item['curve'])<this.global.settings.passing_limit);
      });
  }
  }
  console.log(this.students);
  }
 public showinfo()
  {
    this.global.presentModal('subject-student-grade');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SubjectStudentGradePage');
  }
  public gradeChanged(i)
  {
    if(this.students[i]['offer_type'] == "crossing")
    {
      if(this.grade=="corse1" || this.grade=="final")
        this.students[i]['result'] = parseInt(this.students[i]['crossing'])+parseInt(this.students[i]['crossingfinal'])+parseInt(this.students[i]['curve']);
      else
        this.students[i]['result'] = parseInt(this.students[i]['crossing'])+parseInt(this.students[i]['crossing2'])+parseInt(this.students[i]['curve']);
    }
    else{
      if(this.grade=="corse1" || this.grade=="final")
        this.students[i]['result'] = parseInt(this.students[i]['corse1'])+parseInt(this.students[i]['final'])+parseInt(this.students[i]['curve']);
      else
        this.students[i]['result'] = parseInt(this.students[i]['corse1'])+parseInt(this.students[i]['corse2'])+parseInt(this.students[i]['curve']);
    }
  }
  public saveNotes()
  {
    this.sub_.saveNotes(this.students).subscribe(data=>{
      console.log(data);

    });
  }
  public saveGrades()
  {
  	console.log(this.students);
  	this.sub_.add_sub_grades(this.students,this.grade).subscribe(data=>{
  		console.log(data);

  	});
  }
  public dismiss()
  {
  	this.navCtrl.pop();
  }
  public export()
{
  let alert = this.alertCtrl.create({
    title: 'File Name ',
    enableBackdropDismiss:false,
    inputs: [

      {
        name: 'Name',
        placeholder: 'Name',
        type:'text'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');

        }
      },
      {
        text: 'Download',
        handler: data => {
          if(data['Name']){
           this.global.extractData(this.students,data['Name']);
          }
         else 
          { this.global.presentToast("Please Enter File Name");
            return false;}
          }
      }
    ]
  });
  alert.present();
}
public printStudent()
  {
    console.log("before print");
    console.log(this.students);
       var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/reportPrint.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify({sem:this.sem,dep:this.dep,sub:this.sub,stage:this.stage,pre_final:this.global.settings.pre_final,final:this.global.settings.final,grade:this.grade}); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "students";
      mapInput2.value = JSON.stringify(this.students); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
  }

}
