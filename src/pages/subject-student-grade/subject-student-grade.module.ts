import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubjectStudentGradePage } from './subject-student-grade';

@NgModule({
  declarations: [
    SubjectStudentGradePage,
  ],
  imports: [
    IonicPageModule.forChild(SubjectStudentGradePage),
  ],
})
export class SubjectStudentGradePageModule {}
