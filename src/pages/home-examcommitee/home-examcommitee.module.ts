import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeExamcommiteePage } from './home-examcommitee';

@NgModule({
  declarations: [
    HomeExamcommiteePage,
  ],
  imports: [
    IonicPageModule.forChild(HomeExamcommiteePage),
  ],
})
export class HomeExamcommiteePageModule {}
