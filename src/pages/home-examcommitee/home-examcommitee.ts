import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { PermissionPage } from '../../pages/permission/permission';
// import { PopularPage } from '../../pages/popular/popular';
import { AddDepartmentPage } from '../../pages/add-department/add-department';
import { AddSubjectPage } from '../../pages/add-subject/add-subject';
import { LoginPage } from '../../pages/login/login';
import { AddTeacherPage } from '../../pages/add-teacher/add-teacher';
import { PopularExamcommitteePage } from '../../pages/popular-examcommittee/popular-examcommittee';
import { HomeProvider } from '../../providers/home/home';
import { GlobalProvider } from '../../providers/global/global';
import {AuthenticationProvider} from '../../providers/authentication/authentication';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the HomeExamcommiteePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-examcommitee',
  templateUrl: 'home-examcommitee.html',
})
export class HomeExamcommiteePage {
   public hidee: boolean;
   selected_dep:any;
   selected_role:any;
   firstN:any;midN:any;thirdN:any;phoneN:any;gender:any;;email:any;address:any;username:any;pass:any;
  show_updateBtn:any;
  person_id:any;
  constructor(public app:App, public authCtrl:AuthenticationProvider, public navCtrl: NavController, public navParams: NavParams,public home:HomeProvider, public global:GlobalProvider, public alertCtrl:AlertController) {
    this.show_updateBtn="";
    this.getMembers();
  }
 public showinfo()
  {
    this.global.presentModal('home-examcommitee');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeExamcommiteePage');
  }
  permission(j:number){
  this.navCtrl.push(PermissionPage,{person:this.global.persons[j]});
  }
  public hide()
  {
  this.hidee=!this.hidee;
  }
  add_department(){
  this.navCtrl.push(AddDepartmentPage);
  }
  add_subject(){
  this.navCtrl.push(AddSubjectPage);
  }
  add_teacher(){
  this.navCtrl.push(AddTeacherPage);
  }
  popular_examcommittee(){
  this.navCtrl.push(PopularExamcommitteePage);
  }
  add_member(){
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.selected_dep)
      {
        id_dep = ele['id'];
      }
    })

    let id_role="";
    this.global.role_name.forEach(ele=>{
      console.log(ele);
      if(ele['roleName']==this.selected_role){
        id_role=ele['id'];
      }
    })
    this.home.add_persons(this.firstN,this.midN,this.thirdN,this.gender,this.phoneN,this.email,this.address,id_dep,this.username,this.pass,this.selected_dep,id_role).subscribe(data=>{
    console.log (data);
    if(data['result']=='success'){
      this.getMembers();
      this.global.presentToast('بە سەرکەوتویی  زیاد کرا ');
      this.firstN=null;
      this.midN=null;
      this.thirdN=null;
      this.gender=null;
      this.phoneN=null;
      this.email=null;
      this.address=null;
      id_dep=null;
      this.username=null;
      this.pass=null;
      this.selected_dep=null;
      id_role=null;
    }else{
      this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
    }
    })
  }
  public getMembers(){
    this.home.getMember().subscribe(data=>{
      console.log(data);
     this.global.persons=data;
    })
  }

  delete_members(j:number){
    const alert = this.alertCtrl.create({
      title: 'ئاگادارکردنەوە !',
      message: 'دڵنیایت ئەتەوێت ئەم  ئەندامە بسڕیتەوە؟',
      buttons: [
        {
          text: 'نەخێر',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'بەڵێ',
          handler: () => {
            let id_person="";
    this.global.persons.forEach(ele=>{
      if(ele['id_person']==this.global.persons[j]['id_person'])
      {
        id_person = ele['id_person'];
        console.log(id_person);
      }
    })
    this.home.delete_member(id_person).subscribe(data=>{
      console.log(data);
      if (data['result']=="success") {
        this.global.presentToast('بەسەرکەوتویی جێبەجێ کرا');
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
      }
    })         
          }
        }
      ]
    });
     alert.present();
  }


    updateEmployee(j:number){
      console.log("inside update employee");
      let id_person="";
      this.global.persons.forEach(ele=>{
        if(ele['id_person']==this.global.persons[j]['id_person'])
        {
          id_person = ele['id_person'];
          console.log(id_person);
        }
      });
  }







  public logout()
  {
    this.authCtrl.logout();
    // this.navCtrl.setRoot(LoginPage);
            this.app.getRootNav().setRoot(LoginPage);
  }
  public onClickCancel() {
    // this.viewCtrl.dismiss();
  }

  get_info_to_update(j:number){
    this.show_updateBtn="true";
    let id_role="";
    this.global.role_name.forEach(ele=>{
      console.log(ele);
      if(ele['roleName']==this.selected_role){
        id_role=ele['id'];
      }
    })
    console.log("inside update employee");
      this.person_id="";
      this.global.persons.forEach(ele=>{
        if(ele['id_person']==this.global.persons[j]['id_person']){
          this.person_id = ele['id_person'];
          console.log(this.person_id);
          this.firstN= this.global.persons[j]['f_name'];
          // console.log(this.firstN);
          this.midN=this.global.persons[j]['m_name'];
          // console.log(this.midN);
          this.thirdN=this.global.persons[j]['l_name'];
          // console.log(this.thirdN);
          this.gender=this.global.persons[j]['gender'];
          // console.log(this.gender);
          this.phoneN=this.global.persons[j]['phone'];
          this.email=this.global.persons[j]['email'];
          this.address=this.global.persons[j]['address'];
          this.username=this.global.persons[j]['username'];
          this.pass=this.global.persons[j]['password'];
          // console.log(this.pass);
          this.selected_role=this.global.persons[j]['role_name'];
          id_role=this.global.persons[j]['id_role'];
          console.log(this.global.persons[j]);
          console.log(id_role);
        }
      });
  }

  updateMember(){

    let id_role="";
    this.global.role_name.forEach(ele=>{
      console.log(ele);
      if(ele['roleName']==this.selected_role){
        id_role=ele['id'];
      }
    })
    this.home.update_employees(this.person_id,this.firstN,this.midN,this.thirdN,this.gender,this.phoneN,this.email,this.address,this.username,this.pass,id_role,this.selected_role).subscribe(data=>{
      console.log (data);
      if(data['result']=='success'){
        this.getMembers();
        this.global.presentToast('بە سەرکەوتوویی ئەنجام درا');
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
      }
    })
    
  }
}
