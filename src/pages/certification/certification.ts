import { Component,ViewChild } from '@angular/core';
import {App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { CertificationProvider } from '../../providers/certification/certification';
import { LoginPage } from '../../pages/login/login';
import {AuthenticationProvider} from '../../providers/authentication/authentication';
import { AdmissionProvider } from '../../providers/admission/admission';
import { GlobalProvider } from '../../providers/global/global';
import { SettingsPage } from '../../pages/settings/settings';
import { InfoPage } from '../../pages/info/info';
import { ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
//
@IonicPage()
@Component({
  selector: 'page-certification',
  templateUrl: 'certification.html',
})
export class CertificationPage {
  @ViewChild("fileInput") fileInput;

  public hidee: boolean;
  id_cert:any;date:any;
  selectedStudent:any;
  create_date:any;
  round:any;
  order_no:any;
  total_student:any;
  file_name:any;
  uploadedImages:any;
  qty:any;
  certifications:any;
  first_name:any;second_name:any;third_name:any;last_name:any;

  constructor(public alertCtrl:AlertController, public modalCtrl: ModalController, public app:App, public authCtrl:AuthenticationProvider, public navCtrl: NavController, public navParams: NavParams,public cert:CertificationProvider,public adm:AdmissionProvider,public global:GlobalProvider) {
  
  this.cert.getCertificate("").subscribe(data=>{
    console.log(data);
    this.certifications=data;
  })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CertificationPage');
    // 
  }
  public showinfo()
  {
    this.global.presentModal('certification');
  }
  //  public presentModal() {
  //   const modal =  this.modalCtrl.create(
  //      InfoPage,{cssClass:"mymodal"}
  //   );
  //     modal.present();
  // }
public openAttachment(i)
{
  window.open('http://localhost/registration/uploads/'+i);
// browser.show()
}
public settings()
{
  this.navCtrl.push(SettingsPage);
}
  home(){
  this.navCtrl.push(HomePage);
  }

  public hide()
  {
  this.hidee=!this.hidee;
  }
  create_cert(){
    // this.cert.create_certification(this.id_cert,this.date).subscribe(data=>{
    // })
  }
  public logout()
  {
    this.authCtrl.logout();
    // this.navCtrl.setRoot(LoginPage);
            this.app.getRootNav().setRoot(LoginPage);

  }

 public selectStudent(i)
  {
    console.log(this.global.searchedStudent[i]);
    this.adm.getSelectedStudent(this.global.searchedStudent[i]).subscribe(data=>{
    console.log(data);
    this.selectedStudent=data[0];
    console.log("==");
    console.log(this.selectedStudent);
     this.global.searchedStudent=[];

    })
    
  }
public saveTranscript()
{ let data1={file_name:this.file_name,
  cert_id:this.id_cert,
  qty:this.qty,
  id_student:this.selectedStudent['id_student'],
  date:this.create_date};
console.log(data1);

    this.cert.create_certification(data1,this.first_name,this.second_name,this.third_name,this.last_name).subscribe(data=>{
                                        console.log(data);
                                        });
 
}
public startUpload()
  {
    let fi = this.fileInput.nativeElement;
    

    // console.log(this.FilesU[i]);

     // let fi = this.FilesU[i].nativeElement;
                        // this.presentToast("number of images"+fi.files.length);
    if(fi.files.length>0){
    this.uploadImage(fi).then(data_=>{
      this.global.presentToast('Uploaded');

     
    
      });
      this.uploadedImages.forEach(data=>{

    })
}
    else{
      this.global.presentToast("Please Choose at least 1 file");
    }
  }
  public uploadImage(fi):Promise <void> {
    console.log("Upload image");
                return new Promise <void>(resolve =>{
                 
                  // console.log("no phone device");
                 
                  this.addFile(fi).then(()=>{
                    
                  resolve();
                  });
              
                
                });
               

}
 public addFile(fi): Promise <void> {
                      return new Promise <void>(resolve =>{
                       if(fi.files.length>6){
                        resolve();
                      }//
                      else{
                        this.uploadedImages = [];
                        let upload_complete = 0;
                      for(let i = 0; i <fi.files.length; i++)
                      { 
                          
                                let fileToUpload = fi.files[i];
                                 let d = new Date();
                                let n = d.getTime();
                                // Extension filaka dyary krawaka adozinawa
                                const name = fi.files[i].name;
                                const lastDot = name.lastIndexOf('.');

                                const fileName = name.substring(0, lastDot);
                                const ext = name.substring(lastDot + 1);

                                console.log(ext);
                                let newFileName =  n + "."+ext;
                                 console.log(newFileName);
                                this.adm.upload(fileToUpload,newFileName).subscribe(res => {
                                        console.log(res);
                                        if(res['success'] == 'successful')
                                        {
                                        //   this.uploadedImages.push(newFileName);
                                        //   if(j<=8 && j>=0)
                                        //   {

                                          this.file_name=newFileName;
                                         //     
                                        //     console.log("uploaded requirement");
                                        //     console.log(newFileName);
                                        //     console.log(this.requirements[j]);
                                        //     console.log(this.global.info.id_student);
                                        //     this.attachments[this.requirements[i]].status = true;
                                        //     this.attachments[this.requirements[i]].file_name = newFileName;
                                        //     console.log(this.attachments);

                                        //   })
                                        // }
                                        // else if (j==9)
                                        // {
                                        //   this.adm.addRequirement(newFileName,"transferOldGrade",this.global.info.id_student).subscribe(data=>{
                                        //     console.log(data);
                                        //     console.log("uploaded requirement");
                                        //     // console.log(newFileName);
                                        //     // console.log(this.requirements[j]);
                                        //     // console.log(this.global.info.id_student);
                                        //     // this.attachments[this.requirements[i]].status = true;
                                        //     // this.attachments[this.requirements[i]].file_name = newFileName;
                                        //     // console.log(this.attachments);

                                        //   })
                                        // }
                                        }
                                        upload_complete+=1;

                                        if(upload_complete ==fi.files.length )
                                          {
                                            resolve();
                                          }
                                    });
                           
                      }
                           

                      }
                        
                          });  
      }
public viewCertificate()
{
  this.selectedStudent['cert_no']= this.id_cert;
  this.selectedStudent['create_date']= this.create_date;
  this.selectedStudent['round']= this.round;
  this.selectedStudent['order_no']= this.order_no;
  this.selectedStudent['total_student']= this.total_student;
  this.adm.getCertificateData(this.selectedStudent).subscribe(data=>{
      console.log("-------");
      console.log(data);
      if(data && data.length>0 && data[0]['failedclass'])
      {
        this.global.presentToast("Student Has Failed Class ");
          let content='';
          data.forEach(ele=>{
            content+="<strong>"+ele['year']+" : "+ele['failedclass']+" Failed Class(s) </strong><br>";
          })
         let alert = this.alertCtrl.create({
          title: 'Student Has Not Passed Yet',
          subTitle: content+'',
          buttons: ['OK']
        });
        alert.present();
      }
      else{
        let grades=[];
        Object.keys(data).forEach(function(key) {
            
            console.table('Key : ' + key + ', Value : ' + data[key])
            console.log(data[key]);
            let temp = 0;
            console.log(temp);
            let totalunit = 0;
             Object.keys(data[key]).forEach(function(sub) {
              totalunit+= parseInt(data[key][sub]['unit']);
             });
             console.log("total unit : "+totalunit);
            Object.keys(data[key]).forEach(function(sub) { //1934282
              console.log(data[key][sub]);
              if(parseInt(data[key][sub]['crossing2'])>0)
              {
                temp+= (parseInt(data[key][sub]['crossing2'])+ parseInt(data[key][sub]['crossing'])+parseInt(data[key][sub]['curve']))*(parseInt(data[key][sub]['unit'])/totalunit);
              } else if(parseInt(data[key][sub]['crossingfinal'])>0)
              {
                temp+= (parseInt(data[key][sub]['crossingfinal'])+ parseInt(data[key][sub]['crossing'])+parseInt(data[key][sub]['curve']))*(parseInt(data[key][sub]['unit'])/totalunit);
              } else if(parseInt(data[key][sub]['corse2'])>0)
              {
                temp+= (parseInt(data[key][sub]['corse1'])+ parseInt(data[key][sub]['corse2'])+parseInt(data[key][sub]['curve']))*(parseInt(data[key][sub]['unit'])/totalunit);
              } else 
              {
                temp+= (parseInt(data[key][sub]['corse1'])+ parseInt(data[key][sub]['final'])+parseInt(data[key][sub]['curve']))*(parseInt(data[key][sub]['unit'])/totalunit);
              }
            });
            grades.push(temp);
            console.log(temp);
          });
        console.log(grades);
        console.log(this.global.settings);
        this.selectedStudent['passing_limit']=this.global.settings.passing_limit;
        this.selectedStudent['grades']=grades;
        this.selectedStudent['dist']=this.global.settings.grade_distribute;
        // console.log(data);
        var mapForm = document.createElement("form");
      mapForm.target = "_blank";    
      mapForm.method = "POST";
      mapForm.action = "http://localhost/registration/certificate.php";

      // Create an input
      var mapInput = document.createElement("input");
      mapInput.name = "data";
      mapInput.value = JSON.stringify(data); 
      var mapInput2 = document.createElement("input");
      mapInput2.name = "student";
      mapInput2.value = JSON.stringify(this.selectedStudent); 

      // Add the input to the form
      mapForm.appendChild(mapInput);
      mapForm.appendChild(mapInput2);

      // Add the form to dom
      document.body.appendChild(mapForm);

      // Just submit
      mapForm.submit();
      }
})
      
}
  public getItems(param:any)
  {
    let val = param.target.value;
    if (val && val.trim() != '') {

      this.adm.searchStudent(val).subscribe(data=>{
        console.log(data);
        if(data)
        {

        this.global.searchedStudent = data;
        }
      })
  }else {
      this.global.searchedStudent= [];

    }
  }
  public getCert(param:any)
  {
    let val = param.target.value;

      this.cert.getCertificate(val).subscribe(data=>{
        console.log(data);
        this.certifications=data;
        
      })
  
  }
}
