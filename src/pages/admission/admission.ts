import { Component,ViewChild } from '@angular/core';
import {App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewadmissionPage } from '../../pages/viewadmission/viewadmission';
import { AdmissionProvider } from '../../providers/admission/admission';
import { GlobalProvider } from '../../providers/global/global';
import { LoginPage } from '../../pages/login/login';
import {AuthenticationProvider} from '../../providers/authentication/authentication';
import * as XLSX from 'ts-xlsx';


@IonicPage()
@Component({
  selector: 'page-admission',
  templateUrl: 'admission.html',
})
export class AdmissionPage {
@ViewChild("fileInputC") fileInputC;
@ViewChild("fileInputID") fileInputID;
@ViewChild("fileInputR") fileInputR;
@ViewChild("fileInputCZ") fileInputCZ;
@ViewChild("fileInputZF") fileInputZF;
@ViewChild("fileInputCN") fileInputCN;
@ViewChild("fileInputBN") fileInputBN;
@ViewChild("fileInputK") fileInputK;
@ViewChild("fileInputWP") fileInputWP;
@ViewChild("fileInputTR") fileInputTR;
// searchedStudent:any;
  cardToShow:string='n';
  resons:any;
  public show: boolean;
uploadedImages=[];
FilesU=[];
arrayBuffer:any;
file:File;
idNewStudent:any;
this_stage_id:any;
studentPayment:any;
showtable="false";


attachments={'certificate':[{'file_name':'',status:false}],
              'ID':[{'file_name':'',status:false}],
              'jnsya':[{'file_name':'',status:false}],
              'carty_znyary':[{'file_name':'',status:false}],
              'zankoline_form':[{'file_name':'',status:false}],
              'national_card':[{'file_name':'',status:false}],
              'balennama':[{'file_name':'',status:false}],
              'kafalat':[{'file_name':'',status:false}],
              'payment':[{'file_name':'',status:false}]
            }
requirements=['certificate',
              'ID',
              'jnsya',
              'carty_znyary',
              'zankoline_form',
              'national_card',
              'balennama',
              'kafalat',
              'payment'
            ];
  constructor(public app:App,public authCtrl:AuthenticationProvider, public navCtrl: NavController, public navParams: NavParams,public adm:AdmissionProvider,public global:GlobalProvider) {
    this.FilesU=[this.fileInputC,this.fileInputID,this.fileInputR,this.fileInputCZ,this.fileInputZF,this.fileInputCN,this.fileInputBN,this.fileInputK,this.fileInputWP]
    console.log(this.FilesU);
    
  }
  incomingfile(event) 
  {
  this.file= event.target.files[0]; 
  }
  UploadName() {
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.global.info.selected_dep)
      {
        id_dep = ele['id'];
      }
    })
     let id_stage="";
      this.global.stage.forEach(ele=>{
        console.log(ele);
        if(ele['stagetxtE']==this.global.info.selected_stage)
        {
          id_stage = ele['id_stage'];
          console.log(id_stage);
        }
      })
       let id_sem="";
      this.global.semster.forEach(ele=>{
        console.log(ele);
        if(ele['year']==this.global.info.selected_sem)
        {
          id_sem = ele['id'];
          console.log(id_sem);
        }
      });
       this.global.info.id_dep = id_dep;
    this.global.info.id_stage = id_stage;
    this.global.info.id_sem = id_sem;
      let fileReader = new FileReader();
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, {type:"binary"});
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            let students = XLSX.utils.sheet_to_json(worksheet,{raw:true});
            console.log(students);
            this.adm.addFromXLSX(students,this.global.info).subscribe(data=>{
              console.log(data);
            })
        }
        fileReader.readAsArrayBuffer(this.file);
      }
 public showinfo()
  {
    this.global.presentModal('admission');
  }
  view_admission(){
  // this.navCtrl.push(ViewadmissionPage);
  console.log(this.global.info);
  console.log(this.attachments);
  var mapForm = document.createElement("form");
  mapForm.target = "_blank";    
  mapForm.method = "POST";
  mapForm.action = "http://localhost/registration/viewAdmission.php";

  // Create an input
  var mapInput = document.createElement("input");
 mapInput.name = "data";
mapInput.value = JSON.stringify(this.global.info); 
var mapInput2 = document.createElement("input");
 mapInput2.name = "attach";
mapInput2.value = JSON.stringify(this.attachments); 

  // Add the input to the form
  mapForm.appendChild(mapInput);
  mapForm.appendChild(mapInput2);

  // Add the form to dom
  document.body.appendChild(mapForm);

  // Just submit
  mapForm.submit();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdmissionPage');
  }
public openAttachment(i)
{
  console.log(i)
  window.open('http://localhost/registration/uploads/'+i);
// browser.show()
}
  public parallel(){
   this.show=!this.show;
  }
  
  add_student(){
    if(this.global.info.id_student!=''){
      this.global.info = {
        id_student:'',
        code:'',
        fname:'',
        mname:'',
        sname:'',
        lname:'',
        gender:'',
        dob:'',
        phone:'',
        selected_resident:'',
        selected_state:'',
        selected_reciveType:'',
        selected_sem:'',
        note:'',
        selected_dep:'',
        selected_stage:'',
        id_dep:'',
        id_stage:'',
        id_sem:'',
        resons:'',
        date_of_payment:'',
        payment:'',
        sem_payment:'',
        id_sem_payment:'',
        price:'',
        univ_name_transfer:'',
        dep_name_transfer:'',
        univ_name_descent:'',
        dep_name_descent:'',
        selectedDescentType:'',
        selectedDescentState:'',
        dep_name:''
    }
  }else{
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.global.info.selected_dep)
      {
        id_dep = ele['id'];
      }
    })
     
      this.global.stage.forEach(ele=>{
        console.log(ele);
        if(ele['stagetxtE']==this.global.info.selected_stage)
        {
          this.this_stage_id = ele['id_stage'];
          console.log(this.this_stage_id);
        }
      })
       let id_sem="";
      this.global.semster.forEach(ele=>{
        console.log(ele);
        if(ele['year']==this.global.info.selected_sem)
        {
          id_sem = ele['id'];
          console.log(id_sem);
        }
      })
    console.log(this.global.info);
    this.global.info.id_dep = id_dep;
    this.global.info.id_stage = this.this_stage_id;
    this.global.info.id_sem = id_sem;
    this.adm.addStudent(this.global.info).subscribe(data=>{
    console.log (data);
    if(data['result']=='success'){
      console.log('successed');
      this.global.presentToast("بە سەرکەوتویی  زیادکرا");
      console.log(data);
      this.idNewStudent=data["newid"];
      console.log(this.idNewStudent);

       console.log(this.global.info.selected_reciveType);
      if(this.global.info.selected_reciveType=="گەڕاوە")
      {
        console.log(this.global.info.selected_reciveType);
        this.adm.addRecursive(this.idNewStudent,this.this_stage_id).subscribe(data=>{
         console.log(data);
          if(data['result']=='success'){
            this.global.presentToast('بە سەرکەوتویی  زیادکرا');
            this.idNewStudent=null;
            this.this_stage_id=null;
          }else{
            this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
          }

        })

      }
      else if(this.global.info.selected_reciveType=="ئاسایی")
      {
        console.log(this.global.info.selected_reciveType);
        this.adm.addOrdinary(this.idNewStudent,this.this_stage_id).subscribe(data=>{
         console.log(data);
          if(data['result']=='success'){
            this.global.presentToast('بە سەرکەوتویی  زیادکرا');
            this.idNewStudent=null;
            this.this_stage_id=null;
          }else{
            this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
          }
        })
       }
      this.global.info.id_student=null;
      this.global.info.code=null;
      this.global.info.fname=null;
      this.global.info.mname=null;
      this.global.info.sname=null;
      this.global.info.lname=null;
      this.global.info.gender=null;
      this.global.info.dob=null;
      this.global.info.phone=null;
      this.global.info.note=null;
      this.global.info.resons=null;
      this.global.info.date_of_payment=null; 
    }else{
      this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
    }
    })
  }
}

public addDescent(){
  console.log(this.global.info.selected_reciveType);
  this.adm.addDescent(this.idNewStudent,this.global.info.univ_name_descent,this.global.info.dep_name_descent,this.global.info.selectedDescentType,this.global.info.selectedDescentState,this.this_stage_id).subscribe(data=>{
    console.log(data);
    if(data['result']=='success'){
      this.global.presentToast('بە سەرکەوتویی  زیادکرا');
      
      this.global.info.univ_name_descent=null
      this.global.info.dep_name_descent=null;
      
    }
    else{
      this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
    }
  })
}
public addTransfer(){
  console.log(this.global.info);
        this.adm.addTransfer(this.idNewStudent,this.global.info.univ_name_transfer,this.global.info.dep_name_transfer,this.this_stage_id,this.global.info.resons,this.global.info.selected_sem).subscribe(data=>{
         console.log(data);
          if(data['result']=='success'){
            this.global.presentToast('بە سەرکەوتویی  زیادکرا');
            this.global.info.univ_name_transfer=null;
            this.global.info.dep_name_transfer=null;
         
          }else{
           this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
          }
        })
}
public showStudentPayment(){
  this.adm.showStudentPayment(this.global.info.id_student).subscribe(data=>{
    console.log(data);
    if(data['result']=='success'){
      this.studentPayment=data;
      console.log(this.studentPayment);
      this.global.presentToast('بە سەرکەوتویی  زیادکرا');
    }else{
      this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
    }
  })
  

}
public addParallel(){
      this.global.semster.forEach(ele=>{
        console.log(ele);
        if(ele['year']==this.global.info.sem_payment)
        {
          this.global.info.id_sem_payment = ele['id'];
          console.log("id_sem_payment");
          console.log(this.global.info.id_sem_payment);
        }
      })
  console.log(this.global.info.selected_reciveType);

        let id_attach=1;
        this.adm.addPayment(this.idNewStudent,this.global.info.id_sem_payment,id_attach,this.this_stage_id,this.global.info.price,this.global.info.payment,this.global.info.date_of_payment).subscribe(data=>{
           console.log("paralell");
           console.log(data);
          if(data['result']=='success'){
            this.global.presentToast('بە سەرکەوتویی  زیادکرا');
          }else{
            this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
          }
        })
}
  public selectStudent(i)
  {
    console.log(this.global.searchedStudent[i]);
    this.adm.getSelectedStudent(this.global.searchedStudent[i]).subscribe(data=>{
      console.log(data);
      this.global.info=data[0];
      this.global.searchedStudent=[];
      this.adm.getRequirement(data[0]['id_student']).subscribe(data=>{
        console.log(data);
         Object.keys(data).forEach(ele=>{
          console.log(ele);
          this.attachments[ele] = data[ele];
        })
      })
      if(( this.global.info.selected_reciveType=='دابەزین' &&this.global.info.selectedDescentType=='پارالێڵ') || this.global.info.selected_reciveType=='پارالێڵ' || (this.global.info.resons=='پارالێڵ' && this.global.info.selected_reciveType=='گواستنەوە')){
        this.adm.showStudentPayment(data[0]['id_student']).subscribe(data=>{
    console.log(data);
    console.log("true");
    if(data){
      this.studentPayment=data;
      console.log(this.studentPayment);
    }
  })
      }
    })
  }
  public getItems(param:any)
  {
    let val = param.target.value;
    if (val && val.trim() != '') {

      this.adm.searchStudent(val).subscribe(data=>{
        console.log(data);
        if(data)
        {

        this.global.searchedStudent = data;
        }
      })
  }else {
      this.global.searchedStudent= [];

    }
  }
  public saveTransferinfo(){

    this.adm.saveTransferinfo(this.global.info).subscribe(data=>{
      console.log(data);
    })
  }
  public saveStudentinfo(){
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.global.info.selected_dep)
      {
        id_dep = ele['id'];
      }
    })
    let id_stage="";
      this.global.stage.forEach(ele=>{
        console.log(ele);
        if(ele['stagetxtE']==this.global.info.selected_stage)
        {
          id_stage = ele['id_stage'];
          console.log(id_stage);
        }
      })
      console.log(this.global.info.univ_name_transfer);
      let id_semester="";
      this.global.semster.forEach(ele=>{
        console.log(ele);
        if(ele['year']==this.global.info.sem_payment)
        {
          id_semester = ele['id'];
          console.log("this.global.info.id_sem_payment");
          console.log(this.global.info.id_sem_payment);
        }
      })
      this.global.info.id_sem_payment=id_semester;

    console.log(this.global.info);
    this.global.info.id_dep = id_dep;
       this.global.info.id_stage = id_stage;

    this.adm.saveStudentinfo(this.global.info).subscribe(data=>{
      console.log(data);
      console.log(this.global.info);
      if(data['result']=='success'){
        this.global.presentToast('بە سەرکەوتویی  جێبەجێ کرا');
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایا  دوبارەی بکەرەوە ');
      }
    })
  }
  public startUpload(i)
  {
    console.log(i);
    let fi =null;
    if(i==0)
    {
      fi = this.fileInputC.nativeElement;
    }else if(i==1)
    {
      fi = this.fileInputID.nativeElement;
    }else if(i==2)
    {
      fi = this.fileInputR.nativeElement;
    }else if(i==3)
    {
      fi = this.fileInputCZ.nativeElement;
    }else if(i==4)
    {
      fi = this.fileInputZF.nativeElement;
    }else if(i==5)
    {
      fi = this.fileInputCN.nativeElement;
    }else if(i==6)
    {
      fi = this.fileInputBN.nativeElement;
    }else if(i==7)
    {
      fi = this.fileInputK.nativeElement;
    }else if(i==8)
    {
      fi = this.fileInputWP.nativeElement;
    }else if(i==9)
    {
      fi = this.fileInputTR.nativeElement;
    }

    // console.log(this.FilesU[i]);

     // let fi = this.FilesU[i].nativeElement;
                        // this.presentToast("number of images"+fi.files.length);
    if(fi.files.length>0){
      this.uploadImage(fi,i).then(data_=>{
      this.global.presentToast('Uploaded');
      });
      this.uploadedImages.forEach(data=>{

    })
}
    else{
      this.global.presentToast("Please Choose at least 1 file");
    }
  }
  public logout()
  {
    this.authCtrl.logout();
    // this.navCtrl.setRoot(LoginPage);
            this.app.getRootNav().setRoot(LoginPage);

  }
  public uploadImage(fi,i):Promise <void> {
    console.log("Upload image");
                return new Promise <void>(resolve =>{
                 
                  // console.log("no phone device");
                 
                  this.addFile(fi,i).then(()=>{
                    
                  resolve();
                  });
              
                
                });
               

}
 public addFile(fi,j): Promise <void> {
  return new Promise <void>(resolve =>{
    if(fi.files.length>6){
      resolve();
    }//
    else{
      this.uploadedImages = [];
      let upload_complete = 0;
      for(let i = 0; i <fi.files.length; i++){ 
        let fileToUpload = fi.files[i];
        let d = new Date();
        let n = d.getTime();
        // Extension filaka dyary krawaka adozinawa
        const name = fi.files[i].name;
        const lastDot = name.lastIndexOf('.');

        const fileName = name.substring(0, lastDot);
        const ext = name.substring(lastDot + 1);

        console.log(ext);
        let newFileName =  n + "."+ext;
        console.log(newFileName);
        this.adm.upload(fileToUpload,newFileName).subscribe(res => {
          console.log(res);
          if(res['success'] == 'successful'){
            this.uploadedImages.push(newFileName);
            if(j<=8 && j>=0) {
              this.adm.addRequirement(newFileName,this.requirements[j],this.global.info.id_student).subscribe(data=>{
                console.log(data);
                console.log("uploaded requirement");
                console.log(newFileName);
                console.log(this.requirements[j]);
                console.log(this.global.info.id_student);
                this.attachments[this.requirements[i]].status = true;
                this.attachments[this.requirements[i]].file_name = newFileName;
                console.log(this.attachments);
              })
            }
            else if (j==9){
              this.adm.addRequirement(newFileName,"transferOldGrade",this.global.info.id_student).subscribe(data=>{
                console.log(data);
                console.log("uploaded requirement");
                // console.log(newFileName);
                // console.log(this.requirements[j]);
                // console.log(this.global.info.id_student);
                // this.attachments[this.requirements[i]].status = true;
                // this.attachments[this.requirements[i]].file_name = newFileName;
                // console.log(this.attachments);
              })
           }
        }
        upload_complete+=1;
        if(upload_complete ==fi.files.length ){
          resolve();
        }
      });                    
    }
  }
});  
}

}
//