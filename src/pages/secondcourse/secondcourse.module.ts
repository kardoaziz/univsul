import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecondcoursePage } from './secondcourse';

@NgModule({
  declarations: [
    SecondcoursePage,
  ],
  imports: [
    IonicPageModule.forChild(SecondcoursePage),
  ],
})
export class SecondcoursePageModule {}
