import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditEnrollsPage } from './edit-enrolls';

@NgModule({
  declarations: [
    EditEnrollsPage,
  ],
  imports: [
    IonicPageModule.forChild(EditEnrollsPage),
  ],
})
export class EditEnrollsPageModule {}
