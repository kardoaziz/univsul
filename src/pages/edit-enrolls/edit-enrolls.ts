import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { SubjectProvider } from '../../providers/subject/subject';
import { AdmissionProvider } from '../../providers/admission/admission';

//
/**
 * Generated class for the HomeExamcommiteePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-edit-enrolls',
  templateUrl: 'edit-enrolls.html'
})
export class EditEnrollsPage {
  student:any;
  dep_id:any;
  id_sem:any;
  foundSubjects:any;
  selected_sub:any;
  selected_sem:any;
  student_grade:any;
  student_stage:any;
  selected_stage:any;
  
  offer_type="normal";
   constructor(public adm:AdmissionProvider, public sub:SubjectProvider, public app:App,  public navCtrl: NavController, public navParams: NavParams, public global:GlobalProvider) {
  this.student= this.navParams.get("student");
  this.dep_id= this.navParams.get("dep_id");
  this.id_sem= this.navParams.get("id_sem");
  console.log("info ");
  console.log(this.student);
  console.log(this.dep_id);
  console.log(this.id_sem);
  this.sub.get_Stage_Subject(this.student['id_student'],this.id_sem,this.student.id_stage).subscribe(data=>{
      console.log("student grade");
      console.log(data);
    this.student_grade = data;

   
  })
  }
 public showinfo()
  {
    this.global.presentModal('certification');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Edit Enroll page');
  }
  public getSubjects()
{
  let id_sem=-1;
    this.global.semster.forEach(ele=>{
      if(ele['year'] == this.selected_sem)
      {
        id_sem=ele['id'];
      }
    })
    let id_stage=-1;
    this.global.stage.forEach(ele=>{
      if(ele['stagetxt'] == this.selected_stage)
      {
        id_stage=ele['id_stage'];
      }
    })
    console.log("id_sem");
    console.log(id_sem);
    console.log(id_stage);
    console.log(this.dep_id);
  this.adm.getOfferedSubjects(id_sem,this.dep_id.id,id_stage).subscribe(data=>{
    console.log(data);
    this.foundSubjects = data;
  })
}
public deleteEnroll(i)
{
  this.adm.deleteEnroll(this.student_grade[i]).subscribe(data=>{
    console.log(data);
  })
}
public offerSub()
{
   let id_sub=-1;
    this.foundSubjects.forEach(ele=>{
      if(ele['sub_name'] == this.selected_sub)
      {
        id_sub=ele['id_sub'];
      }
    })
  this.adm.offerSub(id_sub,this.id_sem,this.student['id_student']).subscribe(data=>{
    console.log(data);
  })
}
  
}
