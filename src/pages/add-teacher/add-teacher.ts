import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PopularExamcommitteePage } from '../../pages/popular-examcommittee/popular-examcommittee';
import { GlobalProvider } from '../../providers/global/global';
import { AddDepartmentPage } from '../../pages/add-department/add-department';
import { AddSubjectPage } from '../../pages/add-subject/add-subject';
import { HomeProvider } from '../../providers/home/home';
import { AlertController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-add-teacher',
  templateUrl: 'add-teacher.html',
})
export class AddTeacherPage {
  firstN:any;midN:any;thirdN:any;gender:any;phoneN:any;email:any;address:any;acadimic_title:any;selected_dep:any;
  teacher:any;
  person_id:any;
  showUpdateBtn:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public home:HomeProvider, public global:GlobalProvider, public alertCtrl:AlertController) {
    this.showUpdateBtn="false";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTeacherPage');
    // this.getTeachers();
  }
   public showinfo()
  {
    this.global.presentModal('add-teacher');
  }
  popular_examcommittee(){
  this.navCtrl.push(PopularExamcommitteePage);
  }

  add_department(){
  this.navCtrl.push(AddDepartmentPage);
  }

  add_subject(){
  this.navCtrl.push(AddSubjectPage);
  }
  add_teacher(){
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.selected_dep)
      {
        id_dep = ele['id'];
      }
    })

    this.home.addTeacher(this.firstN,this.midN,this.thirdN,this.gender,this.phoneN,this.email,this.address,id_dep,this.acadimic_title).subscribe(data=>{
      console.log (data);
      if(data['result']=='success'){
        this.getTeachers();
        this.global.presentToast('زیادکرا ');
        this.firstN=null;
        this.midN=null;
        this.thirdN=null;
        this.gender=null;
        this.phoneN=null;
        this.email=null;
        this.address=null;
        id_dep=null;
        this.acadimic_title=null;
      }
      else{
        
        this.global.presentToast(' سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە ');
      }
    }) 
  }
  public getTeachers(){
    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.selected_dep)
      {
        id_dep = ele['id'];
      }
    })
    this.home.getTeacher(id_dep).subscribe(data=>{
      console.log(data);
     this.teacher=data;
    })
  }

  public get_teacher_to_update(j:number){
    console.log(this.teacher[j]);
    this.teacher.forEach(ele=>{
      if(ele['id_person']==this.teacher[j]['id_person']){
        this.person_id = ele['id_person'];

        this.firstN=this.teacher[j]['f_name'];
        this.midN=this.teacher[j]['m_name'];
        this.thirdN=this.teacher[j]['l_name'];
        this.gender=this.teacher[j]['gender'];
        this.phoneN=this.teacher[j]['phone'];
        this.email=this.teacher[j]['email'];
        this.address=this.teacher[j]['address'];
        this.selected_dep=this.teacher[j]['dep_namek'];
        this.acadimic_title=this.teacher[j]['acadimic_title'];
        console.log(this.person_id);
        this.showUpdateBtn="true";
      }
    })
  }

  update_teacher(){

    let id_dep="";
    this.global.dep_name.forEach(ele=>{
      console.log(ele);
      if(ele['name']==this.selected_dep)
      {
        id_dep = ele['id'];
      }
    })


    this.home.updateTeacher(this.person_id,this.firstN,this.midN,this.thirdN,this.gender,this.phoneN,this.email,this.address,id_dep,this.acadimic_title).subscribe(data=>{
      console.log (data);
      if(data['result']=='success'){
        this.getTeachers();
        this.global.presentToast('بە سەرکەوتوویی ئەنجام درا ');
        this.firstN=null;
        this.midN=null;
        this.thirdN=null;
        this.gender=null;
        this.phoneN=null;
        this.email=null;
        this.address=null;
        id_dep=null;
         this.acadimic_title=null;
      }
      else{
        this.global.presentToast(' سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە ');
      }
    }) 
  }

  delete_teacher(j:number){
    const alert = this.alertCtrl.create({
      title: 'ئاگادارکردنەوە !',
      message: 'دڵنیایت ئەتەوێت   '+this.teacher[j]['acadimic_title']+" "+this.teacher[j]['name']+'ە بسڕیتەوە ؟',
      buttons: [
        {
          text: 'نەخێر',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'بەڵێ',
          handler: () => {
            let id_person="";
    this.teacher.forEach(ele=>{
      if(ele['id_person']==this.teacher[j]['id_person'])
      {
        id_person = ele['id_person'];
        console.log(id_person);
        console.log(this.teacher[j]['name']);
      }
    })
    this.home.delete_teacher(id_person).subscribe(data=>{
      console.log(data);
      if (data['result']=="success") {
        this.global.presentToast('بەسەرکەوتویی جێبەجێ کرا');
      }else{
        this.global.presentToast('سەرکەوتوو نەبوو تکایە دوبارەی بکەرەوە');
      }
    })
          }
        }
      ]
    });
     alert.present();
  }
}
