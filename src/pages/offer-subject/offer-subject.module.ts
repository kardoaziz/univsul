import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfferSubjectPage } from './offer-subject';

@NgModule({
  declarations: [
    OfferSubjectPage,
  ],
  imports: [
    IonicPageModule.forChild(OfferSubjectPage),
  ],
})
export class OfferSubjectPageModule {}
