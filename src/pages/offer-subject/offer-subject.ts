import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
 import {AdmissionProvider} from '../../providers/admission/admission';
 import {GlobalProvider} from '../../providers/global/global';
 // import {AddDepartmentProvider} from '../../providers/add-department/add-department';
/**
 * Generated class for the OfferSubjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offer-subject',
  templateUrl: 'offer-subject.html',
})
export class OfferSubjectPage {
	stages:any;
	semesters:any;
	selectedStage:any;
	selectedDep:any;
	selectedSem:any;
	subjects:any;
	SelectedSubjects=[];
	semsubjects:any;
  constructor(public global:GlobalProvider, public admCtrl:AdmissionProvider, public navCtrl: NavController, public navParams: NavParams) {
  	this.admCtrl.getStage().subscribe(data=>{
  		console.log(data);
  		this.stages = data;
  	});
  	this.admCtrl.getSemesters().subscribe(data=>{
  		this.semesters = data;
  		console.log(data);
  	});

  }
   public showinfo()
  {
    this.global.presentModal('offer-subject');
  }
  public onChange()
  {
  	console.log(this.selectedStage);
  	console.log(this.selectedSem);
  	console.log(this.selectedDep);
  	if(this.selectedDep && this.selectedSem && this.selectedStage)
  	{
  		let semid=-1;
  		let depid=-1;
  		let stageid=-1;
  		this.global.dep_name.forEach(ele=>{
  			if(ele['name'] == this.selectedDep)
  			{
  				depid = ele['id'];
  			}
  		});
  		this.stages.forEach(ele=>{
  			if(ele['stagetxtE'] == this.selectedStage)
  			{
  				stageid = ele['id_stage'];
  			}
  		});
  		this.semesters.forEach(ele=>{
  			if(ele['year'] == this.selectedSem)
  			{
  				semid = ele['id_sem'];
  			}
  		});
  		console.log(semid);
	  	console.log(depid);
	  	console.log(stageid);
	  	this.admCtrl.getDepSub(depid).subscribe(data=>{
	  		console.log(data);
	  		this.subjects = data;
	  		
	  	});
	 this.getOfferedSubjects(semid,depid,stageid);	
  	}
  }
  public getOfferedSubjects(semid,depid,stageid)
  {
  	this.admCtrl.getOfferedSubjects(semid,depid,stageid).subscribe(data=>{
	 	console.log(data);
	 	this.semsubjects=data;
	 }) 
  }
public selectSubject(i)
{
	console.log(i);
	if(this.SelectedSubjects.indexOf(i) > -1)
	{
		this.SelectedSubjects.splice(this.SelectedSubjects.indexOf(i), 1);
	}
	else {
		this.SelectedSubjects.push(i);
	}
}
public deleteoffer(i)
{
	this.admCtrl.deleteoffer(this.semsubjects[i]).subscribe(data=>{
		console.log(data);
		if(data['result']=="success")
		{
			this.getOfferedSubjects(this.semsubjects[i]['id_sem'],this.semsubjects[i]['id_dep'],this.semsubjects[i]['id_stage']);
		}
	})
}
public OfferSubjects()
{
	let subs = [];
	this.SelectedSubjects.forEach(ele=>{
		console.log(this.subjects[ele]);
		subs.push(this.subjects[ele]);
	})
	let semid=-1;
	let depid=-1;
	let stageid=-1;
	this.global.dep_name.forEach(ele=>{
  			if(ele['name'] == this.selectedDep)
  			{
  				depid = ele['id'];
  			}
  		});
  		this.stages.forEach(ele=>{
  			if(ele['stagetxtE'] == this.selectedStage)
  			{
  				stageid = ele['id_stage'];
  			}
  		});
  		this.semesters.forEach(ele=>{
  			if(ele['year'] == this.selectedSem)
  			{
  				semid = ele['id_sem'];
  			}
  		});
	this.admCtrl.OfferSubjects(subs,stageid,semid).subscribe(data=>{
		console.log(data);
		if(data['result']=="success")
		{
			this.global.presentToast("Subject offered Successfully");
		}
	})
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferSubjectPage');
  }

}
