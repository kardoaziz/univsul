<?php 

$info=json_decode($_POST['data']); 
$students=json_decode($_POST['students']); 
// echo $info;
// $students[] =array("f_name"=>"kardo","m_name"=>"othman","s_name"=>"aziz","l_name"=>"hama");
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Student Form</title>
    <link rel="stylesheet" type="text/css" href="../asset/css/style.css">
    <script type="text/javascript" src="../asset/js/hotsnackbar.js"></script>
</head>
<style type="text/css">
	.text-right{
		text-align: right !important;
	}
	.text-center{
		text-align: center !important;
	}
	.text-left{
		text-align: left !important;
	}
	.full{
		width:100% !important;
	}
	.ffull{
		min-width:100% !important;
	}
	.capital{
		text-transform: capitalize;
	}
	.bold{
	font-weight: bold;
		}
		table tr {
			height:30px;
			border:1px solid grey !important;
		}
		table tr td{
			border:1px solid grey !important;
		}
		table {
			/*border:1px solid grey;*/
		}
		@media print
{     
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
}
	</style>
	<body>
		<button style="height:40px;font-size:20px;float:right; margin:10px;" class="noprint" onclick="javascript:window.close()">Close</button>
		<button style="height:40px;font-size:20px;float:right;margin:10px; " class="noprint" onclick="javascript:window.print()">Print</button>
		<table style="width:100%;border:0px;">
			<tr>
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-transform: capitalize;"> 
					Department of <?php  echo $info->dep;?><br>
					Stage : <?php  echo $info->stage;?> <br>
					Subject: <?php  echo $info->sub;?> <br>
					Teacher: <?php  echo $info->teacher;?><br>
				</td>
				<td colspan="5" style="border:0px white solid !important;font-size:20px; text-transform: capitalize;">
					University of Slemani<br>
					College of Science <br>
					
					<?php  echo $info->sem;?> <br>
				</td>

			</tr>
		</table>
		<hr>
		<table style="width:100%;">
			<tr>
				<th>No.
				</th>
				<th>Name
				</th>
				<th>%<?php echo $info->pre_final;?>
				</th>
				<th>Written
				</th>
			</tr>
			<?php 
				$i=1;
				foreach ($students as $stu )
				{
					$stu = get_object_vars( $stu );

					echo "<tr>
							<td style='text-align:center;'>$i
							</td>";
					if($stu['f_name']){		
						echo 	"<td style='text-transform:capitalize;'>".$stu['f_name']." ".$stu['m_name']." ".$stu['s_name']." ".$stu['l_name']."
							</td>";
						}
							else{
						echo "<td style='text-transform:capitalize;'>".$stu['name']."
							</td>";
							}
						echo 	"<td style='text-align:center;'>
							</td>
							<td></td>
						</tr>";
						$i+=1;
				}
			?>
</table>
<hr>
		<table style="width:100%;margin-top:50px;">
			<tr style="padding-top:30px !important;">
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-align:center;">
					 واژۆی سه‌رۆكی به‌ش
				</td>
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-align:center;">
					واژۆی مامۆستای وانه‌
				</td>
			</tr>
		</table>

	</body>
	</html>