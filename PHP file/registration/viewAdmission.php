<?php 

$info=json_decode($_POST['data']); 
$attachs=json_decode($_POST['attach']); 
// echo $attachs->certificate->requirement;
// echo $info->id_student;
// echo $data->phone;

?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Student Form</title>
    <link rel="stylesheet" type="text/css" href="../asset/css/style.css">
    <script type="text/javascript" src="../asset/js/hotsnackbar.js"></script>
</head>
<style type="text/css">
	.text-right{
		text-align: right !important;
	}
	.text-center{
		text-align: center !important;
	}
	.text-left{
		text-align: left !important;
	}
	.full{
		width:100% !important;
	}
	.ffull{
		min-width:100% !important;
	}
	.capital{
		text-transform: capitalize;
	}
	.bold{
	font-weight: bold;
		}
		table tr {
			height:30px;
			border:1px solid grey !important;
		}
		table tr td{
			border:1px solid grey !important;
		}
		table {
			/*border:1px solid grey;*/
		}
		@media print
{     
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
}
	</style>
<!-- <body onload="javascript:print();"> -->
	<body>
		<button style="height:40px;font-size:20px;position:absolute; left:100px;top:20px;" class="noprint" onclick="javascript:window.close()">Close</button>
	<div style="width:100%;text-align: center;">
		Student Form
	</div>
	<table style="width:100%;">
		<tr>
			<th colspan=4>
				<img src="./uploads/univsul.png">
			</th>
			<th colspan="1"> 2018-2019
			</th>
			<th colspan="1"> University of Slemani <br> College of Science <br> Department of Computer Science
			</th>
		</tr>
</table>
<table style="width:100%;">
		<tr>
			<!-- <td><div class="full capital text-right bold">note</div></td> -->
			<td colspan="2"><div class="full capital text-center bold" ><?php echo $info->lname; ?></div></td>
			<td><div class="full capital text-center bold"><?php echo $info->sname; ?></div></td>
			<td><div class="full capital text-center bold"><?php echo $info->mname; ?></div></td>
			<td><div class="full capital text-center bold"><?php echo $info->fname; ?></div></td>
			<td><div class=" text-right">: Name</div></td>
		</tr>
		<tr>
			<td class="text-right" colspan="2">Note
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="1"><?php echo $info->selected_resident; ?>
			</td>
			<td class="text-right bold">Resident
			</td>
			<td class="text-center"><?php echo $info->gender; ?>
			</td>
			<td class="text-right bold">Gender
			</td>
		</tr>
		<tr>
			<td class="text-right" colspan="2">Note
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="1">08/07/1989
			</td>
			<td class="text-right bold">Date of Birth
			</td>
			<td class="text-center"><?php echo $info->phone; ?>
			</td>
			<td class="text-right bold">phone
			</td>
		</tr>
		<tr>
			<td class="text-right" colspan="2">Note
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="1"><?php echo $info->selected_stage; ?>
			</td>
			<td class="text-right bold">Stage
			</td>
			<td class="text-center"><?php echo $info->selected_dep; ?>
			</td>
			<td class="text-right bold">Department
			</td>
		</tr>
		<tr>
			<td class="text-right" colspan="2">Note
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="1"><?php echo $info->selected_reciveType; ?>
			</td>
			<td class="text-right bold">Registeration Type
			</td>
			<td class="text-center"><?php echo $info->selected_state; ?>
			</td>
			<td class="text-right bold">Status
			</td>
		</tr>
		<tr>
			<!-- <td class="text-right" colspan="1">&nbsp; -->
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="2"><?php echo $info->selected_sem; ?>
			</td>
			<td class="text-right bold">Registeration Year
			</td>
			<td colspan="3" rowspan="4" class="text-center">
				Old university Information
			</td>
		</tr>
		<tr>
			<!-- <td class="text-right" colspan="1">&nbsp; -->
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="2"><?php echo $info->univ_name; ?>
			</td>
			<td class="text-right bold">Old University
			</td>
			
		</tr>
		<tr>
			<!-- <td class="text-right" colspan="1">&nbsp; -->
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="2"><?php echo $info->dep_name; ?>
			</td>
			<td class="text-right bold">Department
			</td>
			
		</tr>
		<tr>
			<!-- <td class="text-right" colspan="1">&nbsp; -->
				<!-- <div class="ffull">   </div> -->
			</td>
			<td class="text-center" colspan="2"><?php echo $info->resons; ?>
			</td>
			<td class="text-right bold">Reason
			</td>
			
		</tr>
		<tr>
			<td colspan="4" class="text-center">Note
			</td>
			<td>Submitted ?
			</td>
			<td>Requirement
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right">
				 <?php if($attachs->certificate->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->certificate->file_name.'>Open File</a>';}?>
			</td>
			<td>
				<?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->certificate->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->certificate->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->ID->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->ID->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->ID->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->ID->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->jnsya->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->jnsya->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->jnsya->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->jnsya->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->carty_znyary->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->carty_znyary->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->carty_znyary->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->carty_znyary->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->zankoline_form->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->zankoline_form->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->zankoline_form->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->zankoline_form->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->national_card->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->national_card->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->national_card->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->national_card->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->balennama->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->balennama->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->balennama->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->balennama->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->kafalat->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->kafalat->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->kafalat->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->kafalat->requirement; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-right"> <?php if($attachs->payment->status==true){echo '<a target="_blank" href=http://localhost/registration/uploads/'.$attachs->payment->file_name.'>Open File</a>';}?>
			</td>
			<td><?php echo '<input style="margin-left:48%;margin-right:auto;width:100%;" '.
				 (($attachs->payment->status) ? 'checked' : "").' type="checkbox"/>'; ?>
			</td>
			<td><?php echo $attachs->payment->requirement; ?>
			</td>
		</tr>
		
	</table>

</body>