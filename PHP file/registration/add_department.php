<?php
header("Access-Control-Allow-Origin:*");
header("Content-type:application/json;charset=utf-8");

// header("Access-Control-Allow-Methods: POST");
include("config.php");
$request = $_POST['request'];
// $request = "getdep";
if($request=="addDep"){
	$dep_name_k= $_POST['dep_name_k'];
    $dep_name_e= $_POST['dep_name_e'];
    $sql_add_dep="insert into departments(dep_namek,dep_namee) VALUES ('$dep_name_k','$dep_name_e');";

    $result = $conn->query($sql_add_dep);
    $obj  =new  stdClass();
    if($result){
	   $obj->result = "success";
    }
    else{
	   $obj->result="failed";
    }
    echo json_encode($obj);
}
else if($request=="addDepinfo"){
    $head= $_POST['head'];
    $assist= $_POST['assist'];
    $depid= $_POST['depid'];
    $id_sem= $_POST['id_sem'];

    $sql_add_dep="insert into dep_head(head_name,assistance,id_dep,id_sem) VALUES ('$head','$assist',$depid,$id_sem);";

    $result = $conn->query($sql_add_dep);
    $obj  =new  stdClass();
    if($result){
       $obj->result = "success";
       // echo "login";
    }
    else{
       $obj->result="failed";
       // echo "decline";
    }
    echo json_encode($obj);
}
//+++++++++++++++++++++++++++++++++
else if($request=="getdep"){

 $sql="select id_dep,dep_namek,dep_namee FROM departments where id_dep>2;";
 $result = $conn->query($sql);
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id" => $row["id_dep"],
            "name"=>$row["dep_namek"],
            "namee"=>$row["dep_namee"]
        );
    }
    echo json_encode($data);	
}
else if($request=="getDepinfo"){

$depid=$_POST['depid'];

 $sql="select d.dep_namek,dh.head_name,dh.assistance, dh.id_dep,dh.id_sem,s.year
from dep_head dh , departments d, semsters s
where dh.id_dep=d.id_dep and dh.id_sem=s.id_sem and dh.id_dep=$depid";
 $result = $conn->query($sql);
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_dep" => $row["id_dep"],
            "dep_namek"=>$row["dep_namek"],
            "head_name"=>$row["head_name"],
            "assistance"=>$row["assistance"],
            "year"=>$row["year"],
            "id_sem"=>$row["id_sem"]
        );
    }
    echo json_encode($data);    
}
else if($request=="delete_dep") {
    $id_dep=$_POST['id_dep'];
    $id_sem=$_POST['id_sem'];
    $sql="DELETE FROM dep_head WHERE id_dep=$id_dep AND id_sem=$id_sem ;";
    $result = $conn->query($sql);
    $obj  =new  stdClass();
    if($result){
       $obj->result = "success";
    }
    else{
       $obj->result="failed";
    }
    echo json_encode($obj);
}


?>