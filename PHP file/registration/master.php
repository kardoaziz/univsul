<?php 

$info=json_decode($_POST['data']); 

// print_r($Pla);
// $students=json_decode($_POST['students']); 
// echo json_encode($info)
// echo $Pla[4];
// $students[] =array("f_name"=>"kardo","m_name"=>"othman","s_name"=>"aziz","l_name"=>"hama");
function getPla($grade)
{
	$Pla = ['كه‌وتووه‌', 'په‌سه‌ند','ناوه‌ند','باش','زۆرباش','نایاب'];
	$x = $grade-50;
	// echo $x.'<br>';
	if($x<0)
		{
			$p=$Pla[0];
		}
	else
		{
			$x = $x/10;
			$x = intval($x);
			// echo '$x = '.$x.'<br>';
			$p = $Pla[$x+1];
	// echo  '-->'.$p.'<br>';
		}
		return $p;
}
// echo getPla(55);
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Master Sheet</title>
    <link rel="stylesheet" type="text/css" href="../asset/css/style.css">
    <script type="text/javascript" src="../asset/js/hotsnackbar.js"></script>
</head>
<style type="text/css">
	.text-right{
		text-align: right !important;
	}
	.text-center{
		text-align: center !important;
	}
	.text-left{
		text-align: left !important;
	}
	.full{
		width:100% !important;
	}
	.ffull{
		min-width:100% !important;
	}
	.capital{
		text-transform: capitalize;
	}

	.bold{
	font-weight: bold;
	  -webkit-print-color-adjust: exact !important;

		}
	.greybg{
		background-color: #CCCCCC;
	}
	.redb{
		background-color: red;
		  -webkit-print-color-adjust: exact !important;

	}
	.orangeb{
		background-color: #FFA500;
	}
	table tr {
		height:30px;
		border:1px solid grey !important;
	}
	table tr td{
		border:1px solid grey !important;
		width: 80px;
	}
	table th {
		text-transform: capitalize;
		border:1px solid grey !important;
	}
	table {
		/*border:1px solid grey;*/
	}
		@media print
{     
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
    .redb{
			background-color: red !important;
				  -webkit-print-color-adjust: exact !important;

		}
		.orangeb{
			background-color: #FFA500 !important;
				  -webkit-print-color-adjust: exact !important;

		}
}
	</style>
	<body >
		<button style="height:40px;font-size:20px;float: right;margin:10px;" class="noprint" onclick="javascript:window.close()">Close</button>
		<button style="height:40px;font-size:20px;float:right;margin:10px; " class="noprint" onclick="javascript:window.print()">Print</button>
		<div style="font-size:25px; font-weight:bold;text-align:center;"> <?php echo $info->sem ?></div>
          <div style="font-size:18px; font-weight:bold;text-align:center;"><?php echo " به‌شی ".$info->dep ; ?></div>
  <!--   <div style="font-size:12px;text-align:center; line-height:14px;color:#0000ff;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;"><p style="margin: 0;font-size: 18px;line-height: 17px"><?php echo $info->sub ?></p></div>   -->
     <div style="font-size:12px;text-align:center; line-height:14px;color:#0000ff;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;"><p style="margin: 0;font-size: 18px;line-height: 17px"><?php echo " قۆناغی ".$info->stage; ?></p></div>  
  
<table id="example" class="display compact" style="width:100%;margin-top:20px;">
			<!-- <thead> -->
				<tbody>
			<?php 
			include("config.php");
			 $sql="SELECT distinct s.sub_name,s.unit
from grade  g,subjects s, stg_sem_sub sss,students st
where g.id_sub=s.id_sub and s.id_sub=sss.id_sub and sss.id_sem=g.id_sem and g.id_student=st.id_student and sss.id_sem=$info->id_sem and sss.id_stage=$info->id_stage  and s.id_dep=$info->id_dep ";
// echo $sql;
$result = $conn->query($sql);
$subs=[];
$units=[];
    while($row = $result->fetch_assoc()) {
        array_push($subs, $row['sub_name']);
        array_push($units, $row['unit']);
        } 
        // array_push($subs, 'curve');
        $k=0;
        echo '<tr>';
        echo '<td rowspan="3" style="width:40px;text-align:center;">تێبینی</td>';
        echo '<td rowspan="3" style="width:40px;text-align:center;">ئه‌نجام</td>';
        echo '<td rowspan="3" style="width:40px;text-align:center;">تێكرا</td>';
        foreach($subs as $sub)
        {
        	echo '<td text-center colspan="2" style="text-align:center;" class="greybg">'.$sub.'</td>';
        	
        }
        // print_r($subs);
			?>

		<td text-center rowspan="3" style="width:20px;text-align:center;" >
			خول
		</td>
		<td text-center style="width:200px;" >
			ناو
		</td>
		<td text-center >
			ژ
		</td>
	</tr>
<!-- </thead> -->
	
		<?php echo '<tr>';
		
        foreach($units as $u)
        {
        	echo '<td text-center colspan="2" style="text-align:center;">'.$u.'</td>';
        	
        }
        // print_r($subs);
			?>
			<td colspan="2" style="text-align:center;">
				یه‌كه‌
			</td>
		
	</tr>
	<?php echo '<tr >';
        foreach($units as $u)
        {
        	echo '<td text-center style="text-align:center;" class="greybg">ئاست</td>';
        	echo '<td text-center style="text-align:center;" class="greybg">نمره‌ی كۆتایی</td>';
        	
        }
        // print_r($subs);
			?>
			<td colspan="2" style="text-align:center;">
				
			</td>
		
	</tr>
		<?php 
		$sql="SELECT st.id_student,st.f_name,st.m_name,st.s_name,st.l_name,s.sub_name,g.corse1,g.corse2,g.final,g.crossing,g.crossing2,g.crossingfinal,g.curve, sss.id_stage,g.curve,g.id_sem,g.crossing_id_sem,s.unit
from grade  g,subjects s, stg_sem_sub sss,students st
where g.id_sub=s.id_sub and s.id_sub=sss.id_sub and sss.id_sem=g.id_sem and g.id_student=st.id_student and sss.id_sem=$info->id_sem and sss.id_stage=$info->id_stage  and s.id_dep=$info->id_dep  order by st.f_name";
            // echo $sql;

    $result = $conn->query($sql);
    $data=[];
    while($row = $result->fetch_assoc()) {
        // if(!$data[$row["id_student"]])
        // {
        //     $data['id_student']=[];
        // }
            $data[$row["id_student"]][$row["sub_name"]]=array( 
            "id_student" => $row["id_student"],
            "f_name" => $row["f_name"],
            "m_name" => $row["m_name"],
            "s_name" => $row["s_name"],
            "l_name" => $row["l_name"],
            "sub_name"=>$row["sub_name"],
            "unit"=>$row["unit"],
            "id_sem"=>$row["id_sem"],
            "corse1"=>$row["corse1"],
            "final"=>$row["final"],
            "curve"=>$row["curve"],
            "corse2"=>$row["corse2"],
            "crossing"=>$row["crossing"],
            "crossing2"=>$row["crossing2"],
            "crossingfinal"=>$row["crossingfinal"],
            "crossing_id_sem"=>$row["crossing_id_sem"],
            "id_stage"=>$row["id_stage"],
            "resultc1"=>$row["corse1"]+$row["final"]+$row["curve"],
            "resultc2"=>$row["corse1"]+$row["corse2"]+$row["curve"],
            "resultcr1"=>$row["crossing"]+$row["crossingfinal"]+$row["curve"],
            "resultcr2"=>$row["crossing"]+$row["crossing2"]+$row["curve"]
        
        );
        // }
        // $data[] =
        // echo json_encode($data); 
    }
    $i=0;
    $doc = new DomDocument;

    foreach ($data as $key => $value) {
    	$tg=0;
    	$tu=0;
    	$failed=0;
    	// $tu=0;
    	$t='';
    	echo '<tr>';
    	echo '<td rowspan="2" id="'.$value[$subs[0]]['id_student'].'-1" style="width:41px;text-align:center;">'.$t.'</td>';
        echo '<td rowspan="2" id="'.$value[$subs[0]]['id_student'].'-2" style="width:40px;text-align:center;"></td>';
        echo '<td rowspan="2" id="'.$value[$subs[0]]['id_student'].'-3" style="width:40px;text-align:center;"></td>';
        // document
        // $doc->getElementById('8').value="hh";
                    
        ?>
       <!--  <script>
        
        </script> -->
        <?php 
    	for ($j=0; $j<sizeof($subs);$j++) {
    		if($value[$subs[$j]]['crossing_id_sem']!=0)
    		{
    				
					echo '<td style="text-align:center;">'.getPla($value[$subs[$j]]['resultcr1']).'</td> ';
					if($value[$subs[$j]]['resultcr1']<50) $failed++;
					echo '<td style="text-align:center;" >'.$value[$subs[$j]]['resultcr1'].' </td>';
					if($value[$subs[$j]]['crossing2']==0){
						$tg+= $value[$subs[$j]]['unit'] * $value[$subs[$j]]['resultcr1'];}
					$tu+=$value[$subs[$j]]['unit'];
    		}else 
	    	{
					echo '<td style="text-align:center;">'.getPla($value[$subs[$j]]['resultc1']).'</td> ';
					if($value[$subs[$j]]['resultc1']<50) $failed++;
					echo '<td style="text-align:center;" >'.$value[$subs[$j]]['resultc1'].' </td>';
					if($value[$subs[$j]]['corse2']==0){	
						$tg+= $value[$subs[$j]]['unit'] * $value[$subs[$j]]['resultc1'];}
					$tu+=$value[$subs[$j]]['unit'];
		
    		}
	    	
    	}
		    	echo '<td style="text-align:center;">یه‌كه‌م</td>
		    	<td style="text-align:right;width:200px;" rowspan="2">'.$value[$subs[0]]['f_name'].' '.$value[$subs[0]]['m_name'].' '.$value[$subs[0]]['s_name'].' '.$value[$subs[0]]['l_name'].'</td>';
		    	echo '<td style="text-align:center;" rowspan="2">'.($i+1).'</td>

		    	</tr>';
    	/////// xwly dwam 
    	echo '<tr>';
    	for ($j=0; $j<sizeof($subs);$j++) {


    		if($value[$subs[$j]]['crossing_id_sem']!=0)
    		{
    			
    			if($value[$subs[$j]]['crossing2']!=0){
    			$tg+=$value[$subs[$j]]['unit'] * $value[$subs[$j]]['resultcr2'];	
    			echo '<td style="text-align:center;">'.getPla($value[$subs[$j]]['resultcr2']).'</td> ';
    			if($value[$subs[$j]]['resultcr2']<50) $failed++;
				echo '<td style="text-align:center;" >'.$value[$subs[$j]]['resultcr2'].' </td>';}
				else{
					echo '<td style="text-align:center;">-</td> ';
					echo '<td style="text-align:center;">-</td> ';
				}
			}else 
	    	{	
		    	
		    	if($value[$subs[$j]]['corse2']!=0){
		    		$tg+=$value[$subs[$j]]['unit'] * $value[$subs[$j]]['resultc2'];
		    	echo '<td style="text-align:center;">'.getPla($value[$subs[$j]]['resultc2']).'</td> ';
		    	if($value[$subs[$j]]['resultc1']<50) $failed++;
				echo '<td style="text-align:center;" >'.$value[$subs[$j]]['resultc2'].' </td>';
				}
				else{

					echo '<td style="text-align:center;">-</td> ';
					echo '<td style="text-align:center;">-</td> ';
				}
    		}
    		}	
				echo '<td style="text-align:center;">دووه‌م</td>';
    		
    		echo '<tr>';
    	$i+=1;
    	echo "<script>document.getElementById('".$value[$subs[0]]['id_student']."-1').innerHTML = '';</script>";
    				if($failed>0)
    				{
    					echo "<script>document.getElementById('".$value[$subs[0]]['id_student']."-2').innerHTML = '".getPla(0)."';</script>";  
    				}else{ 
                    	echo "<script>document.getElementById('".$value[$subs[0]]['id_student']."-2').innerHTML = '".getPla(($tg/$tu))."';</script>"; 
                    } 
                    echo "<script>document.getElementById('".$value[$subs[0]]['id_student']."-3').innerHTML = '".round(($tg/$tu),5)."';</script>";

    }
		?>
	</tbody>
	<!-- </ion-row> -->
	
		
	<!-- </ion-row> -->
	</table>
	
	</body>
	</html>
