<?php
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Methods: POST");
include("config.php");
error_reporting(0);

$request = $_POST['request'];
if($request=="addPerson"){

	$name= $_POST['name'];
	$fname=$_POST['fname'];
	$gname=$_POST['gname'];
	$gender=$_POST['gender'];
	$phone=$_POST['phone'];
	$email=$_POST['email'];
	$address=$_POST['address'];
	$sql_add_person= "insert into persons(f_name, m_name, l_name,gender, phone, email, address) VALUES ('$name','$fname','$gname','$gender','$phone','$email','$address');";
	$result = $conn->query($sql_add_person);
	$obj  =new  stdClass();
	$id_person=-1;
	if($result){
		$obj->result = "success";
		$id_person=$conn->insert_id;
	}
	else{
		$obj->result="failed";
	}
	$user=$_POST['user'];
	$pass=$_POST['pass'];
	$id_dep=$_POST['id_dep'];
	$emp_type=$_POST['emp_type'];
	$id_role=$_POST['id_role'];
	$sql_employee="insert into employees(username, password,id_dep,id_person,emp_type,id_role) VALUES ('$user','$pass',$id_dep,$id_person,'$emp_type',$id_role);";

	$result2 = $conn->query($sql_employee);
	$obj2  =new  stdClass();
	if($result2){
		$obj2->result2 = "success";
	}
	else{
		$obj2->result2="failed";
	}
			echo json_encode($obj);

}
//+++++++++++++++++++++++++++++++ update employee +++++++++++++++++++
else if($request=="updateEmployee"){

	$id_person= $_POST['id_person'];
	$name= $_POST['f_name'];
	$fname=$_POST['m_name'];
	$gname=$_POST['l_name'];
	$gender=$_POST['gender'];
	$phone=$_POST['phone'];
	$email=$_POST['email'];
	$address=$_POST['address'];
	$sql_add_person= "UPDATE persons SET f_name='$name',m_name='$fname',l_name='$gname',gender='$gender',phone='$phone',email='$email',address='$address' WHERE id_person=$id_person;";
	$result = $conn->query($sql_add_person);
	$obj  =new  stdClass();
	// $id_person=-1;
	if($result){
		$obj->result = "success";
		// $id_person=$conn->insert_id;
	}
	else{
		$obj->result="failed";
	}
	$user=$_POST['username'];
	$pass=$_POST['pass'];
	$emp_type=$_POST['emp_type'];
	$id_role=$_POST['id_role'];
	$emp_type=$_POST['emp_type'];
	$sql_employee="UPDATE employees SET id_role=$id_role,emp_type='$emp_type',username='$user',password='$pass' WHERE id_person=$id_person;";

	$result2 = $conn->query($sql_employee);
	$obj2  =new  stdClass();
	if($result2){
		$obj2->result2 = "success";
	}
	else{
		$obj2->result2="failed";
	}
			echo json_encode($obj);
}
//+++++++++++++++++++++++++++++++ get person+++++++++++++++++++++++++

else if ($request=="getperson") {
	$mydep = $_POST['dep'];
	$sql="SELECT p.id_person,p.f_name,p.m_name,p.l_name,p.gender,p.phone,p.email,p.address,e.id_person,e.id_role,e.username,e.password,r.id_role,r.role_name,d.id_dep,d.dep_namee
		FROM persons p,employees e,roles r,departments d
		WHERE p.id_person=e.id_person AND e.id_role=r.id_role AND e.id_dep=d.id_dep AND d.dep_namee IN('$mydep');";
		// echo $sql;
 		$result = $conn->query($sql);
	    while($row = $result->fetch_assoc()) {
	        $data[] = array( 
	        	"id_person" => $row["id_person"],
	            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["l_name"],
	            "f_name"=>$row["f_name"],
	            "m_name"=>$row["m_name"],
	            "l_name"=>$row["l_name"],
	            "gender" => $row["gender"],
	            "phone" => $row["phone"],
	            "email" => $row["email"],
	            "address" => $row["address"],
	            "role_name" => $row["role_name"],
	            "id_role" => $row["id_role"],
	            "username" => $row["username"],
	            "password" => $row["password"]
	        );
	    }
	    echo json_encode($data);	
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
else if ($request=="getmember") {
	$sql="SELECT p.id_person,p.f_name,p.m_name,p.l_name,p.gender,p.phone,p.email,p.address,e.id_person,e.id_role,r.id_role,r.role_name,d.id_dep,d.dep_namee,e.username
		FROM persons p,employees e,roles r ,departments d
		WHERE p.id_person=e.id_person AND e.id_role=r.id_role AND e.id_dep=d.id_dep 
        AND d.dep_namee NOT IN('registration','registrationadmission','registrationcertification');";

 		$result = $conn->query($sql);
	    while($row = $result->fetch_assoc()) {
	        $data[] = array( 
	        	"id_person" => $row["id_person"],
	            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["l_name"],
	            "gender" => $row["gender"],
	            "username" => $row["username"],
	            "phone" => $row["phone"],
	            "email" => $row["email"],
	            "address" => $row["address"],
	            "role_name" => $row["role_name"],
	            "id_role" => $row["id_role"]
	        );
	    }
	    echo json_encode($data);	
}
//+++++++++++++++++++++++++++++++ Add teacher++++++++++++++++++++++++
else if($request=="addteacher"){
	$name= $_POST['name'];
	$fname=$_POST['fname'];
	$gname=$_POST['gname'];
	$gender=$_POST['gender'];
	$phone=$_POST['phone'];
	$email=$_POST['email'];
	$address=$_POST['address'];



	$sql_add_person= "insert into persons(f_name, m_name, l_name,gender, phone, email, address) VALUES ('$name','$fname','$gname','$gender','$phone','$email','$address');";
	// echo $sql_add_person;
	$result = $conn->query($sql_add_person);
	$obj  =new  stdClass();
	$id_person=-1;
	if($result){
		$obj->result = "success";
		$id_person=$conn->insert_id;
		$id_dep=$_POST['id_dep'];
		$acadimic_title=$_POST['acadimic_title'];

		$sql="insert into teacher(id_person,id_dep,acadimic_title) VALUES ($id_person,$id_dep,'$acadimic_title');";

		$result2 = $conn->query($sql);
		$obj  =new  stdClass();
		if($result2){
			$obj->result = "success";
		}
		else{
			$obj->result="failed";
		}
		echo json_encode($obj);
	}
	else{
		$obj->result="failed";
		// echo "decline";
		echo json_encode($obj);
	}
}
else if($request=="updateTeacher"){
	$id_person= $_POST['id_person'];
	$name= $_POST['f_name'];
	$fname=$_POST['m_name'];
	$gname=$_POST['l_name'];
	$gender=$_POST['gender'];
	$phone=$_POST['phone'];
	$email=$_POST['email'];
	$address=$_POST['address'];

	$sql="UPDATE persons SET f_name='$name',m_name='$fname',l_name='$gname',gender='$gender',phone='$phone',email='$email',address='$address' WHERE id_person=$id_person;";

	$result = $conn->query($sql);
	$obj  =new  stdClass();
	if($result){
		$obj->result = "success";
		$id_dep=$_POST['id_dep'];
		$acadimic_title=$_POST['acadimic_title'];

		$sql2="UPDATE teacher SET id_dep=$id_dep,acadimic_title='$acadimic_title' WHERE id_person=$id_person;";

		$result2 = $conn->query($sql2);
		$obj2  =new  stdClass();
		if($result2){
			$obj2->result2 = "success";
		}
		else{
			$obj2->result2="failed";
		}
	}
	else{
		$obj->result="failed";
		
	}

	echo json_encode($obj);
}
else if($request=="deleteTeacher"){
	$id_person=$_POST['id_person'];

	$sql2="DELETE FROM teacher WHERE id_person=$id_person;";
	$sql="DELETE FROM persons WHERE id_person=$id_person;";

	$result = $conn->query($sql);
	$result2 = $conn->query($sql2);
	$obj  =new  stdClass();
	$obj2  =new  stdClass();
	if($result && $result2){
		$obj->result = "success";
		$obj2->result2 = "success";
		
	}
	else{
		$obj->result="failed";
		$obj2->result2="failed";
	}
	echo json_encode($obj);	

}
else if ($request=="getteacher") {
	$depid=$_POST['depid'];
	$sql="SELECT p.id_person,p.f_name,p.m_name,p.l_name,p.gender,
	p.phone,p.email,p.address,t.id_person,t.id_dep,t.acadimic_title,
	d.dep_namek
	FROM persons p,teacher t,departments d
	 WHERE p.id_person=t.id_person AND t.id_dep=d.id_dep AND d.id_dep=$depid;";

 		$result = $conn->query($sql);
	    while($row = $result->fetch_assoc()) {
	        $data[] = array( 
	        	"id_person" => $row["id_person"],
	            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["l_name"],
	            "f_name"=>$row["f_name"],
	            "m_name"=>$row["m_name"],
	            "l_name"=>$row["l_name"],
	            "gender" => $row["gender"],
	            "phone" => $row["phone"],
	            "email" => $row["email"],
	            "address" => $row["address"],
	            "id_dep" => $row["id_dep"],
	            "acadimic_title" => $row["acadimic_title"],
	            "dep_namek" => $row["dep_namek"]
	            
	        );
	    }
	    echo json_encode($data);	
}
// ++++++++++++++++++++++++++++++++++
else if ($request=="deleteMember"){
	$id_person=$_POST['id_person'];
	$sql="DELETE FROM persons WHERE id_person=$id_person;";
	$sql2="DELETE FROM employees WHERE id_person=$id_person;";
	$result = $conn->query($sql);
	$result2 = $conn->query($sql2);
	$obj  =new  stdClass();
	$obj2  =new  stdClass();
	if($result && $result2){
		$obj->result = "success";
		$obj2->result2 = "success";
		
	}
	else{
		$obj->result="failed";
		$obj2->result2="failed";
	}
	echo json_encode($obj);	

}

?>