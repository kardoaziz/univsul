<?php 

$info=json_decode($_POST['data']); 
$students=json_decode($_POST['students']); 
// echo $info;
$Pla = ['كه‌وتووه‌', 'په‌سه‌ند','ناوه‌ند','باش','زۆرباش','نایاب'];

// $students[] =array("f_name"=>"kardo","m_name"=>"othman","s_name"=>"aziz","l_name"=>"hama");
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Student Ranking</title>
    <link rel="stylesheet" type="text/css" href="../asset/css/style.css">
    <script type="text/javascript" src="../asset/js/hotsnackbar.js"></script>
</head>
<style type="text/css">
	body{
		width:21cm !important;
		margin-left: auto;
		margin-right: auto;
	}
	.text-right{
		text-align: right !important;
	}
	.text-center{
		text-align: center !important;
	}
	.text-left{
		text-align: left !important;
	}
	.full{
		width:100% !important;
	}
	.ffull{
		min-width:100% !important;
	}
	.capital{
		text-transform: capitalize;
	}
	.bold{
	font-weight: bold;
		}
		table tr {
			height:30px;
			border:1px solid grey !important;
		}
		table tr td{
			border:1px solid grey !important;
			text-align: center;
			font-weight: bold;
		}
		table {
			/*border:1px solid grey;*/
		}
		@media print
{     
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
}
	</style>
	<body>
		<button style="height:40px;font-size:20px;float:right; margin:10px;" class="noprint" onclick="javascript:window.close()">Close</button>
		<button style="height:40px;font-size:20px;float:right;margin:10px; " class="noprint" onclick="javascript:window.print()">Print</button>
		<table style="width:100%;border:0px;">
			<tr>
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-transform: capitalize;"> 
					به‌شی <?php  echo $info->dep;?><br>
					قۆناغی : <?php  echo $info->stage;?> <br>
				</td>
				<td colspan="5" style="border:0px white solid !important;font-size:20px; text-transform: capitalize;">
					ریزبه‌ندی خوێندكارانی <br>
					University of Slemani<br>
					College of Science <br>
					
					<?php  echo $info->sem;?> <br>
				</td>

			</tr>
		</table>
		<hr>
		<table style="width:100%;">
			<tr>
				<th>No.
				</th>
				<th>Name
				</th>
				<?php 
				if($info->grade==1)
					echo '<th>Total Grades
				</th>';
				else{
					echo '<th>ئاست
				</th>';
				}
				?>
				
				
			</tr>
			<?php 
				$i=1;
				foreach ($students as $id=>$stu )
				{
					$stu = get_object_vars( $stu );

					echo "<tr>
							<td style='text-align:center;'>$i
							</td>";
							
						echo 	"<td style='text-transform:capitalize;text-align:left;'>".$stu['f_name']." ".$stu['m_name']." ".$stu['s_name']." ".$stu['l_name']."
							</td>";
						
						if($info->grade==1)
						{
						echo "<td style='text-transform:capitalize;'>".$stu['grade']."
							</td>";}
							else {
								$p=(int)$stu['grade'];
								$p=$p-50;
								if($p<0) 
											$p=$Pla[0];
										else
										{
											$p = (int)$p/10;
											$p = $Pla[$p+1];
										}
								echo "<td style='text-transform:capitalize;'>".$p."
							</td>";
							}
							
						echo 	"
							
						</tr>";
						$i+=1;
				}
			?>
</table>
<hr>
		<table style="width:100%;margin-top:50px;">
			<tr style="padding-top:30px !important;">
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-align:center;">
					 واژۆی سه‌رۆكی به‌ش
				</td>
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-align:center;">
					واژۆی سه‌رۆكی لیژنه‌
				</td>
			</tr>
		</table>

	</body>
	</html>