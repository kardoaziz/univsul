<?php 

$info=json_decode($_POST['info']); 
$data=json_decode($_POST['data']); 
// print_r( $data );
// $students[] =array("f_name"=>"kardo","m_name"=>"othman","s_name"=>"aziz","l_name"=>"hama");
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Curve Form</title>
    <link rel="stylesheet" type="text/css" href="../asset/css/style.css">
    <script type="text/javascript" src="../asset/js/hotsnackbar.js"></script>
</head>
<style type="text/css">
	.text-right{
		text-align: right !important;
	}
	.text-center{
		text-align: center !important;
	}
	.text-left{
		text-align: left !important;
	}
	.full{
		width:100% !important;
	}
	.ffull{
		min-width:100% !important;
	}
	.capital{
		text-transform: capitalize;
	}
	.bold{
	font-weight: bold;
		}
		table tr {
			height:30px;
			border:1px solid grey !important;
		}
		table tr td{
			border:1px solid grey !important;
			font-weight: bold;
			font-size: 20px;
		}
		table {
			/*border:1px solid grey;*/
		}
		@media print
{     
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
}
	</style>
	<body style="margin-right:80px;margin-left:80px;">
		<button style="height:40px;font-size:20px;float:right; margin:10px;" class="noprint" onclick="javascript:window.close()">Close</button>
		<button style="height:40px;font-size:20px;float:right;margin:10px; " class="noprint" onclick="javascript:window.print()">Print</button>
		<table style="width:100%;border:0px;">
			<tr>
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-transform: capitalize;"> 
					 <?php  echo "به‌شی ".$info->dep."<br> قۆناغی ".$info->stage;
					 ?><br>
				</td>
				<td colspan="5" style="border:0px white solid !important;font-size:20px; text-transform: capitalize;">
					University of Slemani<br>
					College of Science <br>
					
					<?php  echo $info->sem;?> <br>
				</td>

			</tr>
		</table>
		<hr>
		<table style="width:100%;">
			<tr>
				<th>ڕێژه‌ی ده‌رچوون
				</th>
				<th>ژ.به‌سوود بوون
				</th>
				<th>ژ.كه‌وتوو
				</th>
				<th>ژ.ده‌رچوو
				</th>
				<th>بڕیار
				</th>
			</tr>
			<?php 
				$i=1;
				foreach ($data as $d )
				{
					$d = get_object_vars( $d );
					echo "<tr style='text-align:center;'><td>%".((($info->no_student-($info->no_failed-$d['helped']))/$info->no_student)*100)."</td>";
					echo "<td>".$d['helped']."</td>";
					echo "<td>".($info->no_failed-$d['helped'])."</td>";
					echo "<td>".($info->no_student-($info->no_failed-$d['helped']))."</td>";
					echo "<td>".$d['curve']."</td></tr>";

				}
			?>
</table>
<hr>
		<table style="width:100%;margin-top:50px;">
			<tr style="padding-top:30px !important;">
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-align:center;">
					 واژۆی سه‌رۆكی لیژنه‌
				</td>
				
			</tr>
		</table>

	</body>
	</html>