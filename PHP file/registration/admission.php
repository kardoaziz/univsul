<?php
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Methods: POST");
header("Content-type:application/json;charset=utf-8");
include("config.php");
error_reporting(0);
$request = $_POST['request'];
// $request = "getrequirement";
if($request=="addStudents"){
	$info= json_decode($_POST['info']);
	

	$sql="insert into students(stu_code, id_dep, f_name, m_name, s_name, l_name, phone, state, recieve_year, recieve_type, resident, gender, note,dob) VALUES ('$info->code',$info->id_dep,'$info->fname','$info->mname','$info->sname','$info->lname','$info->phone','$info->selected_state','$info->selected_sem','$info->selected_reciveType','$info->selected_resident','$info->gender','$info->note','$info->dob')";
	// echo $sql;
	$result = $conn->query($sql);
	$obj = new stdClass();
	
	if($result){
		$obj->result = "success";
		$obj->newid = $conn->insert_id;
		// $requirements = ['certificate','ID','jnsya','carty_znyary','zankoline_form','national_card','balennama','kafalat','payment'];
        // $sql ="select id_req from requirements";
        // $r = $conn->query($sql);
        // while($row = $r->fetch_assoc()) {
        //                 $sql="insert into attachments(file_name) values('');";
        //                 $r2 = $conn->query($sql);
        //                 // echo $sql."<br>";
        //                 $newattachid = $conn->insert_id;
        //                 $sql="insert into stu_has_req(id_student,id_req,id_attach) values(
        //                 ".$obj->newid.",".$row['id_req'].",".$newattachid.")";
        //                 $r3=$conn->query($sql);
        //                 // echo $sql."<br>";

        // }
		// foreach ($requirements as $r)
		// {
		// 	$sql="insert into attachments(file_name) values('');";
		// 		$result = $conn->query($sql);
		// 		$newid = $conn->insert_id;
		// 		// echo $sql;
		// 	$sql="insert into requirements(requirement,id_student,id_attach) values('$r',$obj->newid,$newid);";
		// 	// echo $sql;
		// 		$result = $conn->query($sql);
		// }
        $sql ="INSERT into grade (id_student,id_sub,id_sem) 
                select $obj->newid,s.id_sub,sm.id_sem
                from subjects s, semsters sm,stg_sem_sub sss,stage stg
                where s.id_sub=sss.id_sub and sm.id_sem=sss.id_sem and sss.id_stage=stg.id_stage and stg.id_stage=$info->id_stage and sss.id_sem=$info->id_sem";
                $r = $conn->query($sql);
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if($request == "getstudent")
{
	$dep_id = $_POST['dep_id'];

	$sql="SELECT distinct s.id_student,s.stu_code,d.id_dep,s.f_name,s.m_name,s.s_name,s.l_name,s.phone,s.state,sm.year as recieve_year,s.recieve_type,s.resident,s.gender,s.note,stg.id_stage,stg.stage_txtEn,sm.id_sem,s.dob
 from  students s, grade g, stg_sem_sub sss, semsters sm, stage stg, subjects sb,departments d
 where s.id_student=g.id_student 
 and g.id_sem = sm.id_sem 
 and g.id_sub=sb.id_sub 
 and sss.id_sem=sm.id_sem
 and sss.id_sub=sb.id_sub
 and stg.id_stage=sss.id_stage
 and s.id_dep=d.id_dep and d.id_dep=$dep_id";
	$result = $conn->query($sql);
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["s_name"]." ".$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "state"=>$row["state"],
            "recieve_year"=>$row["recieve_year"],
            "recieve_type"=>$row["recieve_type"],
            "resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "note"=>$row["note"],
            "id_stage"=>$row["id_stage"],
            "stage_txtEn"=>$row["stage_txtEn"],
            "stage_txt"=>$row["stage_txt"],
            "id_sem"=>$row["id_sem"]

        );
    }
    echo json_encode($data);	
}
else if($request == "semstagestudent")
{
	$dep_id = $_POST['dep_id'];
	$id_sem = $_POST['id_sem'];
	$id_stage = $_POST['id_stage'];

	$sql="SELECT  distinct s.id_student,s.stu_code,d.id_dep,s.f_name,s.m_name,s.s_name,s.l_name,s.phone,s.state,sm.year as recieve_year,s.recieve_type,s.resident,s.gender,s.note,stg.id_stage,stg.stage_txtEn,sm.id_sem,s.dob,
 (SELECT distinct Coalesce(s2.year,0) as nxtyears
from grade g2, semsters s2,stg_sem_sub sss2, subjects sub2,stage st2
where 
         s2.id_sem >$id_sem
        and g2.id_sem=s2.id_sem 
        and  g2.id_sem=sss2.id_sem and g2.id_sub=sss2.id_sub 
        and sub2.id_sub=sss2.id_sub
        and sss2.id_stage = st2.id_stage and g2.id_student=s.id_student
         limit 1
        ) as nextsem,(SELECT distinct Coalesce(st2.stage_txt,0) as nxtstage
from grade g2, semsters s2,stg_sem_sub sss2, subjects sub2,stage st2
where 
         s2.id_sem >$id_sem
        and g2.id_sem=s2.id_sem 
        and  g2.id_sem=sss2.id_sem and g2.id_sub=sss2.id_sub 
        and sub2.id_sub=sss2.id_sub
        and sss2.id_stage = st2.id_stage and g2.id_student=s.id_student
         limit 1
        ) as nextstage
 from  students s, grade g, stg_sem_sub sss, semsters sm, stage stg, subjects sb,departments d
 where s.id_student=g.id_student 
 and g.id_sem = sm.id_sem 
 and g.id_sub=sb.id_sub 
 and sss.id_sem=sm.id_sem
 and sss.id_sub=sb.id_sub
 and stg.id_stage=sss.id_stage
 and s.id_dep=d.id_dep and d.id_dep=$dep_id  and ((sm.id_sem=$id_sem and stg.id_stage=$id_stage ) or ( g.crossing_id_sem=$id_sem and  stg.id_stage=$id_stage)) ";
 // echo $sql;
	$result = $conn->query($sql);
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["s_name"]." ".$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "state"=>$row["state"],
            "recieve_year"=>$row["recieve_year"],
            "recieve_type"=>$row["recieve_type"],
            "resident"=>$row["resident"],
            "gender"=>$row["gender"],
             "nextstage"=>$row["nextstage"],
            "nextsem"=>$row["nextsem"],
            "note"=>$row["note"],
            "id_stage"=>$row["id_stage"],
            "stage_txtEn"=>$row["stage_txtEn"],
            "id_sem"=>$row["id_sem"]

        );
    }
    echo json_encode($data);	
}
else if($request == "semstudent")
{
	$dep_id = $_POST['dep_id'];

	// $sql=$dep_id = $_POST['dep_id'];
	$id_sem = $_POST['id_sem'];

	$sql="SELECT  distinct s.id_student,s.stu_code,d.id_dep,s.f_name,s.m_name,s.s_name,s.l_name,s.phone,s.state,sm.year as recieve_year,
s.recieve_type,s.resident,s.gender,s.note,stg.id_stage,stg.stage_txtEn,sm.id_sem,s.dob,
 (SELECT distinct Coalesce(s2.year,0) as nxtyears
from grade g2, semsters s2,stg_sem_sub sss2, subjects sub2,stage st2
where 
         s2.id_sem >$id_sem
        and g2.id_sem=s2.id_sem 
        and  g2.id_sem=sss2.id_sem and g2.id_sub=sss2.id_sub 
        and sub2.id_sub=sss2.id_sub
        and sss2.id_stage = st2.id_stage and g2.id_student=s.id_student
         limit 1
        ) as nextsem,(SELECT distinct Coalesce(st2.stage_txt,0) as nxtstage
from grade g2, semsters s2,stg_sem_sub sss2, subjects sub2,stage st2
where 
         s2.id_sem >$id_sem
        and g2.id_sem=s2.id_sem 
        and  g2.id_sem=sss2.id_sem and g2.id_sub=sss2.id_sub 
        and sub2.id_sub=sss2.id_sub
        and sss2.id_stage = st2.id_stage and g2.id_student=s.id_student
         limit 1
        ) as nextstage
 from  students s, grade g, stg_sem_sub sss, semsters sm, stage stg, subjects sb,departments d
 where s.id_student=g.id_student 
 and g.id_sem = sm.id_sem 
 and g.id_sub=sb.id_sub 
 and sss.id_sem=sm.id_sem
 and sss.id_sub=sb.id_sub
 and stg.id_stage=sss.id_stage
 and s.id_dep=d.id_dep and d.id_dep=$dep_id and sm.id_sem=$id_sem ";
	$result = $conn->query($sql);
	// echo $sql;
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["s_name"]." ".$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "state"=>$row["state"],
            "recieve_year"=>$row["recieve_year"],
            "recieve_type"=>$row["recieve_type"],
            "resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "nextstage"=>$row["nextstage"],
            "nextsem"=>$row["nextsem"],
            "note"=>$row["note"],
            "id_stage"=>$row["id_stage"],
            "stage_txtEn"=>$row["stage_txtEn"],
            "id_sem"=>$row["id_sem"]

        );
    }
    echo json_encode($data);	
}else if($request == "stagestudent")
{
	$dep_id = $_POST['dep_id'];
	$id_stage = $_POST['id_stage'];

	$sql="SELECT  distinct s.id_student,s.stu_code,d.id_dep,s.f_name,s.m_name,s.s_name,s.l_name,s.phone,s.state,sm.year as recieve_year,s.recieve_type,s.resident,s.gender,s.note,stg.id_stage,stg.stage_txtEn,sm.id_sem,s.dob,
 (SELECT distinct Coalesce(s2.year,0) as nxtyears
from grade g2, semsters s2,stg_sem_sub sss2, subjects sub2,stage st2
where 
          g2.id_sem=s2.id_sem 
        and  g2.id_sem=sss2.id_sem and g2.id_sub=sss2.id_sub 
        and sub2.id_sub=sss2.id_sub
        and sss2.id_stage = st2.id_stage and g2.id_student=s.id_student
         limit 1
        ) as nextsem,(SELECT distinct Coalesce(st2.stage_txt,0) as nxtstage
from grade g2, semsters s2,stg_sem_sub sss2, subjects sub2,stage st2
where 
          g2.id_sem=s2.id_sem 
        and  g2.id_sem=sss2.id_sem and g2.id_sub=sss2.id_sub 
        and sub2.id_sub=sss2.id_sub
        and sss2.id_stage = st2.id_stage and g2.id_student=s.id_student
         limit 1
        ) as nextstage
 from  students s, grade g, stg_sem_sub sss, semsters sm, stage stg, subjects sb,departments d
 where s.id_student=g.id_student 
 and g.id_sem = sm.id_sem 
 and g.id_sub=sb.id_sub 
 and sss.id_sem=sm.id_sem
 and sss.id_sub=sb.id_sub
 and stg.id_stage=sss.id_stage
 and s.id_dep=d.id_dep and d.id_dep=$dep_id and stg.id_stage=$id_stage ";
	$result = $conn->query($sql);
	// echo $sql;
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["s_name"]." ".$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "state"=>$row["state"],
            "recieve_year"=>$row["recieve_year"],
            "recieve_type"=>$row["recieve_type"],
            "resident"=>$row["resident"],
            "gender"=>$row["gender"],
             "nextstage"=>$row["nextstage"],
            "nextsem"=>$row["nextsem"],
            "note"=>$row["note"],
            "id_stage"=>$row["id_stage"],
            "stage_txtEn"=>$row["stage_txtEn"],
            "id_sem"=>$row["id_sem"]

        );
    }
    echo json_encode($data);	
}
else if($request == "searchstudent")
{
	$word = $_POST['word'];

	$sql="SELECT s.id_student,s.stu_code ,s.id_dep,d.dep_namek as department,s.f_name, s.m_name,s.s_name,s.l_name,s.phone,s.state,s.recieve_year,s.recieve_type,s.resident,s.gender,s.note ,s.dob 
from students s ,departments d
 where s.id_dep=d.id_dep and (s.f_name like '%$word%' 
 or s.m_name like '%$word%' 
 or s.l_name like '%$word%' 
 or s.s_name like '%$word%'  
 or s.stu_code like '%$word%') 
		
";
	$result = $conn->query($sql);
	// echo $sql;
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "name"=>$row["f_name"]." ".$row["m_name"]." ".$row["s_name"]." ".$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "department"=>$row["department"],
            "state"=>$row["state"],
            "recieve_year"=>$row["recieve_year"],
            "recieve_type"=>$row["recieve_type"],
            "resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "note"=>$row["note"]
            

        );
    }
    echo json_encode($data);	
}
else if($request == "getselectedStudent")
{
	$student = json_decode($_POST['student']);
	// echo $student->recieve_type;
	if($student->recieve_type == "ئاسایی")
	{
		$sql = "SELECT s.id_student,s.stu_code ,s.id_dep,s.f_name, s.m_name,s.s_name,s.l_name,s.phone,s.state,s.recieve_year,s.recieve_type,s.resident,s.gender,s.note,ord.id_student,ord.id_stage,s.recieve_year ,d.dep_namek,stg.stage_txt,stg.stage_txtEn,d.dep_namee,s.dob
from students s ,ordinary ord,departments d, stage stg
WHERE s.id_student=ord.id_student   
            and s.id_dep=d.id_dep 
            and s.id_student=ord.id_student and stg.id_stage=ord.id_stage
			and s.id_student=$student->id_student  ";
		$result = $conn->query($sql);
		// echo $sql;
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "fname"=>$row["f_name"],
            "mname"=>$row["m_name"],
            "sname"=>$row["s_name"],
            "lname"=>$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "dep_namee"=>$row["dep_namee"],
            "selected_stage"=>$row["stage_txtEn"],
            "selected_dep"=>$row["dep_namek"],
            "selected_state"=>$row["state"],
            "selected_sem"=>$row["recieve_year"],
            "selected_reciveType"=>$row["recieve_type"],
            "selected_resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "note"=>$row["note"]
        );
    }
    echo json_encode($data);
	}
    else if($student->recieve_type == "پارالێڵ"){

		$sql = "SELECT s.id_student,s.stu_code ,s.id_dep,s.f_name, s.m_name,s.s_name,s.l_name,s.phone,s.state,s.recieve_year,s.recieve_type,s.resident,s.gender,s.dob,s.note,p.id_payment,p.id_sem,p.id_attach,p.id_stage,p.price,p.payment,p.datee,dep.dep_namek,dep.dep_namee,stg.stage_txt,stg.stage_txtEn FROM students s,payments p,departments dep,stage stg WHERE  s.id_student=p.id_student AND s.id_dep=dep.id_dep and s.id_student=p.id_student and stg.id_stage=p.id_stage AND s.id_student=$student->id_student ";
        // echo $sql;
        $result = $conn->query($sql);
        
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "fname"=>$row["f_name"],
            "mname"=>$row["m_name"],
            "sname"=>$row["s_name"],
            "lname"=>$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "selected_dep"=>$row["dep_namek"],
            "selected_stage"=>$row["stage_txtEn"],
            "selected_state"=>$row["state"],
            "selected_sem"=>$row["recieve_year"],
            "selected_reciveType"=>$row["recieve_type"],
            "selected_resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "note"=>$row["note"],
            "date_of_payment"=>$row["datee"],
            "price"=>$row["price"],
            "payment"=>$row["payment"]
        );

    }
    echo json_encode($data);
	}
    else if($student->recieve_type == "دابەزین"){

        $sql = "SELECT s.id_student,s.stu_code ,s.id_dep,s.f_name, s.m_name,s.s_name,s.l_name,s.phone,s.state,s.recieve_year,s.recieve_type,s.resident,s.gender,s.dob,s.note,des.id_student,des.id_stage,des.dep_name,des.univ_name, dep.dep_namek,stg.stage_txt,stg.stage_txtEn,dep.dep_namee from students s ,descents des,departments dep,stage stg WHERE s.id_student=des.id_student AND s.id_dep=dep.id_dep and s.id_student=des.id_student and stg.id_stage=des.id_stage AND s.id_student=$student->id_student ";

        $result = $conn->query($sql);
        // echo $sql;
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "fname"=>$row["f_name"],
            "mname"=>$row["m_name"],
            "sname"=>$row["s_name"],
            "lname"=>$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "dep_namee"=>$row["dep_namee"],
            "selected_stage"=>$row["stage_txtEn"],
            "selected_dep"=>$row["dep_namek"],
            "selected_state"=>$row["state"],
            "selected_sem"=>$row["recieve_year"],
            "selected_reciveType"=>$row["recieve_type"],
            "selected_resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "note"=>$row["note"],
            // "dep_name_descent"=>$row["dep_name"],
            // "univ_name_descent"=$row["univ_name"],
        );
    }
    echo json_encode($data);
	}
	else if($student->recieve_type == "گواستنەوە")
	{
		// echo "transfer";
		$sql = "SELECT s.id_student,s.stu_code ,s.id_dep,s.f_name, s.m_name,s.s_name,s.l_name,s.phone,s.state,s.recieve_year,s.recieve_type,s.resident,s.gender,s.note,t.id_student,t.id_stage,s.recieve_year ,d.dep_namek,stg.stage_txt,stg.stage_txtEn,t.resons,t.dep_name,t.univ_name,t.file_name,s.dob
from students s ,transfer t,departments d, stage stg
WHERE s.id_student=t.id_student   
            and s.id_dep=d.id_dep 
            and s.id_student=t.id_student 
            and stg.id_stage=t.id_stage and t.id_student=$student->id_student";
            // echo $sql;
           $result = $conn->query($sql);
		
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "fname"=>$row["f_name"],
            "mname"=>$row["m_name"],
            "sname"=>$row["s_name"],
            "lname"=>$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "resons"=>$row["resons"],
            "dep_name_transfer"=>$row["dep_name"],
            "file_name"=>$row["file_name"],
            "old_grade"=>strlen($row["file_name"])>2,
            "univ_name_transfer"=>$row["univ_name"],
            "selected_dep"=>$row["dep_namek"],
            "selected_stage"=>$row["stage_txtEn"],
            "selected_state"=>$row["state"],
            "selected_sem"=>$row["recieve_year"],
            "selected_reciveType"=>$row["recieve_type"],
            "selected_resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "note"=>$row["note"]
            

        );

    }
    echo json_encode($data);
	} else if($student->recieve_type == "گەڕاوە")
	{
		$sql = "SELECT s.id_student,s.stu_code ,s.id_dep,s.f_name, s.m_name,s.s_name,s.l_name,s.phone,s.state,s.recieve_year,s.recieve_type,s.resident,s.gender,s.note,re.id_student,re.id_stage, stg.stage_txtEn,stg.stage_txt,stg.id_stage,d.dep_namek,s.dob
from students s ,recursives re, stage stg,departments d
WHERE s.id_student=re.id_student  and d.id_dep=s.id_dep  
AND re.id_stage=stg.id_stage and s.id_student=$student->id_student   ";
 // echo $sql;
           $result = $conn->query($sql);
		
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id_student" => $row["id_student"],
            "code"=>$row["stu_code"],
            "id_dep"=>$row["id_dep"],
            "fname"=>$row["f_name"],
            "mname"=>$row["m_name"],
            "sname"=>$row["s_name"],
            "lname"=>$row["l_name"],
            "dob"=>$row["dob"],
            "phone"=>$row["phone"],
            "resons"=>$row["resons"],
            "dep_name"=>$row["dep_name"],
            "selected_stage"=>$row["stage_txtEn"],
            "selected_dep"=>$row["dep_namek"],
            "selected_state"=>$row["state"],
            "selected_sem"=>$row["recieve_year"],
            "selected_reciveType"=>$row["recieve_type"],
            "selected_resident"=>$row["resident"],
            "gender"=>$row["gender"],
            "note"=>$row["note"]
        );
    }
    echo json_encode($data);
	} 
	
		
}else if ($request == "getstage") {
	// echo "get stage";
	$sql_stage="SELECT id_stage,stage_num,stage_txt,stage_txtEn FROM stage;";
	// echo $sql_stage;
	$results = $conn->query($sql_stage);
	while($row = $results->fetch_assoc()) {
        $data[] = array( 
        	"id_stage"=>$row["id_stage"],
        	"stage_num"=>$row["stage_num"],
        	"stagetxt"=>$row["stage_txt"],
        	"stagetxtE"=>$row["stage_txtEn"]
        );
    }
    echo json_encode($data);	
}
else if ($request == "semesters") {
	// echo "get stage";
	$sql_stage="SELECT * FROM semsters;";
	// echo $sql_stage;
	$results = $conn->query($sql_stage);
	while($row = $results->fetch_assoc()) {
        $data[] = array( 
        	"id_sem"=>$row["id_sem"],
        	"year"=>$row["year"],
            "locked"=>$row["locked"]
        );
    }
    echo json_encode($data);	
}
else if($request == "showStudentPayment"){
    $id_student=$_POST['id_student'];
    $sql="SELECT p.id_payment,p.id_student,p.id_sem,p.id_attach,p.price,p.payment,p.datee,s.year FROM payments p,semsters s WHERE p.id_student=$id_student AND p.id_sem=s.id_sem;";
    // echo $sql;
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_payment"=>$row["id_payment"],
            "id_student"=>$row["id_student"],
            "id_sem"=>$row["id_sem"],
            "id_attach"=>$row["id_attach"],
            "year"=>$row["year"],
            "price"=>$row["price"],
            "payment"=>$row["payment"],
            "dateOFpayment"=>$row["datee"]
        );
    }
    echo json_encode($data);
}
else if ($request == "depsubject") {
	// echo "get stage";
	$sql_stage="SELECT * FROM subjects  where id_dep=".$_POST['id_dep'];
	// echo $sql_stage;
	$results = $conn->query($sql_stage);
	while($row = $results->fetch_assoc()) {
        $data[] = array( 
        	"id_sub"=>$row["id_sub"],
        	"id_dep"=>$row["id_dep"],
        	"sub_name"=>$row["sub_name"],
        	"stage"=>$row["stage"],
        	"unit"=>$row["unit"],
        	"sub_type"=>$row["sub_type"],
        	"h_theory"=>$row["h_theory"],
        	"h_practice"=>$row["h_practice"]
        );
    }
    echo json_encode($data);	
}
else if ($request == "getrequirement") {
	// echo "get stage";
	$id = $_POST['id'];
	// $id=6;
	$sql="select a.id_attach,a.file_name,r.id_req,r.requirement,sth.id_student
from attachments a, requirements r, stu_has_req sth
where a.id_attach = sth.id_attach and r.id_req=sth.id_req and sth.id_student=".$id;
	// echo $sql;
	$results = $conn->query($sql);
	$data=[];
	while($row = $results->fetch_assoc()) {
        $data[$row["requirement"]][] = array( 
        	"id_attach"=>$row["id_attach"],
        	"file_name"=>$row["file_name"],
        	"id_req"=>$row["id_req"],
        	"requirement"=>$row["requirement"],
        	"status"=>(strlen($row['file_name']) > 2 ? true : false),
        	"id_student"=>$row["id_student"]
        );
    }
    echo json_encode($data);	
}
else if ($request == "offersubject") {
	$subs = json_decode($_POST['subjects']);
	$stage = json_decode($_POST['stage']);
	$sem = json_decode($_POST['semester']);


	foreach ($subs as $sub) {
		$sql = "Insert into stg_sem_sub(id_stage,id_sem,id_sub) values($stage,$sem,$sub->id_sub)";	
	// echo $sql;
	$results = $conn->query($sql);
	}
    $sql = "select s.id_student from students s, ordinary o where o.id_stage=$stage and s.state='bardawam' and s.id_student=o.id_student";
    $results = $conn->query($sql);
    $students = [];
    while($row = $results->fetch_assoc()) {
        array_push($students, $row['id_student']);
    }
    foreach ($subs as $sub) {
        if($sub->offer_type=="normal"){
        foreach ($students as $stu) {
            $sql ="Insert into grade(id_student,id_sub,id_sem,offer_type,state) 
                        values($stu,$sub->id_sub,$sem,'normal','ئاسایی')";
                        echo $sql;
            $results = $conn->query($sql);
        }}
    }
	$obj = new stdClass();
	if($results){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if ($request == "deleteoffersubject") {
	$data = json_decode($_POST['data']);



		$sql = "delete from stg_sem_sub where id_sub=$data->id_sub and id_sem=$data->id_sem and id_stage=$data->id_stage";	
	// echo $s_Pql;
	$results = $conn->query($sql);
	
	$obj = new stdClass();
	if($results){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if ($request == "deleteenroll") {
    
    $data = json_decode($_POST['data']);



        $sql = "delete from grade where id_sub=$data->id_sub and id_sem=$data->id_sem and id_student=$data->id_student";  
    // echo $s_Pql;
    $results = $conn->query($sql);
    
    $obj = new stdClass();
    if($results){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}else if ($request == "offersub") {
    
    $id_sub = $_POST['id_sub'];
    $id_sem = $_POST['id_sem'];
    $id_student = $_POST['id_student'];



        $sql = "insert into grades(id_sub,id_student,id_sem) values( id_sub=$id_sub , id_sem=$id_sem , id_student=$id_student)";  
    // echo $s_Pql;
    $results = $conn->query($sql);
    
    $obj = new stdClass();
    if($results){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request == "getoffersubject") {
	$sem = $_POST['sem'];
	$dep = $_POST['dep'];
	$stage = $_POST['stage'];

 $sql ="select sb.id_sub,sb.sub_name,sb.sub_type,sb.h_theory,sb.h_practice,sb.unit ,sm.id_sem,st.id_stage,sb.id_dep,st.stage_txt
from  stage st,stg_sem_sub sss, semsters sm, subjects sb
where st.id_stage=sss.id_stage and sm.id_sem=sss.id_sem and sb.id_sub=sss.id_sub and sm.id_sem=$sem and st.id_stage=$stage and sb.id_dep=$dep";
// echo $sql;
$results = $conn->query($sql);
	while($row = $results->fetch_assoc()) {
        $data[] = array( 
        	"id_sub"=>$row["id_sub"],
        	"id_dep"=>$row["id_dep"],
        	"id_sem"=>$row["id_sem"],
        	"sub_name"=>$row["sub_name"],
        	"id_stage"=>$row["id_stage"],
        	"unit"=>$row["unit"],
            "stage"=>$row["stage_txt"],
        	"sub_type"=>$row["sub_type"],
        	"h_theory"=>$row["h_theory"],
        	"h_practice"=>$row["h_practice"]
        );
    }
    echo json_encode($data);
	
}
else if ($request == "getcertificatedata") {
    $student = json_decode($_POST['student']);
 // echo "get grades";
    $sql ="SELECT g.id_sem,sem.year,count(*) failedclass
            FROM grade g, subjects sub, students s,stg_sem_sub sss,semsters sem
            where g.id_sub=sub.id_sub  and  g.id_student=s.id_student and
            (corse1+final+curve <50 ) and 
            (corse2+corse1+curve<50 ) and 
            (crossing+crossingfinal+curve<50) and sem.id_sem=sss.id_sem and 
            (crossing+crossing2+curve<50)   and sss.id_sem =g.id_sem and sss.id_sub=g.id_sub and g.id_student=".$student->id_student."
                        group by g.id_sem,sem.year";
    $results = $conn->query($sql);
    while($row = $results->fetch_assoc()) {
         $data_[]= array( 
            "id_sem"=>$row["id_sem"],
            "year"=>$row["year"],
            "failedclass"=>$row["failedclass"]
           
        );
    }
    if(sizeof($data_))
    {
        echo json_encode($data_);
    }
    else{


 $sql ="select s.id_student,s.f_name,s.m_name,s.s_name,s.l_name,sm.year,stg.stage_txt,g.final,g.corse1,g.corse2,sb.sub_name,sb.h_theory,sb.h_practice,sb.unit,g.crossing,g.crossing2,g.crossingfinal,g.curve
from students s, stage stg, semsters sm, stg_sem_sub sss , grade g, subjects sb
where s.id_student=g.id_student and
           g.id_sem =sss.id_sem and
           sss.id_sem=sm.id_sem and
           sss.id_sub = sb.id_sub and 
           sss.id_stage =stg.id_stage and
           sb.id_sub=g.id_sub and s.id_student=".$student->id_student." 
order by sm.year
           ";
// echo $sql;
           $data=[];
$results = $conn->query($sql);
    while($row = $results->fetch_assoc()) {
         $data[$row["year"]][$row["sub_name"]]= array( 
            "id_student"=>$row["id_student"],
            "f_name"=>$row["f_name"],
            "m_name"=>$row["m_name"],
            "s_name"=>$row["s_name"],
            "l_name"=>$row["l_name"],
            "sub_name"=>$row["sub_name"],
            "h_theory"=>$row["h_theory"],
            "h_practice"=>$row["h_practice"],
            "unit"=>$row["unit"],
            "year"=>$row["year"],
            "stage"=>$row["stage_txt"],
            "final"=>$row["final"],
            "corse1"=>$row["corse1"],
            "crossing"=>$row["crossing"],
            "crossing2"=>$row["crossing2"],
            "crossingfinal"=>$row["crossingfinal"],
            "curve"=>$row["curve"],
            "corse2"=>$row["corse2"]
        );
    }
    echo json_encode($data);
}
    
}
else if ($request == "adddescent") {
	$stage=$_POST['stage'];
	$stu_id=$_POST['stu_id'];
	$univ_name=$_POST['univ_name'];
    $dep_name=$_POST['dep_name'];
    $selectedDescentType=$_POST['selectedDescentType'];
	$selectedDescentState=$_POST['selectedDescentState'];
	
	$sql="INSERT INTO descents(id_student,id_stage,dep_name,univ_name, descent_type,descent_state) VALUES ($stu_id,$stage,'$dep_name','$univ_name','$selectedDescentType','$selectedDescentState');";
// echo $sql;
	$result = $conn->query($sql);
	$obj = new stdClass();
	if($result){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if ($request == "addpayment") {
    $id_student=$_POST['id_student'];
    $id_attach=$_POST['id_attach'];
    $id_stage=$_POST['id_stage'];
    $id_semester=$_POST['id_semester'];
    $price=$_POST['price'];
    $payment=$_POST['payment'];
    $date_of_payment=$_POST['date_of_payment'];
    
    $sql="INSERT INTO payments(id_student,id_sem,id_attach,id_stage,price, payment,datee) VALUES ($id_student,$id_semester,$id_attach,$id_stage,$price,$payment,'$date_of_payment');";
  // echo $sql;
    $result = $conn->query($sql);
    $obj = new stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request == "addfromxlsx") {
    $students=json_decode($_POST['stu']);
    $info=json_decode($_POST['info']);

    foreach ($students as $stu) {


        $newDate = date("Y-m-d", strtotime($stu->dob));
        $sql="INSERT into students(stu_code, id_dep, f_name, m_name, s_name, l_name, phone, state, recieve_year, recieve_type, resident, gender, note,dob) VALUES ('$stu->code',$info->id_dep,'$stu->f_name','$stu->m_name','$stu->s_name','$stu->l_name','$stu->phone','$info->selected_state','$info->selected_sem','$info->selected_reciveType','$info->selected_resident','$stu->gender','$info->note','$newDate')";

        echo $sql.'<br>';
        $result = $conn->query($sql);
        $last_id = $conn->insert_id;
        // if($info->selected_reciveType=="ordinary")
            $sql="INSERT INTO ordinary(id_student,id_stage) VALUES ($last_id,$info->id_stage);";
        // else
        //     $sql="";
        $result = $conn->query($sql);

    }

    // 
    $obj = new stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}

else if ($request == "addtransfer") {
	$stage=$_POST['stage'];
	$stu_id=$_POST['stu_id'];
	$univ_name=$_POST['univ_name'];
	$dep_name=$_POST['dep_name'];
	$reson=$_POST['reson'];
	$id_sem=$_POST['id_sem'];
	

	$sql="INSERT INTO transfer(id_student,id_stage,resons,dep_name,univ_name) VALUES ($stu_id,$stage,'$reson','$dep_name','$univ_name');";
// echo $sql;
	$result = $conn->query($sql);
	$obj = new stdClass();
	if($result){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if ($request == "addrequirement") {
	$file=$_POST['file'];
	$requirement=$_POST['requirement'];
	$id=$_POST['id'];
	

    $sql = "insert into attachments(file_name) values('$file')";
    $r = $conn->query($sql);
    $newattachid = $conn->insert_id;
	$sql="insert into stu_has_req(id_student,id_attach,id_req) 
            select $id,$newattachid,id_req from requirements where requirement='$requirement'";

	if($requirement=="transferOldGrade")
	{
		$sql="update transfer set file_name='$file' where id_student=$id";
	}
	// echo $sql;

	$result = $conn->query($sql);
	$obj = new stdClass();
	if($result){
		$obj->result = "success";
		// $newid = $conn->insert_id;
		// $sql = "INSERT INTO requirements (requirement,id_student,id_attach) values('$requirement',$id,$newid)";
		// echo $sql;
		// $result = $conn->query($sql);

	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if ($request == "addrecursive") {
	$stage=$_POST['stage'];
	$stu_id=$_POST['stu_id'];
	

	$sql="INSERT INTO recursives(id_student,id_stage) VALUES ($stu_id,$stage);";
//echo $sql;
	$result = $conn->query($sql);
	$obj = new stdClass();
	if($result){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if ($request == "addordinary") {
	$stage=$_POST['stage'];
	$stu_id=$_POST['stu_id'];
	

	$sql="INSERT INTO ordinary(id_student,id_stage) VALUES ($stu_id,$stage);";

	$result = $conn->query($sql);
	$obj = new stdClass();
	if($result){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}//
else if ($request == "addstudenttonewsem") {
    $id_sem=$_POST['id_sem'];
    $id_old_sem=$_POST['id_old_sem'];
    $stu_id=$_POST['stu_id'];
    $stage=$_POST['stage'];
    $oboor=$_POST['oboor'];
    $recieve_type=$_POST['receive_type'];
    $passing_limit=$_POST['passing_limit'];
     $obj = new stdClass();
     // $obj->ids=[];
    $sql = "SELECT count(*) as crossing 
            FROM grade
            where (corse1+final+curve <$passing_limit) and (corse2+corse1+curve<$passing_limit) and (crossing+crossingfinal+curve<$passing_limit) and (crossing+crossing2+curve<$passing_limit)  and id_student=$stu_id and (id_sem=$id_old_sem or crossing_id_sem=$id_old_sem)";
            // echo $oboor;
            // echo $sql;
            $result = $conn->query($sql);
            // print_r($result);
            $row=mysqli_fetch_row($result);
            // echo print_r($row); 
            // echo print_r($row[0]); 
            if($row[0]>$oboor)
            {
                   
                   
                        $obj->result="failed to pass student ";
                        $obj->ids=($stu_id);
                    
                    echo json_encode($obj);
            }else
            {
                // echo "num oboor".$row['crossing'];
                if($row[0] != 0)
                    {

                        $sql = "SELECT * 
                    FROM grade
                    where (corse1+final+curve <$passing_limit) and (corse2+corse1+curve<$passing_limit)  and (crossing+crossing2+curve<$passing_limit)  and id_student=$stu_id and (id_sem=$id_old_sem or crossing_id_sem=$id_old_sem)";
                    // echo '<br>'.$sql;
                     $result = $conn->query($sql);
                       while($row = $result->fetch_assoc()) {
                        $sql = "UPDATE  grade  set offer_type='crossing',crossing_id_sem=$id_sem,crossing=0,crossingfinal=0,crossing2=0 where id_student=$stu_id and (id_sem=$id_old_sem or crossing_id_sem=$id_old_sem)  and id_sub=".$row['id_sub'];
                        // echo '<br>'.$sql;
                        $r = $conn->query($sql);
                       }
                       // echo "done crossing1";
                    }

           

                // echo "done crossing2";
                $sql ="INSERT into grade (id_student,id_sub,id_sem) 
                select  $stu_id,s.id_sub,sm.id_sem
                from subjects s, semsters sm,stg_sem_sub sss,stage stg
                where s.id_sub=sss.id_sub and sm.id_sem=sss.id_sem and sss.id_stage=stg.id_stage and stg.id_stage=$stage and sss.id_sem=$id_sem";
                $r = $conn->query($sql);

                // $sql ="Update ordinary set id_stage=id_stage+1 where id_student=$stu_id";
                // $r = $conn->query($sql);

                // echo '<br>'.$sql;
                if($recieve_type=="ئاسایی")
                {
                    $sql="update ordinary set id_stage=$stage where id_student=$stu_id";
                // echo $sql;
                    $r=$conn->query($sql);
                 }else if ($recieve_type=="دابەزین") {
                    $sql="UPDATE descents SET id_stage=$stage WHERE id_student=$stu_id;";
                        $r=$conn->query($sql);
                }else if ($recieve_type=="گەڕاوە") {
                    $sql="UPDATE recursives SET id_stage=$stage WHERE id_student=$stu_id;";
                        $r=$conn->query($sql);
                }
                else if ($recieve_type=="گواستنەوە") {
                    $sql="UPDATE transfer SET id_stage=$stage WHERE id_student=$stu_id;";
                    $r=$conn->query($sql);
                }

                // echo $sql;
                $result = $conn->query($sql);
                $obj = new stdClass();
                if($result){
                    $obj->result = "success";
                }
                else{
                    $obj->result="failed";
                }
                echo json_encode($obj);
            }
}
else if($request == "retakeclass")
{
        $info=json_decode($_POST['info']);
        $sql ="INSERT into grade (id_student,id_sub,id_sem,offer_type) 
                select  $info->stu_id,s.id_sub,sm.id_sem,'second_year'
                from subjects s, semsters sm,stg_sem_sub sss,stage stg
                where s.id_sub=sss.id_sub and sm.id_sem=sss.id_sem and sss.id_stage=stg.id_stage and stg.id_stage=$info->stage and sss.id_sem=$info->id_sem";
                echo $sql;
                $result = $conn->query($sql);
        $obj = new stdClass();
        if($result){
            $obj->result = "success";
        }
        else{
            $obj->result="failed";
        }
        echo json_encode($obj);
}
else if ($request == "savetransferinfo") {
	$info=json_decode($_POST['info']);
	
	$sql="update transfer set univ_name='$info->univ_name_trasfer', dep_name='$info->dep_name_transfer', resons='' where id_student=$info->id_student";

	$result = $conn->query($sql);
	$obj = new stdClass();
	if($result){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
	}
	echo json_encode($obj);
}
else if ($request == "savestudentinfo") {
	$info=json_decode($_POST['info']);
	$sql="UPDATE students set f_name='$info->fname', 
							   m_name='$info->mname', 
							   l_name='$info->lname', 
							   s_name='$info->sname', 
							   gender='$info->gender', 
							   dob='$info->dob', 
                               phone='$info->phone', 
							   state='$info->selected_state', 
							   recieve_type='$info->selected_reciveType', 
							   recieve_year='$info->selected_sem', 
							   resident='$info->selected_resident', 
							   note='$info->note', 
							   stu_code='$info->code', 
							   id_dep=$info->id_dep
							   where id_student=$info->id_student";
                               $result = $conn->query($sql);
    $obj = new stdClass();
    if($result){
        $obj->result = "success";
        if($info->selected_reciveType=='گواستنەوە'){
            $sql2="UPDATE transfer SET resons='$info->resons',id_stage=$info->id_stage,dep_name='$info->dep_name_transfer',univ_name='$info->univ_name_transfer' WHERE id_student=$info->id_student";
            // echo $sql2;
            $result2 = $conn->query($sql2);
            if($result2){

                $obj->result2 = "success";
            }
            else{
                $obj->result2="failed";
            }
            $obj->result = "success";
        }else if ($info->selected_reciveType=='دابەزین') {
            $sql3="UPDATE descents SET dep_name='$info->dep_name_descent',id_stage=$info->id_stage,univ_name='$info->univ_name_descent' WHERE id_student=$info->id_student;";
             // echo $sql3;
            $result3 = $conn->query($sql3);
            if($result3){
                $obj->result3 = "success";
            }
            else{
                $obj->result3="failed";
            }
        }
        else if ($info->selected_reciveType=='ئاسایی') {
            $sql4="UPDATE ordinary SET id_stage=$info->id_stage WHERE id_student=$info->id_student;";

            $result4 = $conn->query($sql4);
            if($result4){
                $obj->result4 = "success";
            }
            else{
                $obj->result4="failed";
            }
        }
        else if ($info->selected_reciveType=='گەڕاوە') {
            $sql5="UPDATE recursives SET id_stage=$info->id_stage WHERE id_student=$info->id_student;";

            $result5 = $conn->query($sql5);
            if($result5){
                $obj->result5 = "success";
            }
            else{
                $obj->result5="failed";
            }
        }
        else if($info->selected_reciveType=='پارالێڵ' || $info->selectedDescentType=='پارالێڵ' || $info->resons=='پارالێڵ'){
            $sql6="UPDATE payments SET id_sem = $info->id_sem_payment,id_stage= $info->id_stage,price =$info->price,payment=$info->payment,datee='$info->date_of_payment' WHERE id_student=$info->id_student;";
            // echo $sql6;
            $result6 = $conn->query($sql6);
            if($result6){
                $obj->result6 = "success";
            }
            else{
                $obj->result6="failed";
            }
        }
    else{
        $obj->result="failed";
    }
    
    echo json_encode($obj);
    
    }
							  
	
}



?>