<?php
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Methods: POST");
include("config.php");

$request = $_POST['request'];
if($request=="addSemster"){
	$semster=$_POST['semster'];
	$dean=$_POST['dean'];
	$reg_liable=$_POST['reg_liable'];
	$head_exam=$_POST['head_exam'];
	$supervisor_exam=$_POST['supervisor_exam'];

	$sql= "insert into semsters(year, dean, registration_liable, head_exam_comm, supervisor_exam_comm) VALUES ('$semster','$dean','$reg_liable','$head_exam','$supervisor_exam');";
// echo $sql;
	$result = $conn->query($sql);
	// $result = $conn->query($sql_add_dep);
    $obj  =new  stdClass();
    $id_semester=-1;
    if($result){
	   $obj->result = "success";
       $obj->id_semester=$conn->insert_id;
    }
    else{
	   $obj->result="failed";
    }
    echo json_encode($obj);
}
//++++++++++++++++++++++++++++++++
else if($request=="getSemster"){

 $sql="select * FROM semsters";
 $result = $conn->query($sql);
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id" => $row["id_sem"],
            "year"=>$row["year"],
            "dean"=>$row["dean"],
            "locked"=>$row["locked"],
            "reg_liable"=>$row["registration_liable"],
            "head_exam"=>$row["head_exam_comm"],
            "supervisor_exam"=>$row["supervisor_exam_comm"]
        );
    }
    echo json_encode($data);	
}
else if($request=="getSemsterInfo"){
    $id_sem = $_POST['id_sem'];
 $sql="select * FROM semsters where id_sem=$id_sem";
 $result = $conn->query($sql);
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id" => $row["id_sem"],
            "year"=>$row["year"],
            "dean"=>$row["dean"],
            "locked"=>$row["locked"],
            "reg_liable"=>$row["registration_liable"],
            "head_exam"=>$row["head_exam_comm"],
            "supervisor_exam"=>$row["supervisor_exam_comm"]
        );
    }
    echo json_encode($data);    
}
// _________________________________________________
else if($request=="deleteSem"){
    $id_sem=$_POST['id_sem'];
    $sql= "DELETE FROM semsters WHERE id_sem=$id_sem;";
    $result = $conn->query($sql);
    $obj  =new  stdClass();
    if($result){
       $obj->result = "success";
    }
    else{
       $obj->result="failed";
    }
    echo json_encode($obj);
}

?>