<?php 

$info=json_decode($_POST['data']); 
$students=json_decode($_POST['students']); 
// echo json_encode($info)
// $students[] =array("f_name"=>"kardo","m_name"=>"othman","s_name"=>"aziz","l_name"=>"hama");
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Student Form</title>
    <link rel="stylesheet" type="text/css" href="../asset/css/style.css">
    <script type="text/javascript" src="../asset/js/hotsnackbar.js"></script>
</head>
<style type="text/css">
	.text-right{
		text-align: right !important;
	}
	.text-center{
		text-align: center !important;
	}
	.text-left{
		text-align: left !important;
	}
	.full{
		width:100% !important;
	}
	.ffull{
		min-width:100% !important;
	}
	.capital{
		text-transform: capitalize;
	}
	.bold{
	font-weight: bold;
		}
		table tr {
			height:30px;
			border:1px solid grey !important;
		}
		table tr td{
			border:1px solid grey !important;
			text-align: center;
		}
		table th {
			text-transform: capitalize;
		}
		table {
			/*border:1px solid grey;*/
		}
		@media print
{     
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
}
	</style>
	<body onload=javascript:print();>
		<button style="height:40px;font-size:20px;float:right; margin:10px;" class="noprint" onclick="javascript:window.close()">Close</button>
		<button style="height:40px;font-size:20px;float:right;margin:10px; " class="noprint" onclick="javascript:window.print()">Print</button>
		<div style="font-size:25px; font-weight:bold;text-align:center;"> <?php echo $info->sem ?></div>
          <div style="font-size:18px; font-weight:bold;text-align:center;"><?php echo " به‌شی ".$info->dep ; ?></div>
    <div style="font-size:12px;text-align:center; line-height:14px;color:#0000ff;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;"><p style="margin: 0;font-size: 18px;line-height: 17px"><?php echo $info->sub ?></p></div>  
     <div style="font-size:12px;text-align:center; line-height:14px;color:#0000ff;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;"><p style="margin: 0;font-size: 18px;line-height: 17px"><?php 
     	if($info->stage) echo " قۆناغی ".$info->stage; ?></p></div>  
  
<table id="example" class="display compact" style="width:100%;margin-top:20px;">
			<thead>
				
			
		<tr style="height:30px;">
			<th style="width:30px;">
				<?php echo 'ئه‌نجام'; ?>
			</th>
		<th text-center style="margin-right:0px;width:50px;">
			<?php echo 'بریار'; ?>
		</th>
		<th text-center style="margin-right:0px;width:50px;">
			<?php echo 'خولی دووه‌م'; ?>
		</th>
		<th text-center style="margin-right:0px;width:50px;">
			<?php echo 'كۆتای ساڵ %'.$info->final ; ?>
		</th>
		<th text-center style="margin-right:0px;width:50px;">
			<?php echo 'پێش كۆتای ساڵ%'.$info->pre_final; ?>
		</th>
		<th text-center style="margin-right:0px;">
			<?php echo 'بابه‌ت'; ?>
		</th>
		<th text-center style="margin-right:0px;">
			<?php echo 'ناو'; ?>
		</th>

		<th text-center>
			<?php echo 'ژ'; ?>
		</th></tr>
	<!-- </ion-row> -->
	<tbody>
		<?php 
		$i=0;
		// echo "json_encode(students)";
		// echo json_encode($students);
		foreach ($students as $pat )
				{
					echo '<tr style="height:27px;">';
					echo '<td text-center class="tableitem" style="text-align:center;font-weight:bold;">
								'.$pat->result.'
							</td>';
					if($pat->help>0)
					{
						echo '		<td text-center class="tableitem" style="margin-right:0px;text-align:center;">
									'.$pat->help.'
								</td>';
					}
					else{

					echo '		<td text-center class="tableitem" style="margin-right:0px;text-align:center;">
									\+'.$pat->curve.'
								</td>';
					}		
					echo '<td text-center class="tableitem" style="margin-right:0px;text-align:center;">
								'.$pat->corse2.'
							</td>';
					
					echo '<td text-center class="tableitem">
						    '.$pat->final.'
						</td>';
					echo '<td text-center class="tableitem">
						    '.$pat->corse1.'
						</td>';
						echo '<td text-center class="tableitem">
						    '.$pat->sub_name.'
						</td>';
					echo '<td text-center class="tableitem" style="text-align:right;">
						    '.$pat->f_name.' '.$pat->m_name.' '.$pat->s_name.' '.$pat->l_name.'
						</td>';
					echo '<td text-center class="tableitem">
					 	    '.($i+1).'
					 	</td>';
					echo '</tr>';
					$i=$i+1;
					// echo $i;
				}
		?>
		
	<!-- </ion-row> -->
	</tbody>
	</table>
	
	</body>
	</html>
