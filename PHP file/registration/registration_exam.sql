-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2019 at 12:55 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registration&exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `id_action` int(11) NOT NULL,
  `action_type` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `action_typek` varchar(50) COLLATE utf8_bin NOT NULL,
  `reg_or_exam` varchar(2) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id_action`, `action_type`, `action_typek`, `reg_or_exam`) VALUES
(1, 'home', 'پەڕەی سەرەکی تۆمارگا', '0'),
(2, 'admission', 'وەرگرتنی خوێندکار', '0'),
(3, 'certificates', 'بڕوانامە', '0'),
(4, 'report', 'پشگیری_ڕاست و دروستی', '0'),
(5, 'home_exam', 'پەڕەی سەرەکی لیژنەی تاقیکردنەوە', '1'),
(6, 'department_degree', 'پەڕەی نمرەی خوێندکاران', '1'),
(7, 'add_sem', 'زیادکردنی وەرزی خوێندن', '2'),
(8, 'add_department', 'زیادکردنی بەش', '1'),
(9, 'add_subject', 'زیادکردنی وانە', '1'),
(10, 'add_teacher', 'زیادکردنی مامۆستا', '1'),
(11, 'show_emp_info', 'پیشاندانی زانیاری فەرمانبەر', '1'),
(12, 'add_emp', 'زیادکردنی فەرمانبەر', '1'),
(13, 'users', 'به‌كارهێنه‌ر', '0'),
(14, 'roles', 'پله‌', '2'),
(15, 'final_degree', 'نمره‌ی كۆتایی ساڵ', '1'),
(16, 'corse1_degree', 'نمره‌ی  كۆرسی یه‌كه‌م', '1'),
(17, 'corse2_degree', 'نمره‌ی خولی دووه‌م', '1'),
(18, 'transfer_student', 'ده‌رچونی خوێندكار', '1'),
(19, 'edit_enroll', 'وانه‌ی خوێندكار', '1'),
(20, 'curve', 'بریار', '1'),
(21, 'students', 'ناوی خوێندكاره‌كان', '1');

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `id_attach` int(11) NOT NULL,
  `file_name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `file_size` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `file_type` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `file_url` varchar(70) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id_attach`, `file_name`, `file_size`, `file_type`, `file_url`) VALUES
(1, '1564143457871.jpg', NULL, NULL, NULL),
(2, '1564143835464.jpg', NULL, NULL, NULL),
(3, '1564143881165.jpg', NULL, NULL, NULL),
(4, '1564143920879.jpg', NULL, NULL, NULL),
(5, '1564144256072.jpg', NULL, NULL, NULL),
(6, '1564155130298.jpg', NULL, NULL, NULL),
(7, '1564155769713.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `certification`
--

CREATE TABLE `certification` (
  `id_cert` int(11) NOT NULL,
  `num_of_cert` int(11) DEFAULT NULL,
  `datee` date DEFAULT NULL,
  `id_student` int(11) DEFAULT NULL,
  `file_name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `code` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `certification`
--

INSERT INTO `certification` (`id_cert`, `num_of_cert`, `datee`, `id_student`, `file_name`, `code`) VALUES
(2, 5, '2019-05-09', 7, '', 'a233'),
(3, 2, '2019-05-09', 7, '1557353519182.pdf', 'a231'),
(5, 3, '2019-05-09', 7, '1557400090562.pdf', 's211'),
(6, 4, '2019-05-13', 7, '', 'g4325'),
(7, 4, '2019-05-13', 7, '', 'g4325'),
(8, 3, '2019-05-13', 7, '', 'g4325'),
(9, 3, '2019-05-13', 7, '1557751797743.pdf', 'g4325'),
(10, 4, '2019-05-19', 7, '1558295946831.pdf', 'a4444'),
(11, 3, '2019-07-01', 16, '', '132');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id_dep` int(11) NOT NULL,
  `dep_namek` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `dep_namee` varchar(30) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id_dep`, `dep_namek`, `dep_namee`) VALUES
(1, 'tomarga', 'registration'),
(2, 'lizhnay emti7ani', 'exam committee'),
(3, 'كۆمپیوته‌ر', 'computer'),
(4, 'ماتماتیك', 'mathmatic'),
(5, 'جیۆلۆجی', 'geology'),
(6, 'بایۆلۆجی', 'biology'),
(7, 'كیمیا', 'chemistry'),
(8, 'فیزیا', 'physic');

-- --------------------------------------------------------

--
-- Table structure for table `dep_head`
--

CREATE TABLE `dep_head` (
  `id_sem` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `head_name` varchar(40) COLLATE utf8_bin NOT NULL,
  `assistance` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `dep_head`
--

INSERT INTO `dep_head` (`id_sem`, `id_dep`, `head_name`, `assistance`) VALUES
(1, 3, 'مسته‌فا', 'میهران'),
(1, 6, 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `descents`
--

CREATE TABLE `descents` (
  `id_student` int(11) NOT NULL,
  `id_stage` int(11) NOT NULL,
  `dep_name` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `univ_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `descent_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `descent_state` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `descents`
--

INSERT INTO `descents` (`id_student`, `id_stage`, `dep_name`, `univ_name`, `descent_type`, `descent_state`) VALUES
(3, 3, 'ØµÙ„Ø§Ø­Û•Ø¯ÛŒÙ†', 'Ø¨Ø§ÛŒÛ†Ù„Û†Ø¬ÛŒ', '', ''),
(4, 4, 'ØµÙ„Ø§Ø­Û•Ø¯ÛŒÙ†', 'Ø¨Ø§ÛŒÛ†Ù„Û†Ø¬ÛŒ', '', ''),
(26, 4, 'چەرمۆ', 'کۆمپیوتەر', '', ''),
(28, 2, 'چەرمۆ', 'کۆمپیوتەر', '', ''),
(34, 1, 'چەرمۆ', 'کۆمپیوتەر', '', ''),
(45, 1, 'halabja', 'computer', '', ''),
(46, 1, 'computer', 'charmo', '', ''),
(47, 1, 'computer', 'halabja', 'ئاسایی', ''),
(48, 1, 'computer', 'halabja', 'ئاسایی', ''),
(49, 1, 'chemistry', 'halabja', 'پارالێڵ', ''),
(50, 1, 'chemistry', 'charmo', 'پارالێڵ', 'تازەوەرگیراو'),
(60, 1, 'math', 'chamro', 'پارالێڵ', 'تازەوەرگیراو'),
(63, 1, 'qq', 'qq', 'پارالێڵ', 'پێشتروەرگیراو'),
(67, 1, 'ww', 'ww', 'پارالێڵ', 'پێشتروەرگیراو');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id_person` int(11) NOT NULL,
  `id_dep` int(11) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  `emp_type` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(20) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id_person`, `id_dep`, `id_role`, `emp_type`, `username`, `password`) VALUES
(1, 1, 1, 'registration', 'bahar', '1'),
(2, 2, 2, 'exam committee', 'hardi', '1'),
(3, 3, 3, 'كۆمپیوته‌ر', 'mihran', '1'),
(4, 4, 3, 'ئەندامی لیژنەی بەش', 'hilal', '1'),
(15, 1, 4, 'فەرمانبەری تۆمارگا', 'zana', '1'),
(16, 3, 3, 'كۆمپیوته‌ر', 'jamal', '1');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id_student` int(11) NOT NULL,
  `id_sub` int(11) NOT NULL,
  `id_sem` int(11) NOT NULL,
  `corse1` int(3) DEFAULT '0',
  `final` int(3) DEFAULT '0',
  `corse2` int(3) DEFAULT '0',
  `state` varchar(20) COLLATE utf8_bin DEFAULT 'ئاسایی',
  `level` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `crossing` int(3) DEFAULT '0',
  `crossingfinal` int(3) DEFAULT '0',
  `crossing2` int(3) DEFAULT '0',
  `curve` int(3) DEFAULT '0',
  `offer_type` varchar(200) COLLATE utf8_bin DEFAULT 'normal',
  `crossing_id_sem` int(3) DEFAULT NULL,
  `note` varchar(1000) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id_student`, `id_sub`, `id_sem`, `corse1`, `final`, `corse2`, `state`, `level`, `crossing`, `crossingfinal`, `crossing2`, `curve`, `offer_type`, `crossing_id_sem`, `note`) VALUES
(2, 19, 1, 22, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(2, 20, 1, 33, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(2, 21, 1, 44, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(2, 22, 1, 55, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(2, 23, 1, 66, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(3, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(3, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(3, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(3, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(3, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(3, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 1, 1, 40, 43, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 2, 1, 35, 38, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 3, 1, 20, 40, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 4, 1, 47, 48, 0, 'دەرچوو', 'باڵا', 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 5, 1, 30, 10, 10, 'کەوتوو', 'کەوتوو', 30, 12, 25, 3, 'crossing', 2, NULL),
(7, 6, 1, 20, 20, 20, 'کەوتوو', 'کەوتوو', 25, 23, 0, 2, 'crossing', 2, NULL),
(7, 7, 2, 34, 9, 20, 'کەوتوو', 'کەوتوو', 0, 0, 0, 0, 'normal', NULL, ''),
(7, 8, 2, 29, 30, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, 'note'),
(7, 9, 2, 26, 40, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 10, 2, 37, 40, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 11, 2, 32, 31, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(7, 12, 2, 44, 35, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 7, 2, 40, 8, 0, 'کەوتوو', 'کەوتوو', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 8, 2, 30, 40, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, ''),
(8, 9, 2, 30, 30, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 10, 2, 40, 30, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 11, 2, 40, 9, 0, 'کەوتوو', 'کەوتوو', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 12, 2, 40, 7, 0, 'کەوتوو', 'کەوتوو', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 13, 3, 34, 23, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 14, 3, 45, 45, 0, 'دەرچوو', 'باڵا', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 15, 3, 40, 33, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 16, 3, 34, 43, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 17, 3, 34, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 18, 3, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 19, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 20, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 21, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 22, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(8, 23, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 1, 1, 40, 45, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 2, 1, 30, 25, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 3, 1, 33, 40, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 4, 1, 42, 24, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 5, 1, 45, 48, 0, 'دەرچوو', 'باڵا', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 6, 1, 38, 33, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 7, 2, 45, 44, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 8, 2, 44, 42, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 9, 2, 37, 40, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 10, 2, 45, 44, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 11, 2, 40, 40, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 12, 2, 39, 43, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 13, 3, 43, 46, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 14, 3, 45, 41, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 15, 3, 43, 36, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 16, 3, 44, 39, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 17, 3, 41, 38, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 18, 3, 41, 38, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 19, 5, 42, 42, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 20, 5, 42, 46, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 21, 5, 41, 41, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 22, 5, 43, 39, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(14, 23, 5, 45, 45, 0, 'دەرچوو', 'باڵا', 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 1, 1, 30, 26, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 2, 1, 40, 22, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 3, 1, 33, 42, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 4, 1, 49, 23, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 5, 1, 35, 34, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 6, 1, 41, 34, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 7, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 8, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 9, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 10, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 11, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(15, 12, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 1, 1, 34, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 2, 1, 44, 22, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 3, 1, 34, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 4, 1, 32, 44, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 5, 1, 48, 22, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 6, 1, 26, 34, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 7, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 8, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 9, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 10, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 11, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(16, 12, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 1, 1, 32, 34, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 2, 1, 34, 23, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 3, 1, 33, 43, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 4, 1, 44, 20, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 5, 1, 23, 35, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 6, 1, 45, 23, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 7, 2, 25, 35, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 8, 2, 34, 34, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 9, 2, 43, 44, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 10, 2, 44, 32, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 11, 2, 33, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 12, 2, 29, 39, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 13, 3, 34, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 14, 3, 43, 32, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 15, 3, 44, 34, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 16, 3, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 17, 3, 37, 34, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 18, 3, 39, 32, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 19, 5, 32, 340, 0, '', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 20, 5, 34, 34, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 21, 5, 34, 34, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 22, 5, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(17, 23, 5, 45, 23, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 1, 1, 43, 34, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 2, 1, 23, 44, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 3, 1, 33, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 4, 1, 44, 32, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 5, 1, 33, 23, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 6, 1, 23, 43, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 7, 2, 34, 23, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 8, 2, 33, 43, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 9, 2, 44, 33, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 10, 2, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 11, 2, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(18, 12, 2, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(19, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(19, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(19, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(19, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(19, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(19, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(20, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(20, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(20, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(20, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(20, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(20, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(21, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(21, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(21, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(21, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(21, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(21, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(22, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(22, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(22, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(22, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(22, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(22, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(23, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(23, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(23, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(23, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(23, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(23, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(24, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(24, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(24, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(24, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(24, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(24, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(25, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(25, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(25, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(25, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(25, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(25, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 1, 1, 30, 34, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 2, 1, 43, 33, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 3, 1, 45, 23, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 4, 1, 33, 23, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 5, 1, 33, 26, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 6, 1, 34, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 7, 2, 29, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 8, 2, 32, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 9, 2, 32, 22, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 10, 2, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 11, 2, 44, 44, 0, 'دەرچوو', 'زۆر باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 12, 2, 32, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 13, 3, 32, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 14, 3, 33, 22, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 15, 3, 43, 33, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 16, 3, 32, 45, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 17, 3, 33, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 18, 3, 29, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 19, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 20, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 21, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 22, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(26, 23, 5, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 1, 1, 33, 22, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 2, 1, 34, 43, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 3, 1, 43, 33, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 4, 1, 34, 43, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 5, 1, 33, 44, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 6, 1, 41, 32, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 7, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 8, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 9, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 10, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 11, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(27, 12, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 1, 1, 34, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 2, 1, 43, 23, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 3, 1, 43, 23, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 4, 1, 33, 23, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 5, 1, 33, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 6, 1, 25, 34, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 7, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 8, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 9, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 10, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 11, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(28, 12, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(29, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(29, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(29, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(29, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(29, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(29, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(30, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(30, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(30, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(30, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(30, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(30, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(31, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(31, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(31, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(31, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(31, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(31, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(32, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(32, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(32, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(32, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(32, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(32, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(33, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(33, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(33, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(33, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(33, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(33, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(34, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(34, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(34, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(34, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(34, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(34, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(35, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(35, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(35, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(35, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(35, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(35, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(36, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(36, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(36, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(36, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(36, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(36, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(37, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(37, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(37, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(37, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(37, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(37, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(38, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(38, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(38, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(38, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(38, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(38, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(39, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(39, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(39, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(39, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(39, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(39, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(40, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(40, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(40, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(40, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(40, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(40, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(41, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(41, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(41, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(41, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(41, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(41, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(42, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(42, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(42, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(42, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(42, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(42, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(43, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(43, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(43, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(43, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(43, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(43, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(44, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(44, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(44, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(44, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(44, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(44, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(45, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(45, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(45, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(45, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(45, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(45, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(46, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(46, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(46, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(46, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(46, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(46, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(47, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(47, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(47, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(47, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(47, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(47, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(48, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(48, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(48, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(48, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(48, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(48, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(49, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(49, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(49, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(49, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(49, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(49, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(50, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(50, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(50, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(50, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(50, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(50, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(51, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(51, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(51, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(51, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(51, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(51, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(52, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(52, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(52, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(52, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(52, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(52, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(53, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(53, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(53, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(53, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(53, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(53, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(54, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(54, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(54, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(54, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(54, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(54, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(55, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(55, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(55, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(55, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(55, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(55, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(56, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(56, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(56, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(56, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(56, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(56, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(57, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(57, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(57, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(57, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(57, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(57, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(58, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(58, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(58, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(58, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(58, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(58, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(59, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(59, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(59, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(59, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(59, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(59, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(60, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(60, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(60, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(60, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(60, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(60, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(61, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(61, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(61, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(61, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(61, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(61, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(62, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(62, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(62, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(62, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(62, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(62, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(63, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(63, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(63, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(63, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(63, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(63, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(64, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(64, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(64, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(64, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(64, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(64, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(65, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(65, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(65, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(65, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(65, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(65, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(66, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(66, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(66, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(66, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(66, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(66, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(67, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(67, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(67, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(67, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(67, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(67, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(68, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(68, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(68, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(68, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(68, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(68, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(69, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(69, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(69, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(69, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(69, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(69, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(70, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(70, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(70, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(70, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(70, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(70, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(71, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(71, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(71, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(71, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(71, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(71, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(72, 1, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(72, 2, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(72, 3, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(72, 4, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(72, 5, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(72, 6, 1, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 1, 1, 34, 33, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 2, 1, 33, 22, 0, 'دەرچوو', 'پەسەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 3, 1, 33, 44, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 4, 1, 33, 44, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 5, 1, 33, 44, 0, 'دەرچوو', 'باش', 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 6, 1, 33, 32, 0, 'دەرچوو', 'ناوەند', 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 7, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 8, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 9, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 10, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 11, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL),
(73, 12, 2, 0, 0, 0, 'ئاسایی', NULL, 0, 0, 0, 0, 'normal', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ordinary`
--

CREATE TABLE `ordinary` (
  `id_student` int(11) NOT NULL,
  `id_stage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ordinary`
--

INSERT INTO `ordinary` (`id_student`, `id_stage`) VALUES
(2, 4),
(7, 2),
(8, 4),
(9, 1),
(13, 1),
(14, 3),
(15, 2),
(16, 2),
(17, 3),
(18, 2),
(43, 1),
(44, 1),
(64, 1);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id_payment` int(11) NOT NULL,
  `id_student` int(11) DEFAULT NULL,
  `id_sem` int(11) DEFAULT NULL,
  `id_attach` int(11) DEFAULT NULL,
  `id_stage` int(11) DEFAULT NULL,
  `price` int(15) DEFAULT NULL,
  `payment` int(15) DEFAULT NULL,
  `datee` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id_payment`, `id_student`, `id_sem`, `id_attach`, `id_stage`, `price`, `payment`, `datee`) VALUES
(1, 41, 1, 1, 1, 2000500, 1000500, '2019-07-31'),
(2, 42, 1, 1, 1, 2000000, 1000000, '2019-08-02'),
(3, 53, 1, 1, 1, 2000000, 1000000, '2019-08-02'),
(4, 55, 2, 1, 1, 2000000, 1000000, '2019-08-03'),
(5, 60, 3, 1, 1, 2000000, 1000000, '2019-08-03'),
(6, 63, 1, 1, 1, 2000000, 1000000, '2019-08-05'),
(8, 67, 2, 1, 1, 2000000, 10000, '2019-08-05'),
(9, 72, 3, 1, 1, 20000, 1000, '2019-08-05'),
(10, 73, 1, 1, 1, 2000000, 1000000, '2019-08-16');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id_role` int(11) NOT NULL,
  `id_action` int(11) NOT NULL,
  `insertp` varchar(5) COLLATE utf8_bin DEFAULT 'true',
  `updatep` varchar(5) COLLATE utf8_bin DEFAULT 'true',
  `deletep` varchar(5) COLLATE utf8_bin DEFAULT 'true',
  `viewp` varchar(5) COLLATE utf8_bin DEFAULT 'true'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id_role`, `id_action`, `insertp`, `updatep`, `deletep`, `viewp`) VALUES
(1, 1, '1', '1', '1', '1'),
(1, 2, '1', '1', '1', '1'),
(1, 3, '1', '1', '1', '1'),
(1, 4, '1', '1', '1', '1'),
(1, 5, '0', '0', '0', '0'),
(1, 6, '0', '0', '0', '0'),
(1, 7, '1', '1', '1', '1'),
(1, 8, '1', '1', '1', '1'),
(1, 9, '1', '1', '1', '1'),
(1, 10, '1', '1', '1', '1'),
(1, 11, '1', '1', '1', '1'),
(1, 12, '1', '1', '1', '1'),
(1, 13, '1', '1', '1', '1'),
(1, 14, '1', '1', '1', '1'),
(1, 15, '1', '1', '1', '1'),
(1, 16, '1', '1', '1', '1'),
(1, 17, '1', '1', '1', '1'),
(1, 18, '1', '1', '1', '1'),
(1, 19, '1', '1', '1', '1'),
(1, 20, '1', '1', '1', '1'),
(1, 21, '1', '1', '1', '1'),
(2, 1, '0', '0', '0', '0'),
(2, 2, '0', '0', '0', '0'),
(2, 3, '0', '0', '0', '0'),
(2, 4, '0', '0', '0', '0'),
(2, 5, '1', '1', '1', '1'),
(2, 6, '1', '1', '1', '1'),
(2, 7, '1', '1', '1', '1'),
(2, 8, '1', '1', '1', '1'),
(2, 9, '1', '1', '1', '1'),
(2, 10, '1', '1', '1', '1'),
(2, 11, '1', '1', '1', '1'),
(2, 12, '1', '1', '1', '1'),
(2, 13, '1', '1', '1', '1'),
(2, 14, '1', '1', '1', '1'),
(2, 15, '1', '1', '1', '1'),
(2, 16, '1', '1', '1', '1'),
(2, 17, '1', '1', '1', '1'),
(2, 18, '1', '1', '1', '1'),
(2, 19, '1', '1', '1', '1'),
(2, 20, '1', '1', '1', '1'),
(2, 21, '1', '1', '1', '1'),
(3, 1, '0', '0', '0', '0'),
(3, 2, '0', '0', '0', '0'),
(3, 3, '0', '0', '0', '0'),
(3, 4, '0', '0', '0', '0'),
(3, 5, '1', '1', '1', '1'),
(3, 6, '1', '1', '1', '1'),
(3, 7, '0', '0', '0', '0'),
(3, 8, '0', '0', '0', '0'),
(3, 9, '1', '1', '1', '1'),
(3, 10, '1', '1', '1', '1'),
(3, 11, '0', '0', '0', '0'),
(3, 12, '0', '0', '0', '0'),
(3, 13, 'true', 'true', 'true', 'true'),
(3, 14, 'true', 'true', 'true', 'true'),
(3, 15, '1', '1', '1', '1'),
(3, 16, '1', '1', '1', '1'),
(3, 17, '1', '1', '1', '1'),
(3, 18, '1', '1', '1', '1'),
(3, 19, '1', '1', '1', '1'),
(3, 20, '1', '1', '1', '1'),
(3, 21, 'false', 'false', 'false', 'false'),
(4, 1, '0', '0', '0', '0'),
(4, 2, '1', '1', '1', '1'),
(4, 3, '0', '0', '0', '0'),
(4, 4, '0', '0', '0', '0'),
(4, 5, '0', '0', '0', '0'),
(4, 6, '0', '0', '0', '0'),
(4, 7, '0', '0', '0', '0'),
(4, 8, '0', '0', '0', '0'),
(4, 9, '0', '0', '0', '0'),
(4, 10, '0', '0', '0', '0'),
(4, 11, '0', '0', '0', '0'),
(4, 12, '0', '0', '0', '0'),
(4, 13, 'true', 'true', 'true', 'true'),
(4, 14, 'true', 'true', 'true', 'true'),
(4, 15, 'true', 'true', 'true', 'true'),
(4, 16, 'true', 'true', 'true', 'true'),
(4, 17, 'true', 'true', 'true', 'true'),
(4, 18, 'true', 'true', 'true', 'true'),
(4, 19, 'true', 'true', 'true', 'true'),
(4, 20, 'false', 'false', 'false', 'false'),
(4, 21, 'false', 'false', 'false', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id_person` int(11) NOT NULL,
  `f_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `m_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `l_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `gender` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(40) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id_person`, `f_name`, `m_name`, `l_name`, `gender`, `phone`, `email`, `address`) VALUES
(1, 'بەهار', 'اللەکرم', 'تۆفیق', 'female', '07701234455', 'bahar.tofiq@univsul.edu.iq', 'سلێمانی'),
(2, 'هەردی', 'محمد', 'محمد', 'male', '092763', 'hardi@univsul', 'سلێمانی'),
(3, 'میهران', 'عبدالرحمن', 'محمد', 'female', '٠٧٧٠٢٧٣٢', 'mihran.abdalrahman@univsul.edu.iq', 'سلێمانی'),
(4, 'هیلال', 'محمد', 'محمد', 'male', '٠٧٧٩٢٣٦', 'hilal.mhamad@univsul.edu.iq', 'سلێمانی'),
(6, 'میهران', 'عبدالرحمن', 'محمد', 'female', '٩٢٠٤٧٠٣٩٧', 'mihran@univsul.edu.iq', 'سلێمانی'),
(7, 'چواز', 'عباس', 'محمد', 'male', '٩٣٢٤٧٧', 'chwaz@univsul.edu.iq', 'سلێمانی'),
(8, 'زانا', 'محمد', 'محمد', 'male', '٣٨٤٧٩٣٢', 'zana@univsul.edu.iq', 'سلێمانی'),
(9, 'زانا', 'محمد', 'محمد', 'male', '٠٢٨٢٠', 'zana@univsul', 'سلێمانی'),
(10, 'زانا', 'محمد', 'محمد', 'male', '٢٧١٢٦٣٢', 'zana@univsul', 'سلێمانی'),
(15, 'زانا', 'محمد', 'محمد', 'male', '٣٩٧٤٢٨٩٧', 'zana.mhamad@univsul.edu', 'سلێمانی'),
(16, 'جمال', 'علی', 'حسێن', 'male', '٠٧٧٠٠٧٦٩٠٦٠', 'jamal.hussen@univsul.edu.iq', 'سلێمانی');

-- --------------------------------------------------------

--
-- Table structure for table `recursives`
--

CREATE TABLE `recursives` (
  `id_student` int(11) NOT NULL,
  `id_stage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `recursives`
--

INSERT INTO `recursives` (`id_student`, `id_stage`) VALUES
(19, 1),
(29, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id_rep` int(11) NOT NULL,
  `code_report` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `direct` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `date_sys` date DEFAULT NULL,
  `title` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `body` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `follow` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`id_rep`, `code_report`, `direct`, `date_sys`, `title`, `body`, `follow`) VALUES
(1, '', '', '0000-00-00', '', '', ''),
(2, '', '', '0000-00-00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
--

CREATE TABLE `requirements` (
  `id_req` int(11) NOT NULL,
  `requirement` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `requirements`
--

INSERT INTO `requirements` (`id_req`, `requirement`) VALUES
(1, 'certificate'),
(2, 'ID'),
(3, 'jnsya'),
(4, 'carty_znyary'),
(5, 'zankoline_form'),
(6, 'national_card'),
(7, 'balennama'),
(8, 'kafalat'),
(9, 'payment');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(70) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_role`, `role_name`) VALUES
(1, 'لێپرسراوی تۆمارگا'),
(2, 'سەرپەرشتیاری لیژنەی تاقیکردنەوە'),
(3, 'ئەندامی لیژنەی بەش'),
(4, 'فەرمانبەری تۆمارگا');

--
-- Triggers `roles`
--
DELIMITER $$
CREATE TRIGGER `add_permission_for_role` AFTER INSERT ON `roles` FOR EACH ROW insert into permission (id_role,id_action,insertp,updatep,deletep,viewp )
select NEW.id_role,id_action,0,0,0,0
from actions
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `semsters`
--

CREATE TABLE `semsters` (
  `id_sem` int(11) NOT NULL,
  `year` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `dean` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `registration_liable` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `head_exam_comm` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `supervisor_exam_comm` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `locked` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `semsters`
--

INSERT INTO `semsters` (`id_sem`, `year`, `dean`, `registration_liable`, `head_exam_comm`, `supervisor_exam_comm`, `locked`) VALUES
(1, '2018-2019', 'سۆران', 'به‌هار', 'هه‌ردی', 'محمد', 0),
(2, '2019-2020', 'سۆران', 'به‌هار', 'محمد', 'هه‌ردی', 0),
(3, '2020-2021', 'soran', 'bahar', 'tara', 'hardy', 0),
(5, '2021-2022', 'سۆران', 'بەهار', 'دیاری', 'هەردی', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stage`
--

CREATE TABLE `stage` (
  `id_stage` int(11) NOT NULL,
  `stage_num` int(2) DEFAULT NULL,
  `stage_txt` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `stage_txtEn` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `stage`
--

INSERT INTO `stage` (`id_stage`, `stage_num`, `stage_txt`, `stage_txtEn`) VALUES
(1, 1, 'یه‌كه‌م', 'First'),
(2, 2, 'دووه‌م', 'Second'),
(3, 3, 'سێیه‌م', 'Third'),
(4, 4, 'چوارەم', 'Fourth');

-- --------------------------------------------------------

--
-- Table structure for table `stg_sem_sub`
--

CREATE TABLE `stg_sem_sub` (
  `id_stage` int(11) NOT NULL,
  `id_sem` int(11) NOT NULL,
  `id_sub` int(11) NOT NULL,
  `offer_type` varchar(255) COLLATE utf8_bin DEFAULT 'normal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `stg_sem_sub`
--

INSERT INTO `stg_sem_sub` (`id_stage`, `id_sem`, `id_sub`, `offer_type`) VALUES
(1, 1, 1, 'normal'),
(1, 1, 2, 'normal'),
(1, 1, 3, 'normal'),
(1, 1, 4, 'normal'),
(1, 1, 5, 'normal'),
(1, 1, 6, 'normal'),
(1, 3, 1, 'normal'),
(1, 3, 2, 'normal'),
(1, 3, 3, 'normal'),
(1, 3, 4, 'normal'),
(1, 3, 5, 'normal'),
(1, 3, 6, 'normal'),
(2, 1, 7, 'normal'),
(2, 1, 8, 'normal'),
(2, 1, 9, 'normal'),
(2, 1, 10, 'normal'),
(2, 1, 11, 'normal'),
(2, 2, 7, 'normal'),
(2, 2, 8, 'normal'),
(2, 2, 9, 'normal'),
(2, 2, 10, 'normal'),
(2, 2, 11, 'normal'),
(2, 2, 12, 'normal'),
(2, 3, 7, 'normal'),
(2, 3, 8, 'normal'),
(2, 3, 9, 'normal'),
(2, 3, 10, 'normal'),
(2, 3, 11, 'normal'),
(2, 3, 12, 'normal'),
(3, 1, 12, 'normal'),
(3, 1, 13, 'normal'),
(3, 1, 14, 'normal'),
(3, 1, 15, 'normal'),
(3, 1, 16, 'normal'),
(3, 1, 17, 'normal'),
(3, 1, 18, 'normal'),
(3, 3, 13, 'normal'),
(3, 3, 14, 'normal'),
(3, 3, 15, 'normal'),
(3, 3, 16, 'normal'),
(3, 3, 17, 'normal'),
(3, 3, 18, 'normal'),
(4, 5, 19, 'normal'),
(4, 5, 20, 'normal'),
(4, 5, 21, 'normal'),
(4, 5, 22, 'normal'),
(4, 5, 23, 'normal');

--
-- Triggers `stg_sem_sub`
--
DELIMITER $$
CREATE TRIGGER `student_grade` AFTER INSERT ON `stg_sem_sub` FOR EACH ROW insert into grade (id_sub,id_sem,id_student )
select NEW.id_sub,NEW.id_sem,s.id_student
from students s,stage st, descents d
where s.id_student=d.id_student AND st.id_stage=d.id_stage and st.id_stage=NEW.id_stage and s.state='bardawam'
UNION
select NEW.id_sub,NEW.id_sem,s.id_student
from students s,stage st,ordinary o
where s.id_student=o.id_student AND st.id_stage=o.id_stage and st.id_stage=NEW.id_stage and s.state='bardawam'
UNION
select NEW.id_sub,NEW.id_sem,s.id_student
from students s,stage st, recursives r
where s.id_student=r.id_student AND st.id_stage=r.id_stage and st.id_stage=NEW.id_stage and s.state='bardawam'
UNION
select NEW.id_sub,NEW.id_sem,s.id_student
from students s,stage st,transfer tr
where s.id_student=tr.id_student AND st.id_stage=tr.id_stage and st.id_stage=NEW.id_stage and s.state='bardawam'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id_student` int(11) NOT NULL,
  `stu_code` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `id_dep` int(11) DEFAULT NULL,
  `f_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `m_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `s_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `l_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `fname` varchar(20) COLLATE utf8_bin NOT NULL,
  `mname` varchar(20) COLLATE utf8_bin NOT NULL,
  `sname` varchar(20) COLLATE utf8_bin NOT NULL,
  `lname` varchar(20) COLLATE utf8_bin NOT NULL,
  `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(20) COLLATE utf8_bin DEFAULT 'بەردەوام',
  `recieve_year` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `recieve_type` varchar(20) COLLATE utf8_bin DEFAULT 'ئاسایی',
  `resident` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `gender` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `note` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `dob` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id_student`, `stu_code`, `id_dep`, `f_name`, `m_name`, `s_name`, `l_name`, `fname`, `mname`, `sname`, `lname`, `phone`, `state`, `recieve_year`, `recieve_type`, `resident`, `gender`, `note`, `dob`) VALUES
(7, 'kardo2019', 3, 'كاردۆ', 'عثمان', 'عزیز', 'حمه‌', '', '', '', '', '459034', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'نێر', '', '1989-07-08'),
(8, 'shanyan2019', 3, 'shanyan', 'kamaran', 'j', 'j', '', '', '', '', '2309', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'مێ', '', NULL),
(9, 'nahro2019', 5, 'nahro ', 'osman', 'aziz', 'hama', '', '', '', '', '355', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'نێر', '', '1989-09-15'),
(13, '1', 3, 'k', 'o', 'a', 'h', '', '', '', '', '7412', 'bardawam', '2020-2021', 'ئاسایی', 'ناوشار', 'نێر', '', '1989-08-07'),
(14, 'co1', 3, 'سۆما', 'ابوبکر', 'ابراهیم', 'عارف', '', '', '', '', '٠٧٥١٥٢٥٠٣١٣', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'مێ', '', NULL),
(15, 'co٢', 3, 'شانیان', 'کامەران', 'لتیف', 'محمد', '', '', '', '', '٠٧٧٠٧٤٥٦٤٣', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'مێ', '', NULL),
(16, 'co3', 3, 'محمد', 'حسن', 'محمود', 'مردان', '', '', '', '', '٠٧٧٠٧٤٥٦٦٧', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'نێر', '', NULL),
(17, 'co4', 3, 'کەنار', 'حیدەر', 'احمد', 'احمد', 'Kanar', 'Haidar', 'Muhammad', 'Ahmad', '٠٧٦٢٥٢٢', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'مێ', '', '1998-01-16'),
(18, 'CO5', 3, 'هانا', 'ڕزگار', 'محمد ', 'احمد', '', '', '', '', '٠٧٧٠١٩٧٢٩٧', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'مێ', 'تێبینی', '1997-12-16'),
(26, 'co11', 3, 'چیناز', 'کاوە', 'محمد', 'احمد', '', '', '', '', '٠٢٨٢٧٧٦', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'مێ', '', '1997-05-11'),
(28, 'co13', 3, 'نیشتیمان', 'کاوە', 'محمد', 'احمد', '', '', '', '', '٠٧٧٠٣٧٣٦٢', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'مێ', '', '1997-08-10'),
(29, 'Co15', 3, 'سروشت', 'محمد', 'محمد', 'محمد', '', '', '', '', '٠٨٧٢٦٢٨', 'bardawam', '2018-2019', 'گەڕاوە', 'دەرەوەی شار', 'مێ', 'تێبینی', '1998-07-12'),
(31, 'co17', 3, 'محمد', 'کامەران', 'محمد', 'محمد', '', '', '', '', '٠٥٤٥٦', 'bardawam', '2018-2019', 'گواستنەوە', 'ناوشار', 'نێر', '', '1997-11-26'),
(32, 'co18', 3, 'نگین', 'ستار', 'محمد', 'محمد', '', '', '', '', '٠٩٨٦٤٣٦', 'bardawam', '2018-2019', 'گواستنەوە', 'ناوشار', 'مێ', '', '1996-01-26'),
(33, 'co19', 3, 'ئیمان', 'نەوزاد', 'محمد', 'محم', '', '', '', '', '٠٩٨٦٥', 'bardawam', '2018-2019', 'گواستنەوە', 'ناوشار', 'مێ', '', '1998-07-26'),
(34, 'co20', 3, 'بەسۆز', 'نجم الدین', 'احمد', 'محمد', '', '', '', '', '٠٩٨٦٥٣', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'مێ', '', '1995-07-26'),
(41, 'co23', 3, 'یەحیا', 'حسێن', 'محمد', 'محمد', '', '', '', '', '٨٤٧٣', 'bardawam', '2018-2019', 'پارالێڵ', 'ناوشار', 'نێر', '', '1995-07-31'),
(42, 'CO42', 3, 'هەڵگورد', 'احمد', 'محمد', 'محمد', '', '', '', '', '٥٦٧٨', 'bardawam', '2018-2019', 'پارالێڵ', 'ناوشار', 'نێر', 'تێبینی', '1996-08-02'),
(43, 'co43', 3, 'aa', 'aa', 'aa', 'aa', '', '', '', '', '345678', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'نێر', '', '2008-08-02'),
(44, 'co44', 3, 'bb', 'bb', 'bb', 'bb', '', '', '', '', '345678', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'نێر', '', '2005-08-02'),
(45, 'co54', 3, 'cc', 'cc', 'cc', 'cc', '', '', '', '', '567890', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '1998-08-02'),
(46, 'co46', 3, 'dd', 'dd', 'dd', 'dd', '', '', '', '', '345678', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '1997-08-02'),
(47, 'co47', 3, 'ee', 'ee', 'ee', 'ee', '', '', '', '', '2345678', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '1982-08-02'),
(48, 'co48', 3, 'ff', 'ff', 'ff', 'ff', '', '', '', '', '3456789', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '2010-08-02'),
(49, 'co49', 3, 'gg', 'gg', 'gg', 'gg', '', '', '', '', '247', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '1990-08-02'),
(50, 'co50', 3, 'hh', 'hh', 'hh', 'hh', '', '', '', '', '34567890', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '1974-08-02'),
(51, 'co51', 3, 'ii', 'II', 'II', 'II', '', '', '', '', '1234', 'bardawam', '2018-2019', 'گواستنەوە', 'ناوشار', 'نێر', '', '1997-08-02'),
(52, 'co52', 3, 'jj', 'jj', 'jj', 'jj', '', '', '', '', '123456', 'bardawam', '2018-2019', 'گواستنەوە', 'ناوشار', 'مێ', '', '1989-08-02'),
(53, 'co53', 3, 'kk', 'kk', 'kk', 'kk', '', '', '', '', '1234567', 'bardawam', '2018-2019', 'پارالێڵ', 'ناوشار', 'مێ', '', '1984-08-02'),
(55, 'co54', 3, 'mm', 'mm', 'mm', 'mm', '', '', '', '', '123456798', 'bardawam', '2018-2019', 'پارالێڵ', 'ناوشار', 'نێر', '', '1997-08-03'),
(60, 'co92', 3, 'nn', 'nn', 'nn', 'n', '', '', '', '', '9765', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '1979-08-03'),
(61, 'co87', 3, 'oo', 'oo', 'oo', 'oo', '', '', '', '', '88765', 'bardawam', '2018-2019', 'گواستنەوە', 'ناوشار', 'مێ', '', '1999-08-04'),
(62, 'cpo97', 3, 'pp', 'pp', 'pp', 'pp', '', '', '', '', '1242', 'bardawam', '2018-2019', 'گواستنەوە', 'ناوشار', 'مێ', '', '1991-08-04'),
(63, 'sh23', 3, 'qq', 'qq', 'qq', 'qq', '', '', '', '', '123456788', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'مێ', '', '1994-08-05'),
(64, 'sk2', 3, 'ss', 'ss', 'ss', 'ss', '', '', '', '', '99986', 'bardawam', '2018-2019', 'ئاسایی', 'ناوشار', 'مێ', '', '1991-08-05'),
(67, 'kh7', 3, 'ww', 'ww', 'ww', 'ww', '', '', '', '', '134688', 'bardawam', '2018-2019', 'دابەزین', 'ناوشار', 'نێر', '', '2017-08-05'),
(72, 'yt21', 3, 'yy', 'yy', 'yy', 'yy', '', '', '', '', '12345986', 'bardawam', '2018-2019', 'گواستنەوە', 'دەرەوەی شار', 'نێر', '', '2017-08-05'),
(73, 'test12', 3, 'test', 'test', 'test', 'tes', '', '', '', '', '2628', 'bardawam', '2018-2019', 'پارالێڵ', 'ناوشار', 'نێر', '', '1979-08-16');

-- --------------------------------------------------------

--
-- Table structure for table `stu_has_req`
--

CREATE TABLE `stu_has_req` (
  `id_shr` int(11) NOT NULL,
  `id_student` int(11) DEFAULT NULL,
  `id_req` int(11) DEFAULT NULL,
  `id_attach` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `stu_has_req`
--

INSERT INTO `stu_has_req` (`id_shr`, `id_student`, `id_req`, `id_attach`) VALUES
(13, 8, 6, 12),
(14, 9, 1, 13),
(15, 9, 4, 14),
(16, 9, 1, 15),
(17, 8, 1, 16),
(18, 8, 1, 17),
(19, 8, 1, 18),
(20, 7, 1, 19),
(21, 7, 1, 20),
(22, 8, 1, 21),
(23, 7, 1, 22),
(24, 7, 2, 23),
(25, 30, 2, 2),
(26, 30, 3, 3),
(27, 30, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `stu_req_cert`
--

CREATE TABLE `stu_req_cert` (
  `id_src` int(11) NOT NULL,
  `id_student` int(11) DEFAULT NULL,
  `id_cert` int(11) DEFAULT NULL,
  `id_attach` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `stu_req_rep`
--

CREATE TABLE `stu_req_rep` (
  `id_srr` int(11) NOT NULL,
  `id_student` int(11) DEFAULT NULL,
  `id_rep` int(11) DEFAULT NULL,
  `id_attach` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `body` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `direct` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `follow` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id_sub` int(11) NOT NULL,
  `id_dep` int(11) DEFAULT NULL,
  `sub_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `stage` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `unit` int(2) DEFAULT NULL,
  `sub_type` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `h_theory` int(2) DEFAULT NULL,
  `h_practice` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id_sub`, `id_dep`, `sub_name`, `stage`, `unit`, `sub_type`, `h_theory`, `h_practice`) VALUES
(1, 3, 'programming language', 'yakam', 2, 'salana', 3, 2),
(2, 3, 'computer logic & organization', 'yakam', 1, 'salana', 2, 2),
(3, 3, 'English', 'yakam', 1, 'salana', 2, 0),
(4, 3, 'kurdology', 'yakam', 1, 'salana', 2, 0),
(5, 3, 'calculus', 'yakam', 1, 'salana', 2, 0),
(6, 3, 'Academic debate', 'yakam', 0, 'be nmra', 2, 0),
(7, 3, 'Data structure', 'dwam', 4, 'salana', 2, 2),
(8, 3, 'Object oriented programming', 'dwam', 2, 'salana', 2, 2),
(9, 3, 'Computer Architecture', 'dwam', 2, 'salana', 2, 2),
(10, 3, 'numerical', 'dwam', 1, 'salana', 2, 2),
(11, 3, 'Information System Analyse and Design', 'dwam', 2, 'salana', 2, 2),
(12, 3, 'linear algebra & statistic', 'dwam', 2, 'salana', 2, 2),
(13, 3, 'Compiler', 'seyam', 6, 'salana', 3, 2),
(14, 3, 'security', 'seyam', 4, 'salana', 3, 2),
(15, 3, 'Computer graphic', 'seyam', 4, 'salana', 3, 2),
(16, 3, 'Network', 'seyam', 4, 'salana', 3, 2),
(17, 3, 'Database', 'seyam', 4, 'salana', 3, 2),
(18, 3, 'Web programming', 'seyam', 4, 'salana', 3, 2),
(19, 3, 'Operating System', 'chwaram', 6, 'salana', 3, 2),
(20, 3, 'Mobile Application', 'chwaram', 6, 'salana', 2, 2),
(21, 3, 'Artificial intelligence', 'chwaram', 6, 'salana', 2, 2),
(22, 3, 'Database Adminstrator', 'chwaram', 6, 'salana', 2, 2),
(23, 3, 'methodology', 'chwaram', 6, 'corsat', 2, 2),
(24, 4, 'algebra', 'dwam', 6, 'corsat', 2, 2),
(25, 4, 'Discrete', 'seyam', 6, 'salana', 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id_person` int(11) NOT NULL,
  `id_dep` int(11) DEFAULT NULL,
  `acadimic_title` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id_person`, `id_dep`, `acadimic_title`) VALUES
(6, 3, 'mamosta'),
(7, 4, 'mamosta');

-- --------------------------------------------------------

--
-- Table structure for table `teach_has_sub`
--

CREATE TABLE `teach_has_sub` (
  `id_teacher` int(11) NOT NULL,
  `id_sub` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `transfer`
--

CREATE TABLE `transfer` (
  `id_student` int(11) NOT NULL,
  `id_stage` int(11) NOT NULL,
  `resons` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `dep_name` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `univ_name` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `file_name` varchar(500) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transfer`
--

INSERT INTO `transfer` (`id_student`, `id_stage`, `resons`, `dep_name`, `univ_name`, `file_name`) VALUES
(3, 1, '‌أ', 'IT', 'Komar', '1557226405024.png'),
(31, 1, 'ب', 'کۆمپیوتەر', '', NULL),
(32, 1, '‌أ', 'کیمیا', '', NULL),
(33, 2, 'ب', 'کۆمپیوتەر', '', NULL),
(52, 1, 'ج', 'computer', '', NULL),
(61, 1, 'پارالێڵ', 'ooooo', 'oooo', NULL),
(62, 1, 'د', 'pphh', '', NULL),
(72, 1, 'پارالێڵ', 'yy', 'yy', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id_action`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id_attach`);

--
-- Indexes for table `certification`
--
ALTER TABLE `certification`
  ADD PRIMARY KEY (`id_cert`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id_dep`);

--
-- Indexes for table `dep_head`
--
ALTER TABLE `dep_head`
  ADD PRIMARY KEY (`id_sem`,`id_dep`,`head_name`,`assistance`);

--
-- Indexes for table `descents`
--
ALTER TABLE `descents`
  ADD PRIMARY KEY (`id_student`,`id_stage`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id_person`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id_student`,`id_sub`,`id_sem`);

--
-- Indexes for table `ordinary`
--
ALTER TABLE `ordinary`
  ADD PRIMARY KEY (`id_student`,`id_stage`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id_payment`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id_role`,`id_action`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id_person`);

--
-- Indexes for table `recursives`
--
ALTER TABLE `recursives`
  ADD PRIMARY KEY (`id_student`,`id_stage`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id_rep`);

--
-- Indexes for table `requirements`
--
ALTER TABLE `requirements`
  ADD PRIMARY KEY (`id_req`);

--
-- Indexes for table `semsters`
--
ALTER TABLE `semsters`
  ADD PRIMARY KEY (`id_sem`);

--
-- Indexes for table `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`id_stage`);

--
-- Indexes for table `stg_sem_sub`
--
ALTER TABLE `stg_sem_sub`
  ADD PRIMARY KEY (`id_stage`,`id_sem`,`id_sub`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id_student`);

--
-- Indexes for table `stu_has_req`
--
ALTER TABLE `stu_has_req`
  ADD PRIMARY KEY (`id_shr`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id_person`);

--
-- Indexes for table `transfer`
--
ALTER TABLE `transfer`
  ADD PRIMARY KEY (`id_student`,`id_stage`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `id_action` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id_attach` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `certification`
--
ALTER TABLE `certification`
  MODIFY `id_cert` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id_payment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id_person` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id_rep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `requirements`
--
ALTER TABLE `requirements`
  MODIFY `id_req` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `semsters`
--
ALTER TABLE `semsters`
  MODIFY `id_sem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `stu_has_req`
--
ALTER TABLE `stu_has_req`
  MODIFY `id_shr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id_sub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
