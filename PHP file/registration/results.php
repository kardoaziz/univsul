<?php 

$info=json_decode($_POST['info']); 
$data=json_decode($_POST['data']); 
$dep_info=json_decode($_POST['dep_info']); 
// print_r(  $dep_info);
$Pla = ['كه‌وتووه‌', 'په‌سه‌ند','ناوه‌ند','باش','زۆرباش','نایاب'];
// $students[] =array("f_name"=>"kardo","m_name"=>"othman","s_name"=>"aziz","l_name"=>"hama");
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Student Results</title>
    <link rel="stylesheet" type="text/css" href="../asset/css/style.css">
    <script type="text/javascript" src="../asset/js/hotsnackbar.js"></script>
</head>
<style type="text/css">
	body{
		width:21cm !important;
		margin-left: auto;
		margin-right: auto;
	}
	.text-right{
		text-align: right !important;
	}
	.text-center{
		text-align: center !important;
	}
	.text-left{
		text-align: left !important;
	}
	.full{
		width:100% !important;
	}
	.ffull{
		min-width:100% !important;
	}
	.capital{
		text-transform: capitalize;
	}
	.bold{
	font-weight: bold;
		}
		table tr {
			height:30px;
			/*border:1px solid grey !important;*/
		}
		table tr td{
			/*border:1px solid grey !important;*/
			text-align: center;
			font-weight: bold;
			text-transform: capitalize;
		}
		table {
			/*border:1px solid grey;*/
		}
		.grades{
			width:100%;
		}
		.grades tr{
			border:1px solid grey;
		}
		.grades tr td{
			border:1px solid grey;
		}
		.grades tr th{
			border:1px solid grey;
			background-color: #CCCCCC;
			-webkit-print-color-adjust: exact !important;
		}
		@media print
{     
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
}
	</style>
	<body >
		<button style="height:40px;font-size:20px;float:right; margin:10px;" class="noprint" onclick="javascript:window.close()">Close</button>
		<button style="height:40px;font-size:20px;float:right;margin:10px; " class="noprint" onclick="javascript:window.print()">Print</button>
		<?php 
		include("config.php");
		$sql ="select s.id_student,s.f_name,s.m_name,s.s_name,s.l_name,sm.year,stg.stage_txt,g.final,g.corse1,g.corse2,sb.sub_name,sb.h_theory,sb.h_practice,sb.unit,g.crossing,g.crossing2,g.crossingfinal,g.curve,g.crossing_id_sem
from students s, stage stg, semsters sm, stg_sem_sub sss , grade g, subjects sb
where s.id_student=g.id_student and
           g.id_sem =sss.id_sem and
           sss.id_sem=sm.id_sem and
           sss.id_sub = sb.id_sub and 
           sss.id_stage =stg.id_stage and
           sb.id_sub=g.id_sub and  ((sss.id_sem=".$data->id_sem." and sss.id_stage=".$data->id_stage.") or g.crossing_id_sem=".$data->id_sem.") ";
           if($info->round==2)
           {
           	$sql.=" and s.id_student in (select distinct id_student 
							   from grade g, stg_sem_sub sss 
							   where g.id_sem=sss.id_sem and 
							   sss.id_sub=g.id_sub and 
							   sss.id_sem=".$data->id_sem." and 
							   (g.crossing2>0 or g.corse2>0) 
							   and sss.id_stage=".$data->id_stage.") ";
           }
           // echo $sql;
           $students=[];
$results = $conn->query($sql);
    while($row = $results->fetch_assoc()) {
         $students[$row["id_student"]][]= array( 
            "id_student"=>$row["id_student"],
            "f_name"=>$row["f_name"],
            "m_name"=>$row["m_name"],
            "s_name"=>$row["s_name"],
            "l_name"=>$row["l_name"],
            "sub_name"=>$row["sub_name"],
            "h_theory"=>$row["h_theory"],
            "h_practice"=>$row["h_practice"],
            "unit"=>$row["unit"],
            "year"=>$row["year"],
            "stage"=>$row["stage_txt"],
            "final"=>$row["final"],
            "corse1"=>$row["corse1"],
            "crossing_id_sem"=>$row["crossing_id_sem"],
            "crossing"=>$row["crossing"],
            "crossing2"=>$row["crossing2"],
            "crossingfinal"=>$row["crossingfinal"],
            "curve"=>$row["curve"],
            "corse2"=>$row["corse2"]
        );
    }
    $sql="SELECT grd.id_student,sum(sub.unit) as totalunit
                FROM subjects sub,grade grd,students stu, stg_sem_sub sss
                WHERE sub.id_sub=sss.id_sub AND 
                             grd.id_student= stu.id_student AND 
                             sss.id_sem=grd.id_sem and 
                             sss.id_sub =grd.id_sub and
                             sub.id_dep=$data->id_dep and 
                             sss.id_sem=$data->id_sem and sss.id_stage=$data->id_stage  group by grd.id_student limit 1";

        // $info['grades']=$data;
        $result = $conn->query($sql);
        $row=mysqli_fetch_row($result);
        $totalunit = $row[1] ;
        // echo 'total unit : '.$totalunit;
    // echo json_encode($data);
    $m=1;
    foreach ($students as $sid => $subs) {
			$k=1;
			// echo $sid.'--<br>';
			echo '
		<table style="width:100%;border:0px;margin-top:100px;">
			<tr>
				<td colspan="2" rowspan="4" style="border:0px white solid !important;font-size:24px; text-transform: capitalize;"> 
					<img src="./uploads/univsul.png" style="width:250px;height:200px;">
				</td>
				<td colspan="5" style="border:0px white solid !important;font-size:24px; text-transform: capitalize;">
					وه‌زاره‌تی خوێندنی باڵا و توێژینه‌وه‌ی زانستی <br>
				</td>

			</tr>
			<tr><td colspan="5" style="border:0px white solid !important;font-size:24px; text-transform: capitalize;">
					زانكۆی سلێمانی<br>
				</td>

			</tr>
			<tr><td colspan="5" style="border:0px white solid !important;font-size:24px; text-transform: capitalize;">
					كۆلیژی زانست<br>
				</td>
			</tr>
			<tr><td colspan="5" style="border:0px white solid !important;font-size:20px; text-transform: capitalize;">
				</td>
			</tr>';
			if($info->round==1){
			echo '<tr style="margin:10px;font-size:20px;">
				<td colspan="7" style="text-align:center;margin:10px;">
					ئه‌نجامی ئه‌زموونه‌كانی كۆتایی 
					خولی یه‌كه‌م
				</td>
			</tr>';}
			else{
				echo '<tr style="margin:10px;font-size:20px;">
				<td colspan="7" style="text-align:center;margin:10px;">
					ئه‌نجامی ئه‌زموونه‌كانی كۆتایی 
					خولی دووه‌م
				</td>
			</tr>';
			}
			echo '<tr style="margin:10px;font-size:20px;">
				<td colspan="7" style="text-align:center;margin:10px;">
					ساڵی خوێندن '.$info->sem.' <br>
				</td>
			</tr>';
			if($info->round==1){
			echo '<tr style="font-size:20px;height:100px;">
				<td colspan="7" style="text-align:center;margin:10px;">
					خولی یه‌كه‌م
				</td>
			</tr>';}
			else{
				echo '<tr style="font-size:20px;height:100px;">
				<td colspan="7" style="text-align:center;margin:10px;">
					خولی دووه‌م
				</td>
			</tr>';
			}
			 echo '<tr>
				<td colspan="7" style="text-align:right;margin:10px;font-size:20px;height:40px;">
					<a style="padding-left: 20px;">به‌ش و قۆناغ:</a> <span >'.$info->dep.'/'.$info->stage.' </span><br>
				</td>
				</tr>
				<tr >
					<td colspan="7" style="text-align:right;margin:10px;"font-size:20px;height:40px;>
					<a style="padding-left: 20px;font-size:20px;">ژ.خوێندكار:</a>  <a style="padding-left: 20px;font-size:20px;">'.$m.'</a>  <a style="padding-left: 20px;font-size:20px;">ناوی خوێندكار: </a><a style="padding-left: 20px;font-size:20px;">'.$students[$sid][0]['f_name'].' '.$students[$sid][0]['m_name'].' '.$students[$sid][0]['s_name'].' '.$students[$sid][0]['l_name'].' </a> <br>
				</td>
			</tr>
		</table>
		<table class="grades" style="font-size:20px;">
				<tr style="box-shadow: 6px 4px 0px 0px rgba(0,0,0,0.75);margin-bottom:30px;">
					<th style="box-shadow: 6px 4px 0px 0px rgba(0,0,0,0.75);margin-bottom:30px;">ئاست
					</th> 
					<th colspan="3" style="box-shadow: 6px 4px 0px 0px rgba(0,0,0,0.75);margin-bottom:30px;"> ناوی وانه‌
					</th>
					<th style="box-shadow: 6px 4px 0px 0px rgba(0,0,0,0.75);margin-bottom:30px;"> ژماره‌
					</th>
				</tr>
				<tr style="height:8px; border:0px solid white;">
				</tr>
				';
				$grade=0;
				$f=0;
				$O=0;
			foreach ($students[$sid] as $sub) {
				echo '<tr style="height:35px; font-size:20px;">';
				$p="test";
				echo "<script>console.log( 'grade: " .$sub['crossing'] . "' );</script> <td";
				
				if(($sub['corse2']==0 && $sub['crossing2']==0 )|| $info->round==1){
					if(!((int)$sub['crossing_id_sem']) )
						$x = $sub['corse1']+$sub['final']+$sub['curve'];
					else $x = $sub['crossing']+$sub['crossingfinal']+$sub['curve'];
						$grade+= $x*(intval($sub['unit'])/$totalunit);
						// echo '<td> '.($x*(intval($sub['unit'])/$totalunit)).'- '.$sub['unit'].'- '.$x.'</td>';
						$x=$x-50;
					}
					else{
						if(!((int)$sub['crossing_id_sem']) )
						$x = $sub['corse1']+$sub['corse2']+$sub['curve'];
					else $x = $sub['crossing']+$sub['crossing2']+$sub['curve'];
						$grade+= $x*(intval($sub['unit'])/$totalunit);
						// echo '<td> '.($x*(intval($sub['unit'])/$totalunit)).'- '.$sub['unit'].'- '.$x.'</td>';
						$x=$x-50;
					}
					
				if($x<0) 
					{$p=$Pla[0];
						echo ' style="background-color:#BBBBBB;" ';
						if(!((int)$sub['crossing_id_sem'])) $f+=1;
						else $O+=1;
				}
				else
				{
					$x= (int)$x/10;
					$p = $Pla[$x+1];
				}

				echo '> '.$p.' </td>
				<td colspan="3"> '.$sub['sub_name'];
				if(((int)$sub['curve'])>0)
					echo "*";
				if(((int)$sub['crossing_id_sem'])>0)
					echo "(عبور)";
				echo ' </td><td>'.$k.' </td>';
				echo '</tr>';
				$k+=1;
			}
		echo '</table><br>';
		// echo $grade;
		$grade=$grade-50;
		if($grade<0) 
					$p2=$Pla[0];
				else
				{
					$grade= (int)$grade/10;
					$p2 = $Pla[$grade+1];
				}
		echo '
		<table style="width:100%;margin-top:50px; page-break-after: always;">
			<tr style="padding-top:30px !important;text-align: right;">
				<td colspan="2">
					
				</td>';
				if($f>0)
				{
					echo '<td colspan="4" style="font-size:20px; text-align:right;border:2px solid grey;height:45px;text-align:center;">
					<a > ئیكماله‌</a> 
				</td>
				<td>
				<a style="padding-left: 20px;font-size:24px;"> :ئه‌نجام</a>
				</td>';
				}
				else{
				echo '<td colspan="4" style="font-size:20px; text-align:right;border:2px solid grey;height:45px;text-align:center;">
					<a > ده‌رچووه‌ به‌ ئاستی '.$p2;
					if($O)
					{
						echo "/له‌ عبور ئیكماله‌";
					}
					echo '</a> 
				</td>
				<td>
				<a style="padding-left: 20px;font-size:24px;"> :ئه‌نجام</a>
				</td>';}
				
			echo '</tr>
			<tr style="padding-top:30px !important;text-align: right;">
				<td colspan="2">
					
				</td>
				<td colspan="4" style="border:0px white solid !important;font-size:20px; text-align:right;">
				واته‌ ئه‌و وانه‌یه‌ به‌ بڕیار ده‌رچووه‌ (*)
					  <br>
				</td>
				<td>
				<a style="padding-left: 20px;"> :تێبینی</a> 
				</td>
			</tr>
			<tr style="padding-top:30px !important;text-align: left;">
				<td colspan="2" style="border:0px white solid !important;font-size:20px; text-align:center;">
					 '.$dep_info->head_exam.'   <br>
					سه‌رۆكی لیژنه‌ی ئه‌زموونه‌كان
				</td>
				<td colspan="5">
					
				</td>
				
			</tr>
		</table>';
		$m+=1;
		}
		
		
		
			
				// $i=1;
				// foreach ($students as $id=>$stu )
				// {
				// 	$stu = get_object_vars( $stu );

				// 	echo "<tr>
				// 			<td style='text-align:center;'>$i
				// 			</td>";
							
				// 		echo 	"<td style='text-transform:capitalize;text-align:left;'>".$stu['f_name']." ".$stu['m_name']." ".$stu['s_name']." ".$stu['l_name']."
				// 			</td>";
						
							
				// 		echo "<td style='text-transform:capitalize;'>".$stu['grade']."
				// 			</td>";
							
				// 		echo 	"
							
				// 		</tr>";
				// 		$i+=1;
				// }
			
  
?>
	</body>
	</html>