<?php 

$info=json_decode($_POST['data']); 
$student=json_decode($_POST['student']); 
// echo $attachs->certificate->requirement;
// echo $info->id_student;
// echo $data->phone;
// echo json_encode($student);
// $sems = array();
 $orders = ["st","nd","rd","th"];
 // foreach($info as $sem)
   $temp =   array_keys(get_object_vars($info));
   // print_r($temp);
   $sems = [];
   for($k=0;$k<sizeof($temp);$k++)
   {
    array_push($sems,(int)explode("-", $temp[$k])[0]);
    array_push($sems,(int)explode("-", $temp[$k])[1]);
   }

function convertNumberToWord($num = false)
{
    $num = str_replace(array(',', ' '), '' , trim($num));
    if(! $num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
    );
    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ( $tens < 20 ) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}
?>

<html>
<head>
    <title>Certification</title>
  <div style="font-weight: 600;font-size: 25px;text-align: center;width:100% !important;">University of Sulaimani</div><br>
  <div style="font-size: 22px;text-align: center;width:100% !important;">Collage of Science</div><br>
  <div style="font-size: 22px;text-align: center;width:100% !important;">Sulaimani, Kurdistan Region-Iraq</div><br><br>
<table style="width:100%;margin-left:18%;">
  <tr>
    <td colspan="9" rowspan="5">
  <img src="./uploads/univsul.png"  style="margin-left: 48%;max-height: 130px; ">
</td>
<td colspan="2" rowspan="5">

  </td><td colspan="2" rowspan="5">
    
  </td>
<td colspan="1">

    <span style="font-weight: 600;font-size: 20px;">Ref.No:</span><span style="font-weight: 600;text-transform: capitalize;"> <?php echo $student->cert_no ?></span><br>
    <span style="font-weight: 600;font-size: 20px;">Date:</span><span style="font-weight: 600;"> <?php echo $student->create_date ?></span><br>
    <span style="font-weight: 600;font-size: 20px;">E-mail:</span><span style="font-weight: 600;"> sci@univsul.com</span><br>
    <span style="font-weight: 600;font-size: 20px;">Website:</span><span style="font-weight: 600;"> www.univsul.edu.iq</span>
</td></tr></table>
    <br><br>
    <span style="clear: both;margin-left: 34%;font-weight: 200;font-size: 25px;">To:</span>
    <span style="font-weight: 600;font-size: 20px;border-bottom: 1px solid black;">Whom it may concern</span>

    <br>
    <p style="margin-left: 13%;margin-right: 12%;font-weight: 400;font-size: 20px;text-align: justify;">This is to certify that 
    <?php 
    if($student->gender=="male"){
      echo '<a style="font-weight: 600">Mr. </a>';
    }
    else{
       echo '<a style="font-weight: 600">Miss. </a>'; }
      ?>(
   
    <a style="font-weight: 600;text-transform: capitalize; 
 "><?php echo $student->fname." ".$student->mname." ".$student->sname." ".$student->lname ?></a>
    ) whose photograph is affixed above was a student at the University of Sulaimani-Collage of Science during the years ( 
    <a style="font-weight: 600"><?php echo min($sems).'-'.max($sems)?></a> 
    ), and was awarded a Bachelor's degree in 
    <a style="font-weight: 600;text-transform: capitalize;"><?php echo $student->dep_namee ?></a> 
    (B.Sc.) with standard ( 
    <a style="font-weight: 600">Fair</a> 
    ). <?php if($student->gender=="male"){
      echo 'His ';
    }
    else{
       echo 'Her '; } ?> average grade for the four academic years is ( 
    <a style="font-weight: 600"><?php 
      $G=0;
        for ($l=0;$l<sizeof($student->grades);$l++)
        {
          $G += ($student->grades[$l]*$student->dist[$l]);
        }
        echo $G;
    ?></a> ).
    <a> and stood the 
    <a style="font-weight: 600"><?php echo $student->order_no?><sup style="font-weight: 600"><?php 
          if ($student->order_no==1)
            echo 'st';
          else if ($student->order_no==2) 
            echo 'nd';
          else if ($student->order_no==3)
            echo 'rd';
          else 
            echo 'th';
    ?></sup></a> 
    out of <a style="text-transform: capitalize;"><?php echo convertNumberToWord($student->total_student) ?> graduates,</a> 
    for the <a style="font-weight: 600;text-transform: capitalize;"><?php echo $student->round ?> trail.</a> 
    Below is the transcript of 
    <?php if($student->gender=="male"){
      echo 'his ';
    }
    else{
       echo 'her '; } ?> academic records.
    </a>
    </p>

    
    </head>
    
<body>
    <br>
    <?php 
    $i=0;
   
    $trails = ["First","Second"];
      foreach($info as $sem=>$semdetail)
      {

     $passedtrail=$trails[0];
    echo '<span style="margin-left: 43%;font-weight: 700;font-size: 22px;margin-top:20px;">'.($i+1).'<sup>'.$orders[$i].'</sup> Year </span>
    <span style="font-weight: 700;font-size: 20px;">'. $sem.'</span>
    <table style="width: 75%;margin-left: 12%;margin-right: 24%;font-weight: 400;font-size: 20px;border: 1px double black;border-collapse: collapse;margin-bottom:20px;">
        <tr>
          <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;">  
           Subjects
          </td>

          <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;">  
          Units
          </td>
            
          <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;" > 
          Theory
          </td>
            <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;" > 
            Practical
            </td>
            
            <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;">  
           Marks
           </td>
        </tr>
      
      ';
      foreach($semdetail as $sub=>$subdetail)
      {

        if($subdetail->crossing2)
        {
        $mark=intval($subdetail->crossing)+intval($subdetail->crossing2)+intval($subdetail->curve);
        $passedtrail=$trails[1];
      }else if($subdetail->crossing){
        $mark=intval($subdetail->crossing)+intval($subdetail->crossingfinal)+intval($subdetail->curve);
        
      }
      else if($subdetail->corse2)
        {
        $mark=intval($subdetail->corse2)+intval($subdetail->corse1)+intval($subdetail->curve);
        $passedtrail=$trails[1];
      }else{
        $mark=intval($subdetail->final)+intval($subdetail->corse1)+intval($subdetail->curve);
        
      }
          echo '  <tr>
          <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;text-transform:capitalize;">  
           '.$subdetail->sub_name;
           if($passedtrail=="Second")
           {
            echo '*';
           }
           echo '
          </td>

          <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;">  
          '.$subdetail->unit.'
          </td>
            
          <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;" > 
          '.$subdetail->h_theory.'
          </td>
            <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;" > 
            '.$subdetail->h_practice.'
            </td>
            
            <td style="text-align: center;font-weight: 900;font-size: 22px;border: 1px solid black;">  
           '.$mark.'
           </td>
        </tr>';
      }
     echo '<tr width="100%">
      
              <td style="font-weight: 400;font-size: 21px;height: 50px;">  
               Result: Passed in the <span style="font-weight: bold;text-transform:capitalize;">'.$passedtrail.'</span> trail
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
          </tr> 
    </table>';
    $i+=1;
     }
    ?>
    
    
    
    
    
        
    <br>
    <p style="margin-left: 14%;margin-right: 14%;font-weight: 400;font-size: 20px;">
        
    <span style="font-weight: bold;border-bottom: 1px solid black">N.B.</span><br><br>
    <span style="font-weight: bold;">1. </span>Passing grade is <?php echo $student->passing_limit; ?>% .<br>
    <span style="font-weight: bold;">2. </span>Grading system: (50-59% Satisfactory), (60-69% Fair),
    (70-79% Good), (80-89% Very Good), (90-100% Excellent) .<br>
    <span style="font-weight: bold;">3. </span>The average is computed ad follows: (10% 1<sup>th</sup> year), 
    (20% 2<sup>th</sup> year), (30% 3<sup>th</sup> year), (40% 4<sup>th</sup> year) .<br>
    <span style="font-weight: bold;">4. </span>Years failed .<br>
    <span style="font-weight: bold;">5. </span>Years deferred <span style="font-weight: bold;">Nill</span> .<br>
    <span style="font-weight: bold;">6. </span>Years (-) suspended due legitimate reasons: <span style="font-weight: bold;">Nill</span> .<br>
    <span style="font-weight: bold;">7. </span>Summers field training: <span style="font-weight: bold;">(30) thirty days of Practical and Theory</span> .<br>
    <span style="font-weight: bold;">8. </span>(*) Denots passing in second tail .<br>
    <span style="font-weight: bold;">9. </span>(**) Denots for those who are sick leave or having legitimate excuse, ten marks will be deducted .<br>
    <span style="font-weight: bold;">10. </span>This document should be compoesed of two pages and should be free from rubbing erasure and distortion .<br>
    </p><br><br><br><br><br>

   
   <table style="width:100% !important;margin-left:5% !important;margin-right:5% !important;">
    <tr>
      <td>
        Acting President
      </td>
       <td>Dean
      </td>
       <td>General Registrar
      </td>
       <td>Collage Registrar
      </td>
    </tr>
    <tr>
       <td>Asst.Prof.Dr. Radiha Hasan Hussein
      </td>
       <td>Asst.Prof.Dr. Soran M.Mamand
      </td>
       <td>Dr.Choman A.Omar
      </td>
       <td>Asst.Prof.Dr. Meeran A.Omar
      </td>
    </tr>
  </table>
    
    
    </body>
</html>