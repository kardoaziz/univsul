<?php
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Methods: POST");
include("config.php");
error_reporting(0);

$request = $_POST['request'];
if($request=="addsubject"){
	$id_dep=$_POST['id_dep'];
	$sub_name=$_POST['sub_name'];
	$stage=$_POST['stage'];
	$unit=$_POST['unit'];
	$sub_type=$_POST['sub_type'];
	$h_theory=$_POST['h_theory'];
	$h_practice=$_POST['h_practice'];

	$sql="insert into subjects(id_dep,sub_name,stage,unit, sub_type, h_theory,h_practice) VALUES ($id_dep,'$sub_name','$stage',$unit,'$sub_type',$h_theory,$h_practice);";
	// echo $sql;
	$result = $conn->query($sql);
	$obj  =new  stdClass();
	if($result){
		$obj->result = "success";
	}
	else{
		$obj->result="failed";
		// echo "decline";
	}

	echo json_encode($obj);

}
else if($request=="getsubject"){

 $sql="SELECT s.id_sub,s.id_dep,s.sub_name,s.stage ,s.unit,s.sub_type,	 s.h_theory,s.h_practice, d.id_dep,d.dep_namek
		FROM subjects s,departments d WHERE s.id_dep=d.id_dep and d.dep_namek='".$_POST["dep"]."';";
 $result = $conn->query($sql);
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
        	"id" => $row["id_sub"],
            "id_dep"=>$row["id_dep"],
            "sub_name"=>$row["sub_name"],
            "stage"=>$row["stage"],
            
            "unit"=>$row["unit"],
            "sub_type"=>$row["sub_type"],
            "h_theory"=>$row["h_theory"],
            "h_practice"=>$row["h_practice"],
            "dep_namek"=>$row["dep_namek"],
        );
    }
    echo json_encode($data);	
}
else if($request=="get_stage_subject"){
	$id_student=$_POST['id_student'];
    $id_sem=$_POST['id_sem'];
    $id_stage=$_POST['id_stage'];
	$sql="SELECT stu.id_student,stu.f_name,sub.id_sub,sub.sub_name,sub.stage,grd.id_student,grd.id_sem,grd.corse1,grd.final,grd.corse2,grd.state,grd.level,grd.offer_type ,grd.crossing,grd.crossing2,grd.crossingfinal,grd.note
FROM subjects sub,grade grd,students stu ,stg_sem_sub sss
WHERE sub.id_sub=grd.id_sub AND grd.id_student= stu.id_student AND grd.id_student=$id_student and grd.id_sem=$id_sem and grd.id_sem=sss.id_sem and sss.id_sub=grd.id_sub and sss.id_stage=$id_stage
union 
SELECT stu.id_student,stu.f_name,sub.id_sub,sub.sub_name,sub.stage,grd.id_student,grd.id_sem,grd.corse1,grd.final,grd.corse2,grd.state,grd.level,grd.offer_type,grd.crossing ,grd.crossing2,grd.crossingfinal,grd.note
FROM subjects sub,grade grd,students stu ,stg_sem_sub sss
WHERE sub.id_sub=grd.id_sub AND grd.id_student= stu.id_student AND grd.id_student=$id_student and grd.crossing_id_sem=$id_sem and grd.offer_type='crossing' and grd.id_sem=sss.id_sem and sss.id_stage=$id_stage and grd.id_sub=sss.id_sub;";
    
	$result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_student" => $row["id_student"],
            "f_name" => $row["f_name"],
        	"id_sub" => $row["id_sub"],
            "sub_name"=>$row["sub_name"],
            "state"=>$row["state"],
            "offer_type"=>$row["offer_type"],
            "id_sem"=>$row["id_sem"],
            "corse1"=>$row["corse1"],
            "crossing"=>$row["crossing"],
            "crossingfinal"=>$row["crossingfinal"],
            "crossing2"=>$row["crossing2"],
            "final"=>$row["final"],
            "level"=>$row["level"],
            "corse2"=>$row["corse2"],
            "stage"=>$row["stage"],
            "note"=>$row["note"],
            "g_final"=>$row["corse1"]+$row["final"],
            "g_corse2"=>$row["corse1"]+$row["corse2"],
            "g_crossingfinal"=>$row["crossing"]+$row["crossingfinal"],
            "g_crossing2"=>$row["crossing"]+$row["crossing2"],
        );
    }
    echo json_encode($data);
}
else if($request=="get_sub_student"){
    $id_dep=$_POST['id_dep'];
    $id_sem=$_POST['id_sem'];
    $id_sub=$_POST['id_sub'];
    $sql="SELECT stu.id_student,stu.f_name,stu.m_name,stu.s_name,stu.l_name,sub.id_sub,sub.sub_name,sub.stage,grd.id_student,grd.id_sem,grd.corse1,grd.final,grd.corse2,grd.state,grd.level ,grd.crossing, grd.crossingfinal,grd.crossing2,grd.offer_type,grd.curve,grd.note
FROM subjects sub,grade grd,students stu, stg_sem_sub sss
WHERE sub.id_sub=sss.id_sub AND 
             grd.id_student= stu.id_student AND 
             sss.id_sem=grd.id_sem and 
             sss.id_sub =grd.id_sub and
             sub.id_dep=$id_dep and 
             sss.id_sem=$id_sem and
             sub.id_sub=$id_sub;";

    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_student" => $row["id_student"],
            "f_name" => $row["f_name"],
            "m_name" => $row["m_name"],
            "s_name" => $row["s_name"],
            "l_name" => $row["l_name"],
            "id_sub" => $row["id_sub"],
            "sub_name"=>$row["sub_name"],
            "state"=>$row["state"],
            "id_sem"=>$row["id_sem"],
            "corse1"=>$row["corse1"],
            "final"=>$row["final"],
            "level"=>$row["level"],
            "curve"=>$row["curve"],
            "corse2"=>$row["corse2"],
            "crossing"=>$row["crossing"],
            "crossing2"=>$row["crossing2"],
            "crossingfinal"=>$row["crossingfinal"],
            "stage"=>$row["stage"],
            "note"=>$row["note"],
            "offer_type"=>$row["offer_type"],
            "g_final"=>$row["corse1"]+$row["final"],
            "g_corse2"=>$row["corse1"]+$row["corse2"],
        );
    }
    echo json_encode($data);
}
else if($request=="stufailedclass"){
    $id_dep=$_POST['id_dep'];
    $id_sem=$_POST['id_sem'];
    $id_stage=$_POST['id_stage'];
    $round=$_POST['round'];
    $passing_limit=$_POST['passing_limit'];
    $sql="SELECT *
            FROM grade g, subjects sub, students s,stg_sem_sub sss
            where g.id_sub=sub.id_sub and sub.id_dep=$id_dep and  g.id_student=s.id_student and ";
            if($round=="1"){
                $sql.="(corse1+final <$passing_limit) and 
            (crossing+crossingfinal <$passing_limit) and 
              ";
            }else if ($round=="2")
            {
                 $sql.=" (corse1+final+curve <$passing_limit) and 
            (crossing+crossingfinal+curve<$passing_limit) and  
            (corse2+corse1<$passing_limit) and  
            (crossing+crossing2<$passing_limit)  and ";
            }
           $sql.= " 
                        ((g.id_sem=$id_sem and sss.id_stage=$id_stage) or g.crossing_id_sem=$id_sem) and sss.id_sem =g.id_sem and sss.id_sub=g.id_sub ";
            // echo $sql;
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_student" => $row["id_student"],
            "f_name" => $row["f_name"],
            "m_name" => $row["m_name"],
            "s_name" => $row["s_name"],
            "l_name" => $row["l_name"],
            "id_sub" => $row["id_sub"],
            "sub_name"=>$row["sub_name"],
            "state"=>$row["state"],
            "id_sem"=>$row["id_sem"],
            "corse1"=>$row["corse1"],
            "final"=>$row["final"],
            "level"=>$row["level"],
            "curve"=>$row["curve"],
            "corse2"=>$row["corse2"],
            "crossing"=>$row["crossing"],
            "crossing2"=>$row["crossing2"],
            "crossingfinal"=>$row["crossingfinal"],
            "stage"=>$row["stage"],
            "note"=>$row["note"],
            "offer_type"=>$row["offer_type"],
            "g_final"=>$row["corse1"]+$row["final"],
            "g_corse2"=>$row["corse1"]+$row["corse2"]
        );
    }
   
    echo json_encode($data);
}
else if($request=="no_student")
{
    $id_dep=$_POST['id_dep'];
    $id_sem=$_POST['id_sem'];
    $id_stage=$_POST['id_stage'];
    $passing_limit=$_POST['passing_limit'];

     $sql = "SELECT count(distinct g.id_student) as no_student
            FROM grade g, subjects sub, students s,stg_sem_sub sss
            where g.id_sub=sub.id_sub and sub.id_dep=$id_dep and  g.id_student=s.id_student and
            (g.id_sem=$id_sem or g.crossing_id_sem=$id_sem) and sss.id_sem =g.id_sem and sss.id_sub=g.id_sub and sss.id_stage=$id_stage ";
            $r = $conn->query($sql);
            $row = $r->fetch_assoc();
            $data['no_student']= $row['no_student'];

    $sql = "SELECT count(distinct g.id_student) as no_failed
            FROM grade g, subjects sub, students s
            where g.id_sub=sub.id_sub and sub.id_dep=$id_dep and  g.id_student=s.id_student and
            (corse1+final+curve <$passing_limit) and 
            (corse2+corse1+curve<$passing_limit) and 
            (crossing+crossingfinal+curve<$passing_limit) and 
            (crossing+crossing2+curve<$passing_limit)  and
            (id_sem=$id_sem or crossing_id_sem=$id_sem)";
            $r = $conn->query($sql);
            $row = $r->fetch_assoc();
            $data['no_failed']= $row['no_failed'];
            echo json_encode($data);
}
else if($request=="clearcurve")
{
    $id_dep=$_POST['id_dep'];
    $id_sem=$_POST['id_sem'];
    $id_stage=$_POST['id_stage'];
    $passing_limit=$_POST['passing_limit'];

     $sql = "UPDATE  grade g, subjects sub, students s,stg_sem_sub sss
   SET g.curve = 0
  where g.id_student=s.id_student and sub.id_sub=g.id_sub and g.id_sem=sss.id_sem and sss.id_sub=g.id_sub and g.id_sem=$id_sem and sss.id_stage=$id_stage and sub.id_dep=$id_dep
";
            $r = $conn->query($sql);
             $obj  =new  stdClass();
    if($r){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
            
}
else if($request=="applycurve")
{
    $data=json_decode($_POST['data']);
    foreach ($data as $d) {
        # code...
         $sql = "UPDate grade set curve='".$d->help."' where (id_sem=".$d->id_sem." or crossing_id_sem=".$d->id_sem.")and id_student=".$d->id_student." and id_sub=".$d->id_sub;
         // echo $sql."<br>";
         $r = $conn->query($sql);
    }
    
            // $r = $conn->query($sql);
             $obj  =new  stdClass();
    if($r){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
            
}
else if($request=="mastersheet"){
    $id_dep=$_POST['dep'];
    $id_sem=$_POST['sem'];
    $id_stage=$_POST['stage'];
    $sql="SELECT s.sub_name
from grade  g,subjects s, stg_sem_sub sss,students st
where g.id_sub=s.id_sub and s.id_sub=sss.id_sub and sss.id_sem=g.id_sem and g.id_student=st.id_student and sss.id_sem=$id_sem and sss.id_stage=$id_stage  and s.id_dep=$id_dep  order by st.f_name";
$result = $conn->query($sql);
$subs=[];
    while($row = $result->fetch_assoc()) {
        array_push($subs, $row['sub_name']);
        } 

$sql="SELECT st.id_student,st.f_name,st.m_name,st.s_name,st.l_name,s.sub_name,g.corse1,g.corse2,g.final,g.crossing,g.crossing2,g.crossingfinal,g.curve, sss.id_stage
from grade  g,subjects s, stg_sem_sub sss,students st
where g.id_sub=s.id_sub and s.id_sub=sss.id_sub and sss.id_sem=g.id_sem and g.id_student=st.id_student and sss.id_sem=$id_sem and sss.id_stage=$id_stage  and s.id_dep=$id_dep  order by st.f_name";
            // echo $sql;
    $result = $conn->query($sql);
    $data=[];
    while($row = $result->fetch_assoc()) {
        // if(!$data[$row["id_student"]])
        // {
        //     $data['id_student']=[];
        // }
            $data[$row["id_student"]][$row["sub_name"]]=array( 
            "id_student" => $row["id_student"],
            "f_name" => $row["f_name"],
            "m_name" => $row["m_name"],
            "s_name" => $row["s_name"],
            "l_name" => $row["l_name"],
            "sub_name"=>$row["sub_name"],
            "id_sem"=>$row["id_sem"],
            "corse1"=>$row["corse1"],
            "final"=>$row["final"],
            "curve"=>$row["curve"],
            "corse2"=>$row["corse2"],
            "crossing"=>$row["crossing"],
            "crossing2"=>$row["crossing2"],
            "crossingfinal"=>$row["crossingfinal"],
            "id_stage"=>$row["id_stage"]
        
        );
        // }
        // $data[] = 
    }
    $data['subs']=$subs;
    // $r= [json_encode($data),json_encode($subs)];
    echo json_encode($data);
}
/////////////////////////////////////////////////
else if ($request=="addcorse1") {
    $corse1=$_POST['corse1'];
    $corse1 = json_decode($corse1);
foreach ($corse1 as &$c) {
    if($c->offer_type == 'crossing')
    {
         $sql="UPDATE grade SET crossing=$c->crossing WHERE id_student=$c->id_student AND id_sub=$c->id_sub and id_sem=$c->id_sem;";
    }
    else{

    $sql="UPDATE grade SET corse1=$c->corse1 WHERE id_student=$c->id_student AND id_sub=$c->id_sub and id_sem=$c->id_sem;";
    }
    // echo $sql;
    $result = $conn->query($sql);
}
    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}



//////////////////////////////////////
else if ($request=="addfinal") {
    // echo "add final";
    $final=$_POST['final'];
    // $state=$_POST['state'];
    $final = json_decode($final);
    // $state=json_decod($state);
foreach ($final as &$c) {
    if($c->offer_type == 'crossing')
    {
        $sql="UPDATE grade SET crossingfinal=$c->crossingfinal,state='$c->states',level='$c->level' WHERE id_student=$c->id_student AND id_sub=$c->id_sub and id_sem=$c->id_sem;";
      }
      else{

    $sql="UPDATE grade SET final=$c->final,state='$c->states',level='$c->level' WHERE id_student=$c->id_student AND id_sub=$c->id_sub and id_sem=$c->id_sem;";
      }
    // echo $sql;
    $result = $conn->query($sql);
}
    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request=="savenotes") {
    // echo "save notes";
    $stu=$_POST['stu'];
    $stu = json_decode($stu);
foreach ($stu as $c) {
   
        $sql="UPDATE grade SET note='$c->note' WHERE id_student=$c->id_student AND id_sub=$c->id_sub and id_sem=$c->id_sem;";
     // echo $sql;
        $result = $conn->query($sql);
        }
    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request=="addgrades") {
    // echo "add grades";
    $round=$_POST['round'];
    $grades = json_decode($_POST['grades']);
    // echo($grades);
    // echo("---");
    foreach ($grades as $c) {
        if($c->offer_type == 'crossing')
        {
            $sql="UPDATE grade SET crossingfinal=$c->crossingfinal,crossing=$c->crossing,crossing2=$c->crossing2 WHERE id_student=$c->id_student AND id_sub=$c->id_sub and id_sem=$c->id_sem;";
          }
          else{

        $sql="UPDATE grade SET final=$c->final,corse1=$c->corse1,corse2=$c->corse2 WHERE id_student=$c->id_student AND id_sub=$c->id_sub and id_sem=$c->id_sem;";
          }
        // echo $sql;
        $result = $conn->query($sql);
    }
    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request=="addcorse2") {
    $corse2=$_POST['corse2'];
    $corse2 = json_decode($corse2);
foreach ($corse2 as &$c2) {
    if($c2->offer_type == 'crossing')
    {
         $sql="UPDATE grade SET crossing2=$c2->crossing2 WHERE id_student=$c2->id_student AND id_sub=$c2->id_sub and id_sem=$c2->id_sem;";
    }
    else{

    $sql="UPDATE grade SET corse2=$c2->corse2 WHERE id_student=$c2->id_student AND id_sub=$c2->id_sub and id_sem=$c2->id_sem;";
    }
    // echo $sql;
    $result = $conn->query($sql);
}
    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request=="locksemester") {
    $sem=$_POST['sem'];
    
                  $sql="UPDATE semsters SET locked=1 where id_sem=$sem;";

    
    $result = $conn->query($sql);

    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request=="unlocksemester") {
    $sem=$_POST['sem'];
    
         $sql="UPDATE semsters SET locked=0 where id_sem=$sem;";
    
    $result = $conn->query($sql);

    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if ($request=="stsubgrades") {
    $id_sem=$_POST['id_sem'];
    $id_dep=$_POST['id_dep'];
    $id_stage=$_POST['id_stage'];
    $passing_limit=$_POST['passing_limit'];
         $sql="SELECT stu.id_student,stu.f_name,stu.m_name,stu.s_name,stu.l_name,sub.id_sub,sub.sub_name,sub.stage,grd.id_student,grd.id_sem,grd.corse1,grd.final,grd.corse2,grd.state,grd.level ,grd.crossing, grd.crossingfinal,grd.crossing2,grd.offer_type,grd.curve,grd.note,sub.unit
FROM subjects sub,grade grd,students stu, stg_sem_sub sss
WHERE sub.id_sub=sss.id_sub AND 
             grd.id_student= stu.id_student AND 
             sss.id_sem=grd.id_sem and 
             sss.id_sub =grd.id_sub and
             sub.id_dep=$id_dep and 
             (grd.corse1+grd.final+grd.curve >=$passing_limit) and
              sss.id_sem=$id_sem  and sss.id_stage=$id_stage and stu.id_student not in (
             SELECT distinct g.id_student as no_failed
            FROM grade g, subjects sub, students s
            where g.id_sub=sub.id_sub and sub.id_dep=$id_dep and  g.id_student=s.id_student and
            (g.corse2<>0 or g.crossing2<>0)  and
            id_sem=$id_sem )";
    // echo $sql;
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $data[] = array( 
            "id_student" => $row["id_student"],
            "f_name" => $row["f_name"],
            "m_name" => $row["m_name"],
            "s_name" => $row["s_name"],
            "l_name" => $row["l_name"],
            "id_sub" => $row["id_sub"],
            "sub_name"=>$row["sub_name"],
            "state"=>$row["state"],
            "unit"=>$row["unit"],
            "id_sem"=>$row["id_sem"],
            "corse1"=>$row["corse1"],
            "final"=>$row["final"],
            "level"=>$row["level"],
            "curve"=>$row["curve"],
            "corse2"=>$row["corse2"],
            "crossing"=>$row["crossing"],
            "crossing2"=>$row["crossing2"],
            "crossingfinal"=>$row["crossingfinal"],
            "stage"=>$row["stage"],
            "note"=>$row["note"],
            "offer_type"=>$row["offer_type"],
            "g_final"=>$row["corse1"]+$row["final"],
            "g_corse2"=>$row["corse1"]+$row["corse2"]
        );
    }
    $sql="SELECT grd.id_student,sum(sub.unit) as totalunit
                FROM subjects sub,grade grd,students stu, stg_sem_sub sss
                WHERE sub.id_sub=sss.id_sub AND 
                             grd.id_student= stu.id_student AND 
                             sss.id_sem=grd.id_sem and 
                             sss.id_sub =grd.id_sub and
                             sub.id_dep=$id_dep and 
                             sss.id_sem=$id_sem and sss.id_stage=$id_stage  group by grd.id_student limit 1";

        $info['grades']=$data;
        $result = $conn->query($sql);
        $row=mysqli_fetch_row($result);
        $info['totalunit']=$row[1] ;
        echo json_encode($info);
}


// else if($request =="getSubjectStage"){
//     $stagetxt=$_POST['stagetxt'];
//     $sql="SELECT id_sub,id_dep,sub_name,stage,unit,sub_type FROM subjects WHERE stage='$stagetxt';";

//     $result = $conn->query($sql);
//         }
//     $obj  =new  stdClass();
//     if($result){
//         $obj->result = "success";
//     }
//     else{
//         $obj->result="failed";
//     }
//     echo json_encode($obj);
// }
else if($request =="updateSubject"){
    $id_dep=$_POST['id_dep'];
    $id_subject=$_POST['id_subject'];
    $sub_name=$_POST['sub_name'];
    $selected_stage=$_POST['selected_stage'];
    $sub_type=$_POST['sub_type'];
    $unit=$_POST['unit'];
    $theory=$_POST['theory'];
    $practice=$_POST['practice'];
    $sql="UPDATE subjects SET id_dep=$id_dep,sub_name='$sub_name',stage='$selected_stage',unit=$unit,sub_type='$sub_type',h_theory=$theory,h_practice=$practice WHERE id_sub=$id_subject;";

    $result = $conn->query($sql);
        
    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}
else if($request =="deleteSubject"){
    $id_sub=$_POST['id_sub'];
    
    $sql="DELETE FROM subjects WHERE id_sub=$id_sub;";

    $result = $conn->query($sql);
        
    $obj  =new  stdClass();
    if($result){
        $obj->result = "success";
    }
    else{
        $obj->result="failed";
    }
    echo json_encode($obj);
}

?>


